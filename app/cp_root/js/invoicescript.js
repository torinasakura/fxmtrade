jQuery(document).ready(function($) {
		$(function() {
			$( "#tabs" ).tabs();
		});
		
		
		
	});

jQuery(function($)
{
		
  $(".add_button").click(function()
	{
		update_invoice($(this).attr('prefix'));
	});
	
	$(".delete_button").click(function()
	{
		delete_invoice($(this).attr('prefix'));
	});

	$('input[type=checkbox]').change(function(){
		checkUpdateButtonToggle( $(this).attr("prefix"))
	});		
    
    
	function ajaxRequestDeleteConfirm(prefix)
	{
		m_method=$('#'+prefix+'_invoice_form').attr('method');
		m_action=$('#'+prefix+'_invoice_form').attr('action') + 'delete_invoice/'+prefix;
		m_data=$('#'+prefix+'_invoice_form').serialize();
		$.ajaxSetup({
			beforeSend	: function (xhr) { $('#'+prefix+'_ajax_img').show();  },
			complete : function (xhr){ $('#'+prefix+'_ajax_img').hide(); }
		});
		$.ajax({
			async    : false,
			dateType : 'json',
			type     : m_method,
			url      : m_action,
			data     : m_data,
			success  : function(data){
				showJsonData(prefix, data);
			}				
		});
	}
	
	function ajaxRequestUpdateConfirm(prefix, id, sum)
	{
		$.ajaxSetup({
			beforeSend	: function (xhr) { $('#'+prefix+'_ajax_img').show();  },
			complete : function (xhr){ $('#'+prefix+'_ajax_img').hide(); }
		})
		$.ajax({
			async    : false,
			dateType : 'json',
			type     : 'POST',
			url      : "/invoice/update_invoice/"+prefix,
			data     : { "id":id, "sum":sum },
			success  : function(data){
				showJsonData(prefix, data);
			}				
		});
	}
	
	function showJsonData(prefix, data)
	{
		var oMessage = jQuery.parseJSON(data)
		if(oMessage != null)
		{
			//$('#temp').attr("id", prefix+"_"+typ+"_button");
			if( oMessage.messagegTyp == 'error' ) {
                console.log ('error');
            } else if (oMessage.messagegTyp == 'ok' ) {
				confirm(prefix, 'исполнено');
			} else {
				console.log ('callback output not conform');
			}
			$('#magbox').html(oMessage.message);
		}
		else{
			console.log ('no callback output');
		}
	}
	
	function checkUpdateButtonToggle(prefix)
	{
		var countChecked = get_selected_rows('#'+prefix+'_invoice_table');
		
		if(countChecked != null && countChecked.length > 1){
			//disable add_button
			$('#'+prefix+'_add_button').removeClass("add_button").removeClass("fxr-button").addClass("disable_button");
			$('#'+prefix+'_add_button').unbind("click");
		}
		else{
			//enable add_button
			$('#'+prefix+'_add_button').removeClass("disable_button").addClass("add_button").addClass("fxr-button");
			$('#'+prefix+'_add_button').bind("click", function(){
				update_invoice($('#'+prefix+'_add_button').attr('prefix'));
			});
		}
	}
	
	function delete_invoice(prefix)
	{
		if(check_one_row_selected('#'+prefix+'_invoice_table'))
		{
			ajaxRequestDeleteConfirm(prefix);
		}
		else{
			errorOutput();
		}
	}
	
	function update_invoice(prefix)
	{
		if(check_one_row_selected('#'+prefix+'_invoice_table'))
		{
			//prüfen obwirklich nur eine row selectiert ist
			var aCheckBox = get_selected_rows('#'+prefix+'_invoice_table');
			if(aCheckBox.length == 1)
			{
				update_invoice_dialog(prefix, $(aCheckBox[0]).attr('value'));
			}
			else{
				errorOutput();
			}
		}
		else{
			errorOutput();
		}
	}
	
	function update_invoice_dialog(prefix, id)
	{
		var a = {
			title: 'Зачисления денежных средств',
			width: 320,
			buttons: {
				"Подтвердить": function() {
					var sum = $("[name='summ']").val();
					console.log( sum );
					ajaxRequestUpdateConfirm(prefix, id, sum);
					$( this ).dialog( "close" );
				}
			},
			close: function() {$("#dialog").html('')}
		};
		show_invoice(id);
		$("#dialog").dialog(a);
	}
	
	function confirm(prefix, status_name)
	{
		$('#'+prefix+'_invoice_table').find('input[type=checkbox]').each(function(){
			if( $(this).is(':checked') == true){
				$(this).attr('checked',false);
				$(this).parent().siblings("td:last").css('color', 'red').html(status_name);
				$(this).attr('disabled', 'disabled');
			}
		});
	}
	
	function show_invoice(id)
	{  
		$.ajax({  
			type: "POST",
			url: "/invoice/get_invoice",  
			cache: false,
			data: "id="+id,
			success: function(html){
				$("#dialog").html(html);
			}  
		});
	}
});