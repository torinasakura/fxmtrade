<div id="dialog" title="Basic dialog"></div>
<div id="tablewrapper">
	<div id="tableheader">
		<div class="search">
			<select id="columns" onchange="sorter.search('query')"  style="display: none;"></select>
			<input type="text" id="query" onkeyup="sorter.search('query')" />
		</div>
		<span class="details">
			<div>Показаны результаты с <span id="startrecord"></span> по <span id="endrecord"></span> из <span id="totalrecords"></span></div>
			<div><a href="javascript:sorter.reset()">Сбросить</a></div>
		</span>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
		<thead>
			<tr>
				<th class="nosort"><h3></h3></th>
							<th class="nosort"style="width: 20px;"><h3></h3></th>
				<th class="nosort" style="width: 75px;"><h3>Partner ID</h3></th>
				<th class="nosort"><h3>Фамилия Имя</h3></th>
				<th class="nosort"><h3>Спонсор</h3></th>
				<th class="nosort"><h3>E-mail</h3></th>
							<th class="nosort"><h3>Skype</h3></th>
				<th class="nosort"><h3>Статус</h3></th>
				<th class="nosort"><h3>Последняя<br />активность</h3></th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach($aAllUsers as $aUser)
				{
					//var_dump($aUser);
					echo '
					<tr>
						<td>' .($aUser["fr_user"]["sex"] ? '<span class="icon user_male" style="margin-left: 5px;"></span>' : '<span class="icon user_female" style="margin-left: 5px;"></span>'). '</td>
						<td>' .($aUser[0]["invoice_status_id"] > 2 ? '<span class="icon pay_status_0"></span>' : '<span class="icon pay_status_'.$aUser[0]["invoice_status_id"].'"></span>'). '</td>
						<td style="text-align: center;">' .$aUser["fr_user_settings"]["partner_id"]. '</td>
						<td><span id="' .$aUser["fr_user"]["id"]. '" class="link">' .$aUser["fr_user"]["last_name"]. ', ' .$aUser["fr_user"]["first_name"]. '</span></td>
						<td>' .$aUser[0]["parent_last_name"]. '</td>
						<td><a href="mailto:' .$aUser["fr_user"]["user_email"]. '?subject=Forex%20Revolution"><span class="icon email" style="margin-left: 22px;"></a></td>
						<td style="text-align:center;"> '.($aUser["fr_user_info"]["skypename"] ? '<a href="skype:' .$aUser["fr_user_info"]["skypename"]. '?chat"><img src="http://mystatus.skype.com/smallicon/' .$aUser["fr_user_info"]["skypename"]. '" style="border: none;" width="16" height="16" alt="My status" />' : '').'</td>
						<td> '.$aUser["fr_user_settings"]["user_status_name"].'</td>
						<td style="text-align:right;">' .($aUser["fr_user"]["lastvisitDate"] != '0000-00-00 00:00:00' ? date("d.m.Y", strtotime($aUser["fr_user"]["lastvisitDate"])) : 'не был(а)') . '</td>
					</tr>
					';
				}
			?>
			<!-- Table goes here -->
		</tbody>
	</table>
	
	<!-- tablefooter -->
		<?php echo $this->element('tablefooter'); ?>
	<!-- end tablefooter -->
</div>

