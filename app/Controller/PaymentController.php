<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PaymentController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Payment';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('User', 'UserInfo', 'Account', 'Invoice');
	public $components = array('Session', 'MTQ', 'UserAccount');

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function index($massege = false)
	{
		$iInviceNummer = ($this->Invoice->isInvoicAktiv()) ? $this->Invoice->isInvoicAktiv() : false;
		$this->set('iInviceNummer',$iInviceNummer);
		/*
		$iPaySum1 = 1450;
		$iPaySum2 = 515;
		$DollarRate = $this->MTQ->getDollarRate();
		$EuroPaySum1 = ceil($iPaySum1 / ($DollarRate + 0.0040));
		$EuroPaySum2 = ceil($iPaySum2 / ($DollarRate + 0.0040));
		$aSum = array('USD' => array($iPaySum1, $iPaySum2), 'EUR' => array($EuroPaySum1, $EuroPaySum2));
		$this->set('sum',$aSum);
		*/
		$this->set('paysystem', $this->Invoice->getAllActivPaysystem());
		$this->set('massege',$massege);
        $this->set('min_sum', 10);

	}

	public function err()
	{
		$massege = "При передаче данных произошла внутренняя ошибка сервиса. Попробуйте еще раз через несколько минут.";
		$this->index($massege);
		$this->render('index');
	}

	public function add()
	{
		if($this->Invoice->isInvoicAktiv())
			$this->redirect('/payment');
		if($this->request->is("post"))
		{
			$aPaySystemData = $this->Invoice->getPaysystemById($this->data['addInvoice']['payment_system_id']);
			if($aPaySystemData)
			{
				$iPaySum = $this->request->data['addInvoice']['paysum'];
				if($aPaySystemData['fr_payment']['currency_code'] == 'EUR')
				{
					$DollarRate = $this->MTQ->getDollarRate();
					$iPaySum = ceil($iPaySum / ($DollarRate + 0.0040));
				}

				if($aPaySystemData['fr_payment']['class'] == 'intern')
					$this->redirect('/payment/err');

				if ($iPaySum <10) {
                    $this->Session->setFlash("К сожалению, минимальная сумма пополнения торгового места - 10$. Попробуйте еще раз.");
                    $this->redirect("/payment/index");
                }
                $aPayData = array('sum' => $iPaySum, 'payments_id' => $aPaySystemData['fr_payment']['id'], 'user_id' => USER_ID);
				$invoiceId = $this->Invoice->addInvoice($aPayData);
				$this->set('invoiceId',$invoiceId);

				App::import("Controller", "Email");
				$oEmailController = new EmailController();
				$oEmailController->prepareMailByInvoiceID(7, $invoiceId);

			}else{
				$this->redirect('/payment');
			}
		}else{
			$this->redirect('/payment');
		}
	}

	function history()
    {
        $aInvoiceTypes = array("pay" => 1, "in" => 2, "out" =>3, "ref" => 4);
		foreach($aInvoiceTypes as $key => $value)
		{
			$aInvoices[$key] = $this->Invoice->getInvoceHistoryByType($value, true);
		}
		//var_dump($aInvoices);
		$aInvoices['in'] = array_merge((array)$aInvoices['in'], (array)$aInvoices['pay']);
		$this->set("aTabsContent", $aInvoices);
    }

	function pdf()
    {
		$this->set('invoiceId',131);
	}

    /*@method: funding
    *Add invoice for funding deposit (typ = 2)
    *@params: no params
    *@author: Svirsky Evgeny
    *@date: 30.03.2013
    */
    public function funding($typ = "2") {
        $this->set('paysystem', $this->Invoice->getAllActivPaysystem());
        if($this->request->is("post")) {
            $aPaySystemData = $this->Invoice->getPaysystemById($this->data['addInvoice']['payment_system_id'], $typ);
            if($aPaySystemData) {
                $iPaySum = $this->request->data['addInvoice']['paysum'];
                if($aPaySystemData['fr_payment']['currency_code'] == 'EUR')
                {
                    $DollarRate = $this->MTQ->getDollarRate();
                    $iPaySum = ceil($iPaySum / ($DollarRate + 0.0040));
                }
                $aPayData = array('sum' => $iPaySum, 'payments_id' => $aPaySystemData['fr_payment']['id'], 'user_id' => USER_ID);
                $invoiceId = $this->Invoice->addInvoice($aPayData, $typ);
                $this->set('invoiceId',$invoiceId);
                App::import("Controller", "Email");
                $oEmailController = new EmailController();
                $oEmailController->prepareMailByInvoiceID(23, $invoiceId);
                $this->Session->setFlash("Спасибо, заявка на пополнение депозита принята и будет обработана в ближайшее время.");
            } else {
                $this->Session->setFlash("Внутренняя ошибка, попробуйте позже!");
                $this->redirect(array("controller" => "home", "action" => "index"));
            }
        }
    }

    /*@method: withdrawal
    *Add invoice for withdrawal deposit (typ = 3)
    *@params: no params
    *@author: Svirsky Evgeny
    *@date: 31.03.2013
    */
    public function withdrawal($typ = 3) {
        $max_sum = $this->Account->getDepositBalance();
        $this->set('paysystem', $this->Invoice->getAllActivPaysystem());
        $this->set('max_sum', $max_sum);
        if($this->request->is("post")) {
            $full_user_info = $this->UserInfo->getUserFullInfo(USER_ID);
            $aTransfer = array(
                1 => "",
                2 => "1",
                4 => "2",
                5 => "3",
                7 => "4",
                8 => "5",
                15 => "6",
                16 => "7",
                17 => "8"
            );
            /*Проверяем, если не указаны реквизиты для вывода средств, кидаем ошибку*/
            if (!$full_user_info['pay_details' . $aTransfer[$this->data['addInvoice']['payment_system_id']]]) {
                $this->Session->setFlash("Укажите реквизиты для вывода средств!");
                $this->redirect(array("controller" => "payment", "action" => "withdrawal"));
            } else {
                $aPaySystemData = $this->Invoice->getPaysystemById($this->data['addInvoice']['payment_system_id'], $typ);
                if($aPaySystemData) {
                    $iPaySum = $this->request->data['addInvoice']['paysum'];
                    if($aPaySystemData['fr_payment']['currency_code'] == 'EUR')
                    {
                        $DollarRate = $this->MTQ->getDollarRate();
                        $iPaySum = ceil($iPaySum / ($DollarRate + 0.0040));
                    }
                    if ($iPaySum > $max_sum) {
                        $this->Session->setFlash("Вы не можете вывести больше имеющейся суммы, попробуйте еще раз!");
                        $this->redirect("/payment/withdrawal");
                    }
                    $aPayData = array('sum' => $iPaySum, 'payments_id' => $aPaySystemData['fr_payment']['id'], 'user_id' => USER_ID);
                    /*added number of withdrawal type = 8*/
                    $invoiceId = $this->Invoice->addInvoice($aPayData, $typ, 8);
                    $this->set('invoiceId',$invoiceId);
                    App::import("Controller", "Email");
                    $oEmailController = new EmailController();
                    $oEmailController->prepareMailByInvoiceID(7, $invoiceId);
                    $this->Session->setFlash("Спасибо, заявка на вывод средств принята и будет обработана в ближайшее время.");
                    $this->redirect("/payment/result");
                } else {
                    $this->Session->setFlash("Внутренняя ошибка, попробуйте позже!");
                    $this->redirect(array("controller" => "home", "action" => "index"));
                }
            }


        }
    }

    /*@method: transfer
    *Transfer money to checked account
    *@params: no params
    *@author: Svirsky Evgeny
    *@date: 03.04.2013
    */
    public function transfer() {
        $aUserHistoryAccount = $this->UserAccount->getHistoryOfAccount();
        if ($this->request->is("post")) {
            $sum = (float)$this->request->data['sum'];
            $deposit_account = $this->request->data['deposit_account'];
            $aType = array("demo" => "6", "agent" => "7");
            if (!$sum || !$deposit_account) {
                $this->Session->setFlash("Пожалуйста, проверьте введенные данные.");
                $this->redirect("/payment/transfer");
            }
            $type = "";
            switch ($deposit_account) {
                case "demo" :
                    if ($sum > $aUserHistoryAccount['demoAccount']['deposit']) {
                        $this->Session->setFlash("Вы не можете снять больше, чем имеется на Торговом счету");
                        $this->redirect("/payment/transfer");
                    } else {
                        $type = "demo";
                        break;
                    }
                case "agent" :
                    if ($sum > $aUserHistoryAccount['agentAccount']['deposit']) {
                        $this->Session->setFlash("Вы не можете снять больше, чем имеется на Партнёрском счету");
                        $this->redirect("/payment/transfer");
                    } else {
                        $type = "agent";
                        break;
                    }
            }
            if ($type) {
                $invoiceId = $this->Invoice->addInvoice(array('sum' => $sum, 'payments_id' => '1', 'user_id' => USER_ID), 3, $aType[$type]);
                App::import("Controller", "Email");
                $oEmailController = new EmailController();
                $oEmailController->prepareMailByInvoiceID(7, $invoiceId);
                $this->Session->setFlash("Спасибо, заявка на перевод средств принята и будет обработана в ближайшее время.");
                $this->redirect("/payment/result");
            } else {
                $this->Session->setFlash("Внутренняя ошибка, поробуйте снова.");
            }

        }
        $this->set("fulluserinfodemo", $aUserHistoryAccount['demoAccount']);
        $this->set("fulluserinfoagent", $aUserHistoryAccount['agentAccount']);

    }

    public function result($message = "") {
        $this->set("message", $message);
    }
}
