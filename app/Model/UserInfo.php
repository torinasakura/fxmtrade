<?php
  
  App::uses('AppModel', 'Model');
  
  class UserInfo extends AppModel
  {
    public $name = 'UserInfo';
    
    public $primaryKey = 'id';
    public $useTable = 'user_info_typ';
    
    public function getExtraUserInfoType($iUserID)
    {
      $aReturn = null;
    
      $query = "SELECT type as id, value, type, max(add_date) as last_add_date FROM fr_user_info WHERE user_id = $iUserID GROUP BY type";
      $aResults = $this->query($query);
    
      if($aResults != null && is_array($aResults))
        $aReturn = $aResults;
      else
        CakeLog::write('info', 'No userinfos for user with id: ' .$iUserID);
        
      return $aReturn;
      
      
    }

    /*@method: setExtraUserInfoType
    *setting the status of user
    *@params:
    *$string - input string
    *$format - format to convert like d.m.Y
    *@author: Svirsky Evgeny
    *@date: 30.03.2013
    */
    public function setExtraUserInfoType($aUserData) {
        if (is_array($aUserData) && $aUserData) {
            $now = time();
            $query = "INSERT INTO `fr_user_info` (user_id, value, type, add_date) VALUES ('{$aUserData['user_id']}','{$aUserData['value']}','{$aUserData['type']}',$now)";
            $this->query($query);
        }
    }
	
	public function getUserFullInfo($iUserID)
    {
      $aReturn = null;
	            				
	  $query = "Select fr_user.pay_details,fr_user.pay_details1,fr_user.pay_details2,fr_user.pay_details3,fr_user.pay_details4,fr_user.pay_details5,fr_user.pay_details5_1,fr_user.pay_details5_2,fr_user.pay_details5_3,fr_user.pay_details5_4,fr_user.pay_details6,fr_user.pay_details7, fr_user.pay_details8, fr_user.skype, fr_user.tel, fr_user.last_name, fr_user.first_name, fr_user.lastvisitDate,
  fr_user.user_email, fr_user.address, fr_user.zip_code, fr_user.city,
  fr_user_settings.partner_id, fr_user.userParent_id, fr_user.country,
  fr_user.birthday, fr_user.newsletter, fr_user.access, cc.cn,
  fr_user_settings.trading_account, fr_user_settings.agent_account,
  fr_user_settings.status_id, fr_user_settings.programm_typ, fr_user.sex,
  p1.value As phone1, p2.value As phone2, p3.value As phone3, f.value As fax,
  n.value As nickname, fr_user_info.value As skypename,
  COALESCE(fr_invoice_registered.status_id, 0) As invoice_status_id,
  fr_user_settings.status_id As user_status_id, fr_user.user_registered,
  fr_user.activation, (Select ROUND((n.rgt - n.lft - 1) / 2)
  From fr_user_tree As n, fr_user_tree As p
  Where n.lft Between p.lft And p.rgt And n.user_id = fr_user.id Group By
    n.user_id) As countpartner, parent.last_name As parent_last_name,
  parent.first_name As parent_first_name, parent.id As parent_id
From fr_user Inner Join
  fr_user_settings On fr_user.id = fr_user_settings.user_id Inner Join
  cc On fr_user.country = cc.cc Join
  fr_user_info As p1 On fr_user.id = p1.user_id And p1.type = 1 Left Join
  fr_user_info As p2 On fr_user.id = p2.user_id And p2.type = 2 Left Join
  fr_user_info As p3 On fr_user.id = p3.user_id And p3.type = 3 Left Join
  fr_user_info As f On fr_user.id = f.user_id And f.type = 4 Left Join
  fr_user_info As n On fr_user.id = n.user_id And n.type = 6 Left Join
  fr_user_info On fr_user.id = fr_user_info.user_id And fr_user_info.type = 5
  Left Join
  fr_invoice_registered On fr_user.id = fr_invoice_registered.user_id Left Join
  fr_user parent On fr_user.userParent_id = parent.id
Where fr_user.id = $iUserID
Group By parent.last_name, parent.first_name, parent.id, fr_user.id
Order By fr_user.id";
      $sql = $this->query($query);
    
      if($sql != null && is_array($sql))
	  {
		  //$status_name = $this->UserStatusInfo->calculateUserStatus($sql);
		  //var_dump($sql);
		  foreach ($sql as $myrow)
		 {	
			$totalPaydPartner = ($myrow[0]['countpartner']) ? $myrow[0]['countpartner'] : 0;
			$statusInfo = $this->getUserStatusInfo($myrow['fr_user_settings']['status_id'], $totalPaydPartner);
			$filename = USERSPATH . $myrow['fr_user_settings']['partner_id']  . DS . 'avatar_' . $myrow['fr_user_settings']['partner_id'] . '.jpg';
			if (file_exists($filename))
			{
				$avatar = '/users/' .$myrow['fr_user_settings']['partner_id']. '/avatar_' . $myrow['fr_user_settings']['partner_id'] . '.jpg';
			} else {
				$avatar = ($myrow['fr_user']['sex']) ? '/img/user_male_big.png' : '/img/user_female_big.png';
			}
            $trading_account = ($myrow['fr_user_settings']['trading_account'])? $myrow['fr_user_settings']['trading_account'] : 'нет';
			$agent_account = ($myrow['fr_user_settings']['agent_account'])? $myrow['fr_user_settings']['agent_account'] : 'нет';
			$aResults = array (
							'last_name' => $myrow['fr_user']['last_name'],
							'first_name' => $myrow['fr_user']['first_name'],
							'user_email' => $myrow['fr_user']['user_email'],
							'user_registered' => $myrow['fr_user']['user_registered'],
							'sex' => $myrow['fr_user']['sex'],
							'address' => $myrow['fr_user']['address'],
							'zip_code' => $myrow['fr_user']['zip_code'],
							'city' => $myrow['fr_user']['city'],
							'country_code' => $myrow['fr_user']['country'],
							'birthday' => $myrow['fr_user']['birthday'],
							'newsletter' => $myrow['fr_user']['newsletter'],
							'country_name' => $myrow['cc']['cn'],
							'parent_last_name' => $myrow['parent']['parent_last_name'],
							'parent_first_name' => $myrow['parent']['parent_first_name'],
							'parent_full_name' => $myrow['parent']['parent_first_name'].' '.$myrow['parent']['parent_last_name'],
							'userParent_id' => $myrow['fr_user']['userParent_id'],
							'partner_id' => $myrow['fr_user_settings']['partner_id'],
							'trading_account' => $trading_account,
							'agent_account' => $agent_account,
							'status_id' => $myrow['fr_user_settings']['status_id'],
							'phone1' => $myrow['p1']['phone1'],
							'phone2' => $myrow['p2']['phone2'],
							'phone3' => $myrow['p3']['phone3'],
							'fax' => $myrow['f']['fax'],
							'nickname' => $myrow['n']['nickname'],
							'skypename' => $myrow['fr_user_info']['skypename'],
							'avatar' => $avatar,
							'status_name' => $statusInfo['status_name'],
                            'skype' => $myrow['fr_user']['skype'],
                            'tel' => $myrow['fr_user']['tel'],
                            'pay_details' => $myrow['fr_user']['pay_details'],
                            'pay_details1' => $myrow['fr_user']['pay_details1'],
                            'pay_details2' => $myrow['fr_user']['pay_details2'],
                            'pay_details3' => $myrow['fr_user']['pay_details3'],
                            'pay_details4' => $myrow['fr_user']['pay_details4'],
                            'pay_details5' => $myrow['fr_user']['pay_details5'],
                            'pay_details5_1' => $myrow['fr_user']['pay_details5_1'],
                            'pay_details5_2' => $myrow['fr_user']['pay_details5_2'],
                            'pay_details5_3' => $myrow['fr_user']['pay_details5_3'],
                            'pay_details5_4' => $myrow['fr_user']['pay_details5_4'],
                            'pay_details6' => $myrow['fr_user']['pay_details6'],
                            'pay_details7' => $myrow['fr_user']['pay_details7'],
                            'pay_details8' => $myrow['fr_user']['pay_details8'],
							'access' => $myrow['fr_user']['access'] );
	  	  }
		  $aReturn = $aResults;
	  }
	  else
        CakeLog::write('info', 'No userinfos for user with id: ' .$iUserID);
        
      return $aReturn;      
    }
	
	public function getUserStatusInfoInter($totalPaydPartner)
	{
		$query = "SELECT fr_user_status.status_name, fr_user_status.icon, fr_user_status.info_text,
					fr_user_status.total_partner FROM fr_user_status
					WHERE total_partner >= " .$totalPaydPartner. " ORDER BY id LIMIT 1";
		$row = $this->query( $query );
		
		return $statusInfo = array (
									'status_name' => $row[0]['fr_user_status']['status_name'],
									'status_icon' => $row[0]['fr_user_status']['icon'],
									'status_infotext' => $row[0]['fr_user_status']['info_text'] );
	}
	
	public function getUserStatusInfo($statusId, $totalPaydPartner)
	{
		if ( $statusId != 0 )
		{
			$query = "	SELECT fr_user_status.status_name, fr_user_status.icon, fr_user_status.info_text,
								fr_user_status.total_partner FROM fr_user_status
								WHERE id = " .$statusId. " ORDER BY id LIMIT 1";
			$row = $this->query( $query );
			if($totalPaydPartner < $row[0]['fr_user_status']['total_partner'])
			{
				return $statusInfo = array (
									'status_name' => $row[0]['fr_user_status']['status_name'],
									'status_icon' => $row[0]['fr_user_status']['icon'],
									'status_infotext' => $row[0]['fr_user_status']['info_text'] );
			} else {
				return $this->getUserStatusInfoInter($totalPaydPartner);
			}
		} else {
			return $this->getUserStatusInfoInter($totalPaydPartner);
		}
	}

    /*@method: getAllCountries
    *getting all countries for select value => option_name
    *@params:
    *@author: Svirsky Evgeny
    *@date: 12.05.2013
    */
    public function getAllCountries() {
        $countries = array();
        $query = "SELECT cc, cn FROM `cc`";
        $results = $this->query($query);
        foreach($results as $result) {
            $countries[$result['cc']['cc']] = $result['cc']['cn'];
        }
        return $countries;
    }
  }
?>