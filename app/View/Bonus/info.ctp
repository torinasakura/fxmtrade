<div id="dialog" title="Basic dialog"></div>
<div id="'.$keyType.'_tabs">
			<div id="masseg_box" style="display: none; margin: 30px 0 0 0;"></div>
			<div class="fxr-table-header">
			  <div>
				<span class="fxr-button add_button" id="'.$keyType.'_add_button" prefix="'.$keyType.'">Подтвердить</span>
				<span class="fxr-button delete_button" id="'.$keyType.'_delete_button" prefix="'.$keyType.'">Удалить</span>
			  </div>
				<span id="'.$keyType.'_ajax_img" class="fr-ajax-img">
					<span id="magbox" class="fr-table-heder-msgbox">Ваш запрос обрабатывается.</span>
					<img style="position:relative; left: -50px;" src="/img/css/wp-image/ajax-loader_small.gif" alt="ajax-loader" />
				</span>
			</div>
			<table  class="zebra" id="'.$keyType.'_invoice_table">
				<thead> 
				  <tr>
					<th style="width: 40px;">#</th>
					<th style="width: 80px;">Дата</th>
					<th>Фамилия Имя</th>
					<th style="width: 60px;">Уровень</th>
					<th style="width: 100px;">Кол-во</th>
					<th style="width: 40px;">БЦ</th>
					<th style="width: 65px;">Состояние</th>
				  </tr>
				</thead>
				<tbody>
                <?php
				  $i = 1;
				  foreach ($MainInfo as $MainBonusInfo)
				  {
					echo '
					  <tr>
						<td>' .$i. '</td>
						<td> ' .strftime("%d.%m.%Y", strtotime($MainBonusInfo["fr_bonus_tables"]["add_date"])). '</td>
						<td><span id="' .$MainBonusInfo["fr_user"]["user_id"]. '" class="link">' .$MainBonusInfo["fr_user"]["first_name"]. ', '.$MainBonusInfo["fr_user"]["last_name"].'</span></td>
						<td>' .$MainBonusInfo["fr_bonus_tables"]["lavel"]. '</td>
						<td></td>
						<td></td>
						<td></td>
					  </tr>
					';
					++$i;
				  }
				 ?>
				</tbody>
			</table>
		  </div>