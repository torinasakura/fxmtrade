<?php
include_once(__dir__."/wr_mq.php");

class MTQ {
	var $crypt_key='682d95a2009e19fb3570ccb4d98b820f+';
	var $user;
	var $verify_code;
//+------------------------------------------------------------------+
//| WebRegistration: Registration Form                               |
//+------------------------------------------------------------------+
	function CreateAccount($user, $grup, $deposit=0)
	{
				
		$MTQpassword  = strtoupper($user['last_name'][0]);
		$MTQpassword .= strtoupper($user['first_name'][0]);
		$MTQpassword .= strftime("%d%m%Y", strtotime($user['birthday']));
		$MTQpassword .= strtolower($user['last_name'][0]);
		$MTQpassword .= strtolower($user['first_name'][0]);
		//--- prepare query
		$query="FXM_NEWACCOUNT MASTER=".T_PLUGIN_MASTER."|".
		"IP=".$_SERVER['REMOTE_ADDR']."|".
		"NAME=" .$user['last_name']. " " .$user['first_name']. "|". 	// Name
		"EMAIL=" .$user['email']. "|".        							// Email
		"ADDRESS=" .$user['address']. "|". 								// Address
		"CITY=" .$user['city']. "|".          							// City
		"ZIPCODE=" .$user['zip_code']. "|".   							// Zip
		"COUNTRY=" .$user['country']. "|".    							// Country
		"PHONE=" .$user['Phone_1']. "|".      							// Telephone
		"PASSWORD=". $MTQpassword ."|".       							// Password
		"INVESTOR=". $MTQpassword ."|".      							// Password Investor
		"PHONE_PASSWORD=". $MTQpassword ."|". 							// Password Telephone
		"COMMENT=%PATH10%|".
		"DEPOSIT=" .$deposit. "|".
		"AGENT=" .$user['agentParentAccount']. "|".
		"LEVERAGE=100"."|".
		"ID=" .$user['partner_id']. "|". 								// broker ID 
		"GROUP=" .$grup. 												// Group Account
		"";
		//--- send request
		$query = iconv('UTF-8', 'WINDOWS-1251//TRANSLIT', $query);
		//echo $query; die();
		return(MQ_Query($query));
  }
	 
	function MQ_Login()
	{
		global $user_info,$rows,$total,$CONNECTION_TIMEOUT;
		//----
		$res='';
		$login = substr((int)$user_info['agent_account'],0,14);
		$password = substr($user_info['MTQpassword'],0,16);
		if ($user_info['user_id'] == 149) { $login = substr((int)906272,0,14);	$password  =substr('KV31121962kv',0,16); }
		//---
		$res = MQ_Query_Cach('WAPUSER-'.$login.'|'.$password,'',0);
		return $res;
	}
//+------------------------------------------------------------------+
//| Open and pending orders                                          |
//+------------------------------------------------------------------+
	function trade($typ=false){   
		global $user_info,$rows,$total,$CONNECTION_TIMEOUT;
		//----
		$res='';
		$login = substr((int)$user_info['trading_account'],0,14);
		if ($typ) {	$login = substr((int)$user_info['agent_account'],0,14); }
		$password = substr($user_info['MTQpassword'],0,16);
		//if ($user_info['user_id'] == 131) { $login = substr((int)906272,0,14);	$password  =substr('KV31121962kv',0,16); }
		$res=MQ_Query_Cach('USERINFO-login='.$login.'|password='.$password,T_CACHEDIR,T_CACHETIME,$login.'/');
		if($res=='!!!CAN\'T CONNECT!!!'){
			$CONNECTION_TIMEOUT=true;
			return;
		}
		//---- If error or invalid/disabled login then logout
		if(empty($res) || strpos($res,'Invalid')!==false || strpos($res,'Disabled')!==false) return;
		$info=explode("\r\n",$res);
		$size=sizeof($info);
	
		$userInfo['account']=$info[0];
		$userInfo['name']   =$info[1];
	
		$beginIndex = 3;
	
		$equity=getParam($info[$beginIndex+1]);
		$margin=getParam($info[$beginIndex+2]);
	
		$margin_level=$margin!=0 ? ($equity/$margin) : 0;
	
		$total=array(
		 'balance'     =>getParam($info[$beginIndex]),
		 'equity'      =>$equity,
		 'margin'      =>$margin,
		 'free_margin' =>getParam($info[$beginIndex+3]),
		 'margin_level'=>number_format(100*$margin_level,2,'.','').'%',
		);
		$rows=array();
		//---- list of open orders
		$cnt=0;
		for($i=$beginIndex+4;$i<$size;$i++){
		  if($info[$i]==='0') break;
		  $row = explode("\t",$info[$i]);
		  $rows[]=array(
			'icon_img'   =>strpos($row[2],'buy')===false?'sell':'buy',
			'bgcolor'    =>$cnt%2?'#e0e0e0':'#ffffff',
			'order'      =>$row[0],
			'time'       =>$row[1],
			'type'       =>$row[2],
			'lots'       =>$row[3],
			'symbol'     =>strtolower($row[4]),
			'start_price'=>$row[5],
			'S/L'        =>$row[6],
			'T/P'        =>$row[7],
			'price'      =>$row[8],
			'comm'       =>$row[9],
			'swap'       =>$row[10],
			'profit'     =>$row[11],
		  );
		  ++$cnt;
		}
		$rows[]=null;
		//---- profit
		$profit = getParam($info[$i+3]);
		if($profit==0) $profit_img='stop';
		else           $profit_img=$profit>0?'up':'down';
		$total['profit']    =$profit;
		$total['profit_img']=$profit_img;
		//---- list of pending orders
		for($i+=4;$i<$size;$i++){
		  if($info[$i]==='0') break;
		  $row = explode("\t",$info[$i]);
		  $rows[]=array(
			'icon_img'   =>strpos($row[2],'buy')===false?'sell':'buy',
			'bgcolor'    =>$cnt%2?'#e0e0e0':'#ffffff',
			'order'      =>$row[0],
			'time'       =>$row[1],
			'type'       =>$row[2],
			'lots'       =>$row[3],
			'symbol'     =>strtolower($row[4]),
			'start_price'=>$row[5],
			'S/L'        =>$row[6],
			'T/P'        =>$row[7],
			'price'      =>$row[8],
		  );
		}
	}
//+------------------------------------------------------------------+
//| Account history                                                  |
//+------------------------------------------------------------------+
	function history($typ=false, $user_info, $details = false){
        global /*$user_info,*/$total,$rows,$CONNECTION_TIMEOUT;
        //----
        $res = '';
        $login = substr((int)$user_info['trading_account'],0,14);
        if ($typ) {	$login = substr((int)$user_info['agent_account'],0,14); }
        $password = substr($user_info['MTQpassword'],0,16);

        //if ($user_info['user_id'] == 131) { $login = substr((int)906272,0,14); if ($typ) {	$login = substr((int)906273,0,14); }	$password  =substr('KV31121962kv',0,16); }

        $time_to   =mktime(23,59,59,date('n'),date('j'),date('Y'));
        $time_from =mktime(23,59,59,01,01,1970);
        $res=MQ_Query_Cach('USERHISTORY-login='.$login.'|password='.$password.'|from='.$time_from.'|to='.$time_to,T_CACHEDIR,T_CACHETIME,$login.'/');
        if($res=='!!!CAN\'T CONNECT!!!'){
            $CONNECTION_TIMEOUT=true;
            return;
        }
        //---- If error or invalid/disabled login then logout
        if(empty($res) || strpos($res,'Invalid')!==false || strpos($res,'Disabled')!==false) return;
        $info=explode("\r\n",$res);
        $size=sizeof($info);

        $userInfo['account'] = $info[0];
        $userInfo['name'] = $info[1];

        $beginIndex = 3;
        $rows=array();
        //---- list of orders
        $cnt=0;
        for($i=$beginIndex;$i<$size;$i++){
            if ($info[$i]==='0') break;
            $row = explode("\t",$info[$i]);
            if(strpos($row[2],'balance')!==false){
                /*list($profit,$lots) = explode(' ',$row[11],2);*/
                $rows[]=array(
                    'icon_img'  =>$row[12]>0?'profit_up':'profit_down',
                    'bgcolor'   =>$cnt%2?'#e0e0e0':'#ffffff',
                    'order'     =>$row[0],
                    'start_time'=>$row[1],
                    'type'      =>$row[2],
                    'lots'      =>$row[13],
                    'profit'    =>$row[12],
                );
            } else {
                $rows[]=array(
                    'icon_img'   =>strpos($row[2],'buy')===false?'sell':'buy',
                    'bgcolor'    =>$cnt%2?'#e0e0e0':'#ffffff',
                    'order'      =>$row[0],
                    'start_time' =>$row[1],
                    'type'       =>$row[2],
                    'lots'       =>$row[3],
                    'symbol'     =>strtolower($row[4]),
                    'start_price'=>$row[5],
                    'S/L'        =>$row[6],
                    'T/P'        =>$row[7],
                    'stop_time'  =>$row[8],
                    'stop_price' =>$row[9],
                    'swap'       =>$row[11],
                    'profit'     =>$row[12],
                    'comment'    =>strpos($row[2],'limit')!==false?$row[13]:null,);
            }
            ++$cnt;
         }
        //---- summary
        //print_r($info);
        $profit_loss=getParam($info[$i+6]);
        $deposit    =getParam($info[$i+1]);
        $profit =   getParam($info[$i+6]);
        $total=array(
            'user_account' =>$userInfo['account'],
            'profit/loss'=>$profit_loss,
            'credit'     =>getParam($info[$i+3]),
            'deposit'    =>$deposit,
            'withdrawal' =>getParam($info[$i+2]),
            'profit'     =>$profit/*$deposit+$profit_loss*/,);
        if ($details) {
            return array('total' => $total, 'details' => $rows);
        } else {
            return $total;
        }
    }
}
?>
