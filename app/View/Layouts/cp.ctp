<!DOCTYPE html>
<html>
<head>
  <!-- Always force latest IE rendering engine or request Chrome Frame -->
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <link href="../../stylesheets/application.css" media="screen" rel="stylesheet" type="text/css" />
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
  	<?php echo $this->Html->charset(); ?>
	<?php echo $this->Html->meta("description", "Investoria Plus Controlcenter");?>
	<?php echo $this->Html->meta("keywords", "Forex");?>
	
	<title>
		Investoria Plus
	</title>	
		
	<?php
		if(isset($additiona_header_information)){
			echo $additiona_header_information;
		}
	?>
    	<script type="text/javascript" src="https://www.google.com/jsapi?key=IJ-TE2aYjkIGbHdhHpOjAnT6"></script>
        <script type="text/javascript">
        google.load("jquery", "1");
        google.load("jqueryui", "1");
        </script>
        <script type="text/javascript" src="/js/wp/tbscript.js"></script>
        <script src="/js/wp/jquery.cookie.js" type="text/javascript"></script>
        <script src="/js/wp/jquery.treeview.js" type="text/javascript"></script>

</head>
<body>
<!-- START MENU -->
	<?php echo $this->element('navigation-f'); ?>
	<?php echo $this->element('navigation-s'); ?>
	
	


<!-- END MENU -->

<section id="main">
<!-- START CONTENT -->
<?php echo $this->fetch('content'); ?>
<!-- END CONTENT -->
    <div class="row-fluid">
  <div class="span12">
    <div class="footer">
      <div>2013 &copy; FXM Financial Group</div>
      <div>Carefully crafted by <a href="http;//vk.com/torinasakura" target="_blank">Andrew Ghostuhin</a></div>
    </div>
  </div>
</div>
  </div>
</section>

<script type="text/html" id="template-notification">
  <div class="notification animated fadeInLeftMiddle fast{{ item.itemClass }}">
    <div class="left">
      <div style="background-image: url({{ item.imagePath }})" class="{{ item.imageClass }}"></div>
    </div>
    <div class="right">
      <div class="inner">{{ item.text }}</div>
      <div class="time">{{ item.time }}</div>
    </div>

    <i class="icon-remove-sign hide"></i>
  </div>
</script>
<script type="text/html" id="template-notifications">
  <div class="container">
    <div class="row" id="notifications-wrapper">
      <div id="notifications" class="{{ bootstrapPositionClass }} notifications animated">
        <div id="dismiss-all" class="dismiss-all button blue">Закрыть</div>
        <div id="content">
          <div id="notes"></div>
        </div>
      </div>
    </div>
  </div>
</script>

<script src="../../javascripts/application.js" type="text/javascript"></script><script src="../../javascripts/docs.js" type="text/javascript"></script><script src="../../javascripts/docs_charts.js" type="text/javascript"></script><script src="../../javascripts/documentation.js" type="text/javascript"></script><script src="../../javascripts/prettify.js" type="text/javascript"></script><link href="../../stylesheets/prettify.css" media="screen" rel="stylesheet" type="text/css" />
<!-- <script type="text/javascript">
    $(function(){
        Notifications.push({
            imagePath: "../../images/cloud.png",
            text: "<p><b>Добрый день.</b></p><div>Вы зашли в панель администрирования.</div>",
            autoDismiss: 2
        });
    });
</script> -->
<script type="text/javascript">
    prettyPrint()
</script>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-35778683-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<script type="text/javascript" src="/js/tableutil.js"></script>
<script type="text/javascript" src="/js/tablescript.js"></script>
<script type="text/javascript" src="/js/invoicescript.js"></script>
<script type="text/javascript" src="/js/accountscript.js"></script>
<script type="text/javascript" src="/js/userscript.js"></script>
</body>
</html>