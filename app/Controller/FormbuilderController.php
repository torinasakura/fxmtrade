<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

//import the mail controller handling
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class FormbuilderController extends AppController {

    public $name = 'Formbuilder';
	public $components = array('Sendemail','UserAccount');
	public $uses = array('User', 'Email', 'Form', 'UserInfo');
	private $apiSet = array('http://fxmtrade.com' => 'E5HZFKJ0YLWXKSDN38','http://investoriaplus.com' => 'SSATI66W23FF7DFF');

	private $lang = array(
					'ru-ru' => array(
						'form_label' => array(
							'sex'			=> 'Обращение',
							'last_name'		=> 'Фамилия',
							'first_name' 	=> 'Имя',
							'user_email' 	=> 'Email',
							'address' 		=> 'Домашний адрес',
							'zip_code' 		=> 'Индекс',
							'city' 			=> 'Город',
							'country' 		=> 'Страна',
							'birthday'		=> 'Дата рождения',
							'account_option'=> 'Выбор счета',
							'base_currency' => 'Базовая Валюта',
							'leverage' 		=> 'Торговое плечо',
							'initial_deposit'=>'Первоначальный депозит',
							'source_capital'=>'Источник капитала',
							'experience'	=>'Опыт работы на товарной/фондовой бирже',
							'exchange_experience'=>'Опыт совершения торговых операций'
						),
						'message' => array(
							'invalid' 		=> 'К сожалению, неверно заполнены обязательные поля.<br />',
							'try_again'		=> 'Заполните правильно форму и отправите запрос заново.<br />',
							'mail_send'		=> 'На Ваш электронный адрес было отправлено уведомление.<br />',
							'checkbox1'		=> 'Вы должны согласиться с Регламентом торговли, Уведомлением о рисках и Политикой конфиденциальности компании FXM Broker Group S.A.',
							'checkbox2'		=> 'Подтвердите точность и доcтоверность введенной Вами информации.'
						)
					),
					'en-en' => array(
						'form_label' => array(
							'sex'			=> 'Appeal',
							'last_name'		=> 'Last Name',
							'first_name' 	=> 'Name',
							'user_email' 	=> 'Email',
							'address' 		=> 'Home address',
							'zip_code' 		=> 'Postal code',
							'city' 			=> 'City',
							'country' 		=> 'Country',
							'birthday'		=> 'Birthday',
							'account_option'=> 'Account option',
							'base_currency' => 'Base currency',
							'leverage' 		=> 'Leverage',
							'initial_deposit'=>'Initial deposit',
							'source_capital'=>'Source capital',
							'experience'	=>'Experience in the commodity / stock exchange',
							'exchange_experience'=>'Experience making deals'
						),
						'message' => array(
							'invalid' 		=> 'Unfortunately, the required fields is incorrect.<br />',
							'try_again'		=> 'Fill out the form correctly and send the request again.<br />',
							'mail_send'		=> 'The mail has been sent successfully.<br />',
							'checkbox1'		=> 'You must agree to the Rules of Business, Risk Disclosure and Privacy Policy of FXM Broker Group SA.',
							'checkbox2'		=> 'Confirm the accuracy and reliability of information you have entered.'
						)
					)
	);


	private $label = array(
					'form' => array(
							'element_0' 	=> array('name' => 'sex', 			'ru' => 'Обращение',		'en' => 'Appeal'),
							'element_1' 	=> array('name' => 'last_name', 	'ru' => 'Фамилия',			'en' => 'Last Name'),
							'element_2' 	=> array('name' => 'first_name', 	'ru' => 'Имя',				'en' => 'Name'),
							'element_3' 	=> array('name' => 'user_email', 	'ru' => 'Email',			'en' => 'Email'),
							'element_6_1' 	=> array('name' => 'address',	 	'ru' => 'Домашний адрес',	'en' => 'Home address'),
							'element_6_2'	=> array('name' => 'zip_code',	 	'ru' => 'Индекс',			'en' => 'Postal Code'),
							'element_6_3' 	=> array('name' => 'city', 			'ru' => 'Город',			'en' => 'City'),
							'element_6' 	=> array('name' => 'country', 		'ru' => 'Страна',			'en' => 'Country'),
							'element_8' 	=> array('name' => 'birthday', 		'ru' => 'Дата рождения',	'en' => 'Birthday'),

							'element_9' 	=> array('name' => 'account_option', 		'ru' => 'Выбор счета',				'en' => 'Account option'),
							'element_10' 	=> array('name' => 'base_currency', 		'ru' => 'Базовая валюта',			'en' => 'Base currency'),
							'element_11' 	=> array('name' => 'leverage', 				'ru' => 'Торговое плечо',			'en' => 'Leverage'),
							'element_12' 	=> array('name' => 'initial_deposit', 		'ru' => 'Первоначальный депозит',	'en' => 'Initial deposit'),
							'element_13' 	=> array('name' => 'source_capital', 		'ru' => 'Источник капитала',		'en' => 'Source capital'),
							'element_14' 	=> array('name' => 'experience', 			'ru' => 'Опыт работы на товарной/фондовой бирже',	'en' => 'Experience in the commodity / stock exchange'),
							'element_15' 	=> array('name' => 'exchange_experience', 	'ru' => 'Опыт совершения торговых операций',	'en' => 'Experience making deals'),
                            'element_17'    => array('name' => 'flname',                'ru' => 'Имя и Фамилия',                        'en' => 'First Name and Last Name'),
						)
	);

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */

	/**
	 *ajax based function
	 */
	public function prepareMailInformation($emailID){
		$return = null;
		$sql = $this->Email->getMailInformation($emailID);

		if($sql != null && is_array($sql))
		$return = $sql;
		return $return;
	}
	public function index(){
		$action = null;
		$aJsonOutput = null;
		$this->autoRender = false;
		$this->layout = "ajax";

		if($this->request->is("post"))
		{

Configure::write('log', true);


            header('Access-Control-Allow-Origin: *');


			if(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) == 'ru'){$language = 'ru-ru';}else{$language = 'en-en';}
			//if(isset($this->request->data['language'])){$language = $this->request->data['language'];}
			if(isset($this->request->data['element_0'])){$sex = $this->request->data['element_0'];}else{$sex = false;}
			if(isset($this->request->data['element_1'])){$last_name = $this->request->data['element_1'];}else{$last_name = false;}
			if(isset($this->request->data['element_2'])){$first_name = $this->request->data['element_2'];}else{ $first_name = false;}
			if(isset($this->request->data['element_3'])){$user_email = $this->request->data['element_3'];}else{ $user_email = false;}
            if(isset($this->request->data['element_5'])){$skype = $this->request->data['element_5'];}else{ $skype = false;}
			if(isset($this->request->data['element_6_1'])){$address = $this->request->data['element_6_1'];}else{ $address = false;}
			if(isset($this->request->data['element_6_2'])){$zip_code = $this->request->data['element_6_2'];}else{ $zip_code = false;}
			if(isset($this->request->data['element_6_3'])){$city = $this->request->data['element_6_3'];}else{$city = false;}
			if(isset($this->request->data['element_6'])){$country = $this->request->data['element_6'];}else{$country = false;}
			if(isset($this->request->data['element_8'])){$birthday = $this->request->data['element_8'];}else{$birthday = false;}
            if(isset($this->request->data['element_7'])){$telData = $this->request->data['element_7'];}else{$telData = '';}
			if(isset($this->request->data['element_9'])){$account_option = $this->request->data['element_9'];}else{$account_option = false;}
			if(isset($this->request->data['element_10'])){$base_currency = $this->request->data['element_10'];}else{$base_currency = false;}
			if(isset($this->request->data['element_11'])){$leverage = $this->request->data['element_11'];}else{$leverage = false;}
			if(isset($this->request->data['element_12'])){$initial_deposit = $this->request->data['element_12'];}else{$initial_deposit = false;}
			if(isset($this->request->data['element_13'])){$source_capital = $this->request->data['element_13'];}else{$source_capital = false;}
			if(isset($this->request->data['element_14'])){$experience = $this->request->data['element_14'];}else{$experience = false;}
			if(isset($this->request->data['element_15'])){$exchange_experience = $this->request->data['element_15'];}else{$exchange_experience = false;}
            if(isset($this->request->data['element_16'])){$hash_url = $this->request->data['element_16'];}else{$hash_url = false;}
            if(isset($this->request->data['element_17'])){$flname = $this->request->data['element_17'];}else{$flname = false;}
            if(isset($this->request->data['element_18'])){$url = $this->request->data['element_18'];}else{$url = false;}
            if(isset($this->request->data['element_19'])){$contact_type = $this->request->data['element_19'];}else{$contact_type = false;}
            if(isset($this->request->data['element_21'])){$contact_time = $this->request->data['element_21'];}else{$contact_time = false;}
            if(isset($this->request->data['element_22'])){$description = $this->request->data['element_22'];}else{$description = false;}
            if(isset($this->request->data['element_23'])){$company = $this->request->data['element_23'];}else{$company = false;}
            if(isset($this->request->data['element_24'])){$post = $this->request->data['element_24'];}else{$post = false;}
            if(isset($this->request->data['element_25'])){$anketa_25 = $this->request->data['element_25'];}else{$anketa_25 = false;}
            if(isset($this->request->data['element_26'])){$anketa_26 = $this->request->data['element_26'];}else{$anketa_26 = false;}
            if(isset($this->request->data['element_27'])){$anketa_27 = $this->request->data['element_27'];}else{$anketa_27 = false;}
            if(isset($this->request->data['element_28'])){$anketa_28 = $this->request->data['element_28'];}else{$anketa_28 = false;}
            if(isset($this->request->data['element_29'])){$anketa_29 = $this->request->data['element_29'];}else{$anketa_29 = false;}
            if(isset($this->request->data['element_30_1'])){$anketa_30_1 = $this->request->data['element_30_1'];}else{$anketa_30_1 = false;}
            if(isset($this->request->data['element_30_2'])){$anketa_30_2 = $this->request->data['element_30_2'];}else{$anketa_30_2 = false;}
            if(isset($this->request->data['element_30_3'])){$anketa_30_3 = $this->request->data['element_30_3'];}else{$anketa_30_3 = false;}
            if(isset($this->request->data['element_30_4'])){$anketa_30_4 = $this->request->data['element_30_4'];}else{$anketa_30_4 = false;}
            if(isset($this->request->data['element_30_5'])){$anketa_30_5 = $this->request->data['element_30_5'];}else{$anketa_30_5 = false;}
            if(isset($this->request->data['element_31'])){$anketa_31 = $this->request->data['element_31'];}else{$anketa_31 = false;}
            if(isset($this->request->data['element_32'])){$anketa_32 = $this->request->data['element_32'];}else{$anketa_32 = false;}
            if(isset($this->request->data['element_33'])){$anketa_33 = $this->request->data['element_33'];}else{$anketa_33 = false;}
            if(isset($this->request->data['element_34'])){$anketa_34 = $this->request->data['element_34'];}else{$anketa_34 = false;}
            if(isset($this->request->data['element_35'])){$anketa_35 = $this->request->data['element_35'];}else{$anketa_35 = false;}
            if ($this->request->data['form_id'] == "3") {
                /*
                $this->request->data['element_9'] = "5";
                $this->request->data['element_10'] = "1";
                $this->request->data['element_11'] = "4";
                $this->request->data['element_12'] = "1000";
                $this->request->data['element_13'] = "4";
                */
                $account_option = "5";
                $base_currency = "1";
                $leverage = "4";
                $initial_deposit = "1000";
                $source_capital = "4";
            }

            $userData =
					array (
					'fr_user' => array(
						'sex' 			=> $sex,
						'last_name'		=> $last_name,
						'first_name' 	=> $first_name,
						'user_email' 	=> $user_email,
						'address' 		=> $address,
						'zip_code' 		=> $zip_code,
						'city' 			=> $city,
						'country' 		=> $country,
						'birthday'		=> $birthday,
						'account_option' => $account_option,
						'base_currency' => $base_currency,
						'leverage'		=> $leverage,
						'initial_deposit' => $initial_deposit,
						'source_capital' => $source_capital,
						'experience' => $experience,
						'exchange_experience' => $exchange_experience,
                        'skype' => $skype,
                        'tel' => $telData
						),
					'fxm_users_demo' => array(
						'sex' 			=> $sex,
						'last_name'		=> $last_name,
						'first_name' 	=> $first_name,
						'user_email' 	=> $user_email,
						'skype' 		=> $skype
						)
					);
			sleep(2);

			if (!$this->Form->data_escape($userData['fr_user']['user_email'], 'user_email')){
				$aJsonOutput = array("success" => 0, "err" => 1, "message" => "Email введён неверно!");
					die (json_encode($aJsonOutput));
			}

            $usrpwd = $this->UserAccount->createpwd();
            $user_pass = $usrpwd[0];
            //$user_pass = $this->Form->data_escape($userData['fr_user']['user_email'].microtime().$userData['fr_user']['last_name'], 'user_pass');

			if (!$user_pass){
				$aJsonOutput = array("success" => 0, "err" => 1, "message" => "Data is incorrect");
				die (json_encode($aJsonOutput));
			}else{
                $userData['fr_user']['user_pass_clr'] = $user_pass;
                $userData['fr_user']['user_pass'] = $usrpwd[1];
            }

            if ($this->apiSet[$_SERVER["HTTP_ORIGIN"]] == $this->request->data['api_id']){
				sleep(2);
				$result = NULL;
				switch($this->request->data['form_id']){
					case "1": //add user data demo accaunt FXM
						$mailID = 17;
						$userData['fr_user']['partner_id'] = 0;
						$this->User->addDemoUser($userData['fxm_users_demo']); //add user data to SQL Bank
						$result = true;
						break;

					case "2": //add user data real accaunt FXM
                        if($this->request->data['element_20_1'] != 1){
							$aJsonOutput = array("success" => 0, "err" => 1, "message" => "chk2".$this->lang[$language]['message']['checkbox2']);
							die (json_encode($aJsonOutput));
						}

						if($this->request->data['element_20_2'] != 1){
							$aJsonOutput = array("success" => 0, "err" => 1, "message" => "chk1".$this->lang[$language]['message']['checkbox1']);
							die (json_encode($aJsonOutput));
						}

						$valid_data = $this->Form->check_data_form_newuser($userData['fr_user']);
						//$this->log($valid_data);

						if(isset($valid_data['fr_user']['err'])){
							$form_label_name = null;
							foreach ($valid_data['fr_user']['err'] as $key => $value){
								$form_label_name .= '- '.$this->lang[$language]['form_label'][$value].'<br />';
							}
							$errMessage = array(
								"success" => 0,
								"err" => 1,
								"message" => $this->lang[$language]['message']['invalid'].$form_label_name.$this->lang[$language]['message']['try_again']
								);
						} else {
							$mailID = 18;
                            /*generate hash for partner url from random password 10 chars length*/
                            $userData['fr_user']['hash_url'] = md5($this->UserAccount->genpwd(10));

                            $user_id = $this->User->addRealUser($userData['fr_user']);

                            /*setting extra user type*/
                            $this->UserInfo->setExtraUserInfoType(array('user_id' => $user_id, 'value' => $telData, 'type' => '1'));

							$userData['fr_user']['partner_id'] = $this->User->addRealUserAdv($userData['fr_user'], $user_id);
							//$this->log("return: ".print_r($sid,1));
							$result = true;

						}
						break;
                    case "3":
                        if($this->request->data['element_20_1'] != 1){
                            $aJsonOutput = array("success" => 0, "err" => 1, "message" => "chk2".$this->lang[$language]['message']['checkbox2']);
                            die (json_encode($aJsonOutput));
                        }

                        if($this->request->data['element_20_2'] != 1){
                            $aJsonOutput = array("success" => 0, "err" => 1, "message" => "chk1".$this->lang[$language]['message']['checkbox1']);
                            die (json_encode($aJsonOutput));
                        }
                        $valid_data = $this->Form->check_data_form_newuser($userData['fr_user']);
                        //$this->log($valid_data);

                        /*checking for user with this hash url*/
                        if ($hash_url) {
                            $aResultSearch = $this->User->find('all', array("conditions" => array("hash_url" => $hash_url)));
                        }
                        if (isset($aResultSearch) && $aResultSearch[0]['User']['id']) {
                            $userData['fr_user']['userParent_id'] = $aResultSearch[0]['User']['id'];
                        } else {
                            /*в противном случае родитель будет пользователь под id=995*/
                            $userData['fr_user']['userParent_id'] = 995;
                        }

                        if(isset($valid_data['fr_user']['err'])){
                            $form_label_name = null;
                            foreach ($valid_data['fr_user']['err'] as $key => $value){
                                $form_label_name .= '- '.$this->lang[$language]['form_label'][$value].'<br />';
                            }
                            $errMessage = array(
                                "success" => 0,
                                "err" => 1,
                                "message" => $this->lang[$language]['message']['invalid'].$form_label_name.$this->lang[$language]['message']['try_again']
                            );
                        } else {
                            $mailID = 18;
                            /*generate hash for partner url from random password 10 chars length*/
                            $userData['fr_user']['hash_url'] = md5($this->UserAccount->genpwd(10));

                            $user_id = $this->User->addRealUser($userData['fr_user']);

                            /*setting extra user type*/
                            $this->UserInfo->setExtraUserInfoType(array('user_id' => $user_id, 'value' => $telData, 'type' => '1'));

                            $userData['fr_user']['partner_id'] = $this->User->addRealUserAdv($userData['fr_user'], $user_id);
                            //$this->log("return: ".print_r($sid,1));
                            $result = true;

                        }
                    break;
                    case "4":
                        $c_types = array(0 => "Телефонный звонок", 1 => "Email", 2 =>"Skype");
                        $contact_type = $c_types[$contact_type];
                        $result = $this->User->addUserToWhiteList(array(
                            'user_email'      => $user_email,
                            'flname'          => $this->Form->data_escape($flname),
                            'company'         => $this->Form->data_escape($company),
                            'post'            => $this->Form->data_escape($post),
                            'site'            => $this->Form->data_escape($url),
                            'tel'             => $this->Form->data_escape($telData),
                            'contact_type'    => $this->Form->data_escape($contact_type),
                            'contact_time'    => $this->Form->data_escape($contact_time),
                            'description'     => $this->Form->data_escape($description)
                        ));
                        $mailID = 18;
                        $userData['fr_user']['partner_id'] = 131;
                        $result = true;
                    break;
                    case "5":
                        $c_types = array(0 => "Телефонный звонок", 1 => "Email", 2 =>"Skype");
                        $contact_type = $c_types[$contact_type];
                        $result = $this->User->addUserToIBList(array(
                            'user_email'      => $user_email,
                            'flname'          => $this->Form->data_escape($flname),
                            'company'         => $this->Form->data_escape($company),
                            'post'            => $this->Form->data_escape($post),
                            'site'            => $this->Form->data_escape($url),
                            'tel'             => $this->Form->data_escape($telData),
                            'contact_type'    => $this->Form->data_escape($contact_type),
                            'description'     => $this->Form->data_escape($description),
                            'anketa' => array(
                                '25' => $anketa_25,
                                '26' => $anketa_26,
                                '27' => $anketa_27,
                                '28' => $anketa_28,
                                '29' => $anketa_29,
                                '30_1' => $anketa_30_1,
                                '30_2' => $anketa_30_2,
                                '30_3' => $anketa_30_3,
                                '30_4' => $anketa_30_4,
                                '30_5' => $anketa_30_5,

                            )
                        ));
                        $mailID = 18;
                        $userData['fr_user']['partner_id'] = 131;
                        $result = true;
                        break;
                    case "8":
                        $c_types = array(0 => "Телефонный звонок", 1 => "Email", 2 =>"Skype");
                        $contact_type = $c_types[$contact_type];
                        $result = $this->User->addUserToPAMMList(array(
                            'user_email'      => $user_email,
                            'flname'          => $this->Form->data_escape($flname),
                            'company'         => $this->Form->data_escape($company),
                            'post'            => $this->Form->data_escape($post),
                            'site'            => $this->Form->data_escape($url),
                            'tel'             => $this->Form->data_escape($telData),
                            'contact_type'    => $this->Form->data_escape($contact_type),
                            'description'     => $this->Form->data_escape($description),
                            'anketa' => array(
                                '31' => $anketa_31,
                                '32' => $anketa_32,
                                '33' => $anketa_33,
                                '34' => $anketa_34,
                                '35' => $anketa_35
                            )
                        ));
                        $mailID = 18;
                        $userData['fr_user']['partner_id'] = 131;
                        $result = true;
                        break;
                    default: $result = false;
				}
				if($result){
					$emailInformation = $this->Email->getMailInformation($mailID);
                    $this->Sendemail->prepareSystemMail($emailInformation, $userData['fr_user']); //Send e-Mail to the user
					$aJsonOutput = array("success" => 1, "err" => 0, "message" => $this->lang[$language]['message']['mail_send']);
				}else{
					$aJsonOutput = $errMessage;
				}
			} else {
				$aJsonOutput = array("success" => 0, "err" => 1, "message" => "API Error");
			}


		}else{
			CakeLog::write("warning", "action 'null' was called without ajax request");
		}
		//die ('{"s":"1"}');
		echo json_encode($aJsonOutput);
	}

    /*@method: randomPassword
    *generate random password
    *@params:
    *$count - number of chars
    *@author: Svirsky Evgeny
    *@date: 02.04.2013
    */
    function randomPassword($count) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $count; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

}