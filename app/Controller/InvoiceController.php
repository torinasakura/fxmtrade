<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

//import the mail controller handling
App::import("Controller", "Email");
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class InvoiceController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Invoice';
	public $components = array('RequestHandler', 'MTQ', 'UserAccount');
/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Invoice', 'User', 'UserInfo' ,'UserTree', 'Bonus', 'Account');

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function listing() {
    /* get the amount of invoice, differ by type */
		$aInvoiceTypes = array("pay" => 1, "in" => 2, "out" =>3, "ref" => 4);
		$aInvoices = array();
		foreach($aInvoiceTypes as $key => $value)
		{
			$aInvoices[$value] = $this->Invoice->getInvoicesByType($value);
			$Invoices_ = '';
			if($aInvoices[$value] != null && is_array($aInvoices[$value]))
			{
				foreach($aInvoices[$value] as $Invoices)
				{
					$countMember = true;
					if(isset($Invoices["fr_bonus_tables"]["table_id"]))
						$countMember = ($this->Bonus->getQuantityOneLavel($Invoices["fr_bonus_tables"]["table_id"]) == 7)? true : false;
					$Invoices[] = array('countMember' => $countMember);
					$Invoices_[] = $Invoices;
				}
				$aInvoices[$value] = $Invoices_;
			}
		}
		

		//var_dump($aInvoices[1]);
		$this->set("aInvoiceTabsTypes", $aInvoiceTypes);
		$this->set("aInvoiceTabsContent", $aInvoices);
	}
	
	/**
	 *ajax based function
	 */
	public function delete_invoice($sInvoiceType)
	{
		$aJsonOutput = null;
		$this->autoRender = false;
		$this->layout = "ajax";
		
		switch($sInvoiceType){
			case "pay":
				$aJsonOutput = $this->delete_invoice_inpayment();
				break;
            case "in":
                $aJsonOutput = $this->delete_invoice_inpayment();
                break;
            case "out":
                $aJsonOutput = $this->delete_invoice_inpayment();
                break;
			case "ref":
			$aJsonOutput = $this->delete_invoice_inpayment();
			break;
			default:
				$aJsonOutput = array("messagegTyp" => "error", "message" => "Error Message.");
		}
		
		echo json_encode($aJsonOutput);
	}
	
	/**
	 *ajax based function
	 */
	private function delete_invoice_inpayment()
	{
		$aJsonOutput = array("messagegTyp" => "error", "message" => "error");
		
		if($this->request->is("ajax") && $this->request->is("post"))
		{
			/*
			array
				'invoice' => 
					array
						0 => string '1212' (length=4)
						1 => string '1209' (length=4)
			*/
			$aInvoiceIDs = $this->request->data["invoice"];
			
			$iUpdateRows = $this->Invoice->updateRejecteInvoices($aInvoiceIDs);
			if(count($iUpdateRows) > 0)
			{
				
				$iInsertRows = $this->Invoice->insertInvoicesHistory($aInvoiceIDs);
				if($iUpdateRows == $iInsertRows)
				{
					//sucess
					$aJsonOutput = array("messagegTyp" => "ok", "message" => "Заявка отклонена!");
				}
				else
				{
					//failure
					$aJsonOutput = array("messagegTyp" => "error", "message" => "Error Message Invoice Histroy entries are not the same as the update reject entries.");
				}
				
				$this->RequestHandler->setContent("jscon", 'application/json');
				$this->autoRender = false;
				$this->layout = "ajax";
			}
			else
				$aJsonOutput = array("messagegTyp" => "error", "message" => "Error Message Update on Reject was not done.");
			
		}
		else
			CakeLog::write("warning", "action 'delete_invoice' was called without ajax request");
		return $aJsonOutput;
	}
	
	
	/**
	 *ajax based function
	 */
	public function update_invoice($sInvoiceType)
	{
		$this->autoRender = false;
		$this->layout = "ajax";
		
		switch($sInvoiceType){
			/*paying for trade place*/
            case "pay":
                $aJsonOutput = $this->update_invoice_inpayment_tp();
                break;
            /*funding the deposit*/
            case "in":
                $aJsonOutput = $this->update_invoice_inpayment();
                break;
			case "ref":
                $aJsonOutput = $this->update_invoice_referencepayment();
				break;
			case "out":
                $aJsonOutput = $this->update_invoice_outpayment();
				break;
		}
        echo json_encode($aJsonOutput);
	}
	
	/*
	 *ajax based function
	 */
	private function update_invoice_outpayment()
	{
		$aJsonOutput = array("messagegTyp" => "error", "message" => "error");
		if(($this->request->is("ajax") && $this->request->is("post")))
		{
			$iInvoiceID = $this->request->data['id'];
			$fSum = $this->request->data['sum'];
			
			$aUser = $this->User->getUserFullInformationByInvoiceID($iInvoiceID);
			$iUserID = $aUser["fr_user"]["id"];
			$iPaymentID = $aUser["fr_payment"]["id"];
            $iNote = $aUser['fr_invoice_registered']['deposit_type'];
					
			//set invoice status to 2 
			$this->Invoice->updateAcceptedInvoices($iInvoiceID, $fSum);
					
			//add history entry
			$this->Invoice->insertInvoicesHistory(array($iInvoiceID));
				
			//fr_user_statement, INSERT invoice, sum, userid, typ 3
			$iTyp = 3;
			$this->Account->insertStatementUser($iInvoiceID, /*'-'.*/$fSum, $iUserID, $iTyp, $iNote);
            /*if case with type = 8 - decrease value of the sum on the equal value*/
            if ($iNote == '8') {
                $this->Account->insertStatementUser($iInvoiceID, '-'.$fSum, $iUserID, $iTyp, '6');
            }

			$aJsonOutput = array("messagegTyp" => "ok", "message" => "Успешно!");
			
		}
		return $aJsonOutput;
	}
	
	private function update_invoice_referencepayment()
	{
		$aJsonOutput = array("messagegTyp" => "error", "message" => "error");
		if(($this->request->is("ajax") && $this->request->is("post")))
		{
			$iInvoiceID = $this->request->data['id'];
			$fSum = $this->request->data['sum'];
			
			$aUser = $this->User->getUserFullInformationByInvoiceID($iInvoiceID);
			$iUserID = $aUser["fr_user"]["id"];
			$iPaymentID = $aUser["fr_payment"]["id"];
			
			if($iPaymentID == 13 || $iPaymentID == 14)
			{
				
				//set invoice status to 2 
				$this->Invoice->updateAcceptedInvoices($iInvoiceID, $fSum);
						
				//add history entry
				$this->Invoice->insertInvoicesHistory(array($iInvoiceID));
				
				//fr_user_statement, INSERT invoice, sum, userid, typ abhängig 13 -> 5 | 14 -> 4
				$iTyp = $iPaymentID == 13 ? 5 : 4;
				$this->Account->insertStatementUser($iInvoiceID, $fSum, $iUserID, $iTyp);
				$aJsonOutput = array("messagegTyp" => "ok", "message" => "erfolgreich!");
			}
			elseif ($iPaymentID == 12)
			{
				//set invoice status to 2 
				$this->Invoice->updateAcceptedInvoices($iInvoiceID, $fSum);
						
				//add history entry
				$this->Invoice->insertInvoicesHistory(array($iInvoiceID));
				$this->Account->insertStatementUser($iInvoiceID, $fSum, $iUserID, 6);
				$aJsonOutput = array("messagegTyp" => "ok", "message" => "erfolgreich!");
			}
			else
			{
				//Fatal error!!
			}
			
		}
		return $aJsonOutput;
	}
	
	/**
	 *ajax based function
	 */
	private function update_invoice_inpayment_tp()
	{
		$aJsonOutput = null;
		if(($this->request->is("ajax") && $this->request->is("post")))
		{
			$iInvoiceID = $this->request->data['id'];
			$fSum = $this->request->data['sum'];
            if($iInvoiceID > 0)
			{
				$aUser = $this->User->getUserFullInformationByInvoiceID($iInvoiceID);
				$iUserID = $aUser["fr_user"]["id"];
                $aParentInformation = $this->User->checkUserParentHasPaid($iUserID);
                //CakeLog::write('debug', 'aParentInformation: '.print_r($aParentInformation, true) );
                App::uses('CakeTime', 'Utility');
                /*making pass-through for check registration request*/
                $aParentAgentAccount = $this->User->getUserParentAgentAccount($iUserID);
                /*temp*/$aParentAgentAccount = array();
                $sOutPut = $this->addPay(array_merge($aUser, $aParentAgentAccount), $fSum);
                $aJsonOutput = array("messagegTyp" => "ok", "message" => $sOutPut);
                return $aJsonOutput;
                /*ending pass-through*/

                /*
				if($aParentInformation != null)
				{
					//check if parent of the user got paid already?
					if($aParentInformation["fr_invoice_registered"]["status_id"] == 2)
					{
						//has paid
						//test check : has parent_agent_account?
						$aParentAgentAccount = $this->User->getUserParentAgentAccount($iUserID);
						if($aParentAgentAccount["fr_user_parent_settings"]["trading_account"] > 0)
						{
							//normal go
							$sOutPut = $this->addPay(array_merge($aUser, $aParentAgentAccount), $fSum);
							CakeLog::write('debug', $sOutPut);
							$aJsonOutput = array("messagegTyp" => "ok", "message" => $sOutPut);
						}
						else
						{
							//fatal error, this should not exist
							CakeLog::critical("Users parent has no tradeing account. UserID: ".$iUserID);
							$aJsonOutput = array("messagegTyp" => "error", "message" => "Fataler Error bitte den Administrator kontaktieren. Critical Log betrachten");
						}
					}
					else 
					{
						//has not paid
						$sParentRegisterDate = $aParentInformation["fr_user_parent"]["user_registered"];
						$sRegisterDate = CakeTime::fromString($sParentRegisterDate);
						$currentDay = mktime();
						$iDaysAgo = floor(($currentDay - $sRegisterDate)/(60*60*24));
						
						//is parent registerdate older than 3 days?
						if($iDaysAgo > 3)
						{
							//put him to the next parent, who has paid
							$iParentID = $this->User->getNextParentHasPaid($$iUserID);
							if($iParentID > 0)
							{
								$this->User->setUserNewParent($iUserID, $iParentID);
								
								$aParentAgentAccount = $this->User->getUserParentAgentAccount($iUserID);
								$aUser["fr_user"]["userParent_id"] = $iParentID;
								
								//after putting other parent make a normal go
								$sOutPut = $this->addPay(array_merge($aUser, $aParentAgentAccount));
								CakeLog::write('debug', $sOutPut);
								$aJsonOutput = array("messagegTyp" => "ok", "message" => $sOutPut);
							}
							else
							{
								CakeLog::critical("Users parents has not paid: ".$iUserID);
								$aJsonOutput = array("messagegTyp" => "error", "message" => "Fataler Error bitte den Administrator kontaktieren. Critical Log betrachten");
							}
						}
						else
						{
							//output: wait till days left = 3 - $iDaysAgo
							$aJsonOutput = array("messagegTyp" => "ok", "message" => "Das Registrierungsdatum des Referals ist weniger als 3 Tage alt. Bitte Warten Sie noch ".(3-$iDaysAgo)." Tage");
						}
					}
				}*/
			}
		}
	}
	
	private function addPay($aUser, $fSum) {
		$sReturn = "";
        $aProgramm = $this->Invoice->getProgrammByID(1); // the programm id is 1 currently
        $fMinSum = $aProgramm["fr_programm"]["cost"] - $aProgramm["fr_programm"]["cost_account"]; //Мнимальная суммма программы
        $fDollarRate = 1;
		if($aUser["fr_payment"]["currency_code"] == "EUR"){
			$fDollarRate = $this->MTQ->getDollarRate();
		}
        $fSumDollar = $fSum * $fDollarRate;

		/*if ( $fMinSum > $fSumDollar )
		{ 
			// Если пречислели меньше чем минимальная сумма программы то счет не открывается
			$sReturn ="Eingegebene Summe war zu kleine";
			CakeLog::write('info', "The given sum was to small for user_id: ". $aUser["fr_user"]["id"]);
		}
		else
		{*/
			App::import("Vendor", "wr/class-mtq");
			
			$aExtraUserInfos = $this->UserInfo->getExtraUserInfoType($aUser["fr_user"]["id"]);
            $this->UserInfoTyp->addUserInfoTypeInformation($aExtraUserInfos);
            $aFormatMTQAccount = $this->MTQ->formatMTQAccountFromCakePHP($aUser, $this->UserInfoTyp->getUserInfoTyp());
            $fDeposit = $fSumDollar/* - $fMinSum*/;
			
			$myMTQ = new MTQ();
			$result = $myMTQ->CreateAccount($aFormatMTQAccount, 'demo_FR_USD', $fDeposit); // откытие торгового счета на сервере MT
			$result = explode("\r\n",$result);
			
			if(empty($result) || empty($result[0]) || $result[0]=='error' || $result[0] == 'ERROR')
			{ 
				$sReturn ="err002";
				CakeLog::write('info', "$sReturn");
			} else {
				$trading_account = explode("=", $result[1]);
				
				if(!is_array($trading_account)) 
				{
					$sReturn ="err003";
					CakeLog::write('info', "$sReturn");
				}
				else 
				{					
					$trading_account = $trading_account[1];
					$this->User->updateUserTradingSettings($trading_account, $aUser["fr_user"]["id"]);//Заносим номер торгового счета в базу
					
					$this->Invoice->insertStatement($aUser["fr_invoice_registered"]["id"], $aProgramm, $aUser["fr_user"]["id"]);
					$this->Invoice->updateAcceptedInvoices($aUser["fr_invoice_registered"]["id"], $fSum);
					$this->Invoice->insertInvoicesHistory(array($aUser["fr_invoice_registered"]["id"]));
					
					/*temporary disable check adding user*/
                    $parent = '131';
                    if($this->UserTree->addUser($parent/*$aUser["fr_user"]["userParent_id"]*/, $aUser["fr_user"]["id"]))
					{
                        sleep(15);
						$result = $myMTQ->CreateAccount($aFormatMTQAccount, 'FR_AGENT_USD' ); // откытие агентского счета
						$result = explode("\r\n",$result);
						
						
						if(empty($result) || empty($result[0]) || $result[0]=='error' || $result[0] == 'ERROR')
						{ 
							$sReturn ="err004";
							CakeLog::write('info', "$sReturn");
						}
						else
						{
							$agent_account = explode("=", $result[1]);
							if(!is_array($agent_account))
							{
								$sReturn ="err005";
								CakeLog::write('info', "$sReturn");
							}
							else 
							{
								$agent_account = $agent_account[1];
								$this->User->updateUserAgentSettings($agent_account, $aUser["fr_user"]["id"]);//Заносим номер торгового счета в базу
								
								
								/* temporary comment bonus loop
								* $sReturn = ($this->Bonus->addSeriesUser( $aUser));*/
								
								App::import("Controller", "Email");
								$oEmailController = new EmailController();
								$oEmailController->prepareMailByUserID(12,  $aUser["fr_user"]["id"], $aFormatMTQAccount);
								//fxr_send_post_mail('id', $myrow[0]->user_id, '432');
								//CakeLog::write('info', "$sReturn");
							}
						}
					}
				}
			}
		/*}*/
		
		return $sReturn;
	}
	
	public function get_invoice()
	{
        $this->autoRender = false;
		$this->layout = "ajax";
		
		if($this->request->is("ajax") && $this->request->is("post"))
		{
			$iInvoiceID = $this->request->data['id'];
			if($iInvoiceID > 0)
			{
                $aInvoice = $this->Invoice->getInvoiceByID($iInvoiceID);
				$this->set('aInvoice', $aInvoice[0]);

                /*получаем данные аккаунтов пользователя*/
                $aUserHistoryAccount = $this->UserAccount->getHistoryOfAccount(false, $aInvoice[0]['fr_invoice_registered']['user_id']);

                $this->set("fulluserinfodemo", $aUserHistoryAccount['demoAccount']);
                $this->set("fulluserinfoagent", $aUserHistoryAccount['agentAccount']);

				$this->render("modalinvoice");
			}
		}
		
	}
	
	public function paidlisting()
	{
		$this->set("aAllPaidInvoice", $this->Invoice->getAllPaidInvoice());
	}


    /*@method: update_invoice_inpayment
    *funding the deposit (submit the invoice without any logic). After submit admin manually does actions in mt4.
    *@params:
    *@author: Svirsky Evgeny
    *@date: 01.04.2013
    */
    public function update_invoice_inpayment() {
        $aJsonOutput = array("messagegTyp" => "error", "message" => "error");
        if(($this->request->is("ajax") && $this->request->is("post"))) {
            $iInvoiceID = $this->request->data['id'];
            $fSum = $this->request->data['sum'];
            $aUser = $this->User->getUserFullInformationByInvoiceID($iInvoiceID);
            $iUserID = $aUser["fr_user"]["id"];
            //$iPaymentID = $aUser["fr_payment"]["id"];

            //set invoice status to 2
            $this->Invoice->updateAcceptedInvoices($iInvoiceID, $fSum);

            //add history entry
            $this->Invoice->insertInvoicesHistory(array($iInvoiceID));

            //fr_user_statement, INSERT invoice, sum, userid, typ 3
            //$iTyp = 3;
            //$this->Account->insertStatementUser($iInvoiceID, '-'.$fSum, $iUserID, $iTyp);
            $aJsonOutput = array("messagegTyp" => "ok", "message" => "Подтверждено!");
        }
        return $aJsonOutput;
    }
}
