<div id="tabs" style="width: 100%;">
	<ul>
		<li><a href="#in">Пополнение</a></li>
		<li><a href="#ref">Внутренний переводы</a></li>
        <li><a href="#out">Вывод</a></li>
    </ul>
	<!--Start Tabs In -->
	<div id="in">
        <div id="masseg_box" style="display: none; margin: 30px 0 0 0;"></div>
        <div class="fxr-table-header">
            <div>
            	<span class="fxr-button delete_button" id="delete_button" prefix="in">Отменить</span>
            </div>
            <span id="ajax_img" class="fr-ajax-img">
            <span id="magbox" class="fr-table-heder-msgbox">Ваш запрос обрабатывается.</span>
               <img style="position:relative; left: -50px;" src="/img/css/wp-image/ajax-loader_small.gif" alt="ajax-loader" />
            </span>
		</div>
        <?php echo $this->Form->create('HistoryIn', array('url' => '/invoice/', 'id' => 'in_invoice_form')); ?>
        <table  class="zebra" id="in_invoice_table">
            <thead>
                <tr>
                    <th style="width: 65px;">#</th>
                    <th style="width: 80px;">Дата</th>
                    <th>Способ</th>
                    <th style="width: 100px;">Сумма</th>
                    <th style="width: 100px;">Состояние</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if(isset($aTabsContent['in']) && is_array($aTabsContent['in']))
			{
  				foreach ($aTabsContent['in'] as $aContent)
				{
					$disabled = ($aContent['fr_invoice_registered']['status_id'] == 1) ? '' : 'disabled="disabled"';				
				print('
				<tr>
                	<td>
						<input type="checkbox" name="invoice[]" value="'.$aContent['fr_invoice_registered']['id'].'" '.$disabled.' />
						<spsn style="vertical-align:2px; margin-left: 10px;">' .$aContent['fr_invoice_registered']['id']. '</span>
					</td>
					<td>' .strftime("%d.%m.%Y", strtotime($aContent['fr_invoice_history']['add_date'])).'</td>
					<td>' .$aContent['fr_payment']['name_payment_systems'].'</td>
					<td>' .$aContent['fr_invoice_registered']['sum'].' '.$aContent['fr_payment']['currency_code'].'</td>
					<td>' .$aContent['fr_invoice_status']['status_name'].'</td>
				</tr>
				');
				}
			}
			?>
            </tbody>
            <input type="hidden" name="action" id="in_action" value="" />
            </form>
    	</table>
    </div>
    <!-- End Tads In -->
    <!--Start Tabs Ref -->
	<div id="ref">
        <div id="masseg_box" style="display: none; margin: 30px 0 0 0;"></div>
        <div class="fxr-table-header">
            <div>
            	<span class="fxr-button delete_button" id="delete_button" prefix="">Отменить</span>
            </div>
            <span id="ajax_img" class="fr-ajax-img">
            <span id="magbox" class="fr-table-heder-msgbox">Ваш запрос обрабатывается.</span>
               <img style="position:relative; left: -50px;" src="/img/css/wp-image/ajax-loader_small.gif" alt="ajax-loader" />
            </span>
		</div>
        <?php echo $this->Form->create('HistoryRef'); ?>
        <table  class="zebra" id="in_invoice_table">
            <thead>
                <tr>
                    <th style="width: 65px;">#</th>
                    <th style="width: 80px;">Дата</th>
                    <th>Способ</th>
                    <th style="width: 100px;">Сумма</th>
                    <th style="width: 100px;">Состояние</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if(isset($aTabsContent['ref']) && is_array($aTabsContent['ref']))
			{
  				foreach ($aTabsContent['ref'] as $aContent)
				{
					$disabled = ($aContent['fr_invoice_registered']['status_id'] == 1) ? '' : 'disabled="disabled"';				
				print('
				<tr>
                	<td>
						<input type="checkbox" name="invoice[]" value="'.$aContent['fr_invoice_registered']['id'].'" '.$disabled.' />
						<spsn style="vertical-align:2px; margin-left: 10px;">' .$aContent['fr_invoice_registered']['id']. '</span>
					</td>
					<td>' .strftime("%d.%m.%Y", strtotime($aContent['fr_invoice_history']['add_date'])).'</td>
					<td>' .$aContent['fr_payment']['name_payment_systems'].'</td>
					<td>' .$aContent['fr_invoice_registered']['sum'].' '.$aContent['fr_payment']['currency_code'].'</td>
					<td>' .$aContent['fr_invoice_status']['status_name'].'</td>
				</tr>
				');
				}
			}
			?>
            </tbody>
            <input type="hidden" name="action" id="ref_action" value="" />
            </form>
    	</table>
    </div>
    <!-- End Tads Ref -->
    <!--Start Tabs Out -->
	<div id="out">
        <div id="masseg_box" style="display: none; margin: 30px 0 0 0;"></div>
        <div class="fxr-table-header">
            <div>
            	<span class="fxr-button delete_button" id="delete_button" prefix="">Отменить</span>
            </div>
            <span id="ajax_img" class="fr-ajax-img">
            <span id="magbox" class="fr-table-heder-msgbox">Ваш запрос обрабатывается.</span>
               <img style="position:relative; left: -50px;" src="/img/css/wp-image/ajax-loader_small.gif" alt="ajax-loader" />
            </span>
		</div>
        <?php echo $this->Form->create('HistoryRef'); ?>
        <table  class="zebra" id="in_invoice_table">
            <thead>
                <tr>
                    <th style="width: 65px;">#</th>
                    <th style="width: 80px;">Дата</th>
                    <th>Способ</th>
                    <th style="width: 100px;">Сумма</th>
                    <th style="width: 100px;">Состояние</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if(isset($aTabsContent['out']) && is_array($aTabsContent['out']))
			{
  				foreach ($aTabsContent['out'] as $aContent)
				{
					$disabled = ($aContent['fr_invoice_registered']['status_id'] == 1) ? '' : 'disabled="disabled"';				
				print('
				<tr>
                	<td>
						<input type="checkbox" name="invoice[]" value="'.$aContent['fr_invoice_registered']['id'].'" '.$disabled.' />
						<spsn style="vertical-align:2px; margin-left: 10px;">' .$aContent['fr_invoice_registered']['id']. '</span>
					</td>
					<td>' .strftime("%d.%m.%Y", strtotime($aContent['fr_invoice_history']['add_date'])).'</td>
					<td>' .$aContent['fr_payment']['name_payment_systems'].'</td>
					<td>' .$aContent['fr_invoice_registered']['sum'].' '.$aContent['fr_payment']['currency_code'].'</td>
					<td>' .$aContent['fr_invoice_status']['status_name'].'</td>
				</tr>
				');
				}
			}
			?>
            </tbody>
            <input type="hidden" name="action" id="ref_action" value="" />
            </form>
    	</table>
    </div>
    <!-- End Tads Out -->
</div>