<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="tabbable">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#pd">Персональные данные</a></li>
                    <li><a data-toggle="tab" href="#tab-2">Дополнительная информация</a></li>
                    <li><a data-toggle="tab" href="#tab-3">Пароль</a></li>
                    <li><a data-toggle="tab" href="#tab-4">eMail</a></li>
                    <li><a data-toggle="tab" href="#tab-5">Верификация аккаунта</a></li>
                    <!--<li><a href="#tab-6">Настройка аккаунта</a></li>-->
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="pd">
                        <div>
                            <p>В данном разделе Вы можете изменить личные данные.<br/>
                                В связи с тем что учётная запись (аккаунт) не может передаваться другому Пользователю,
                                соответственно,
                                изменение имени и/или фамилии недопустимо. Чтобы изменить имя и/или фамилию, указанные
                                при регистрации,
                                нужно заполнить <a href="#">формуляр</a>. Ваша заявка будет отправленна в отдел
                                финансового обслуживания
                                счетов, после рассмотрения вашей заявки пришлют Вам решение по электронной почте. В
                                некоторых случаях могут
                                попросить Вас прислать ксерокопию/фотокопию соответствующего документа.</p>

                            <div id="flashMessage" class="example_class"><?php echo $this->Session->flash(); ?></div>
                        </div>

                        <?php echo $this->Form->create('MemberData',array("url" => "/member/index", "type" => "file")); ?>
                        <div>
                            <?php foreach ($label['tabs1'] as $key => $value){
                                    $class_off = ($key == 'first_name' || $key == 'last_name') ? 'disabled' : '';?>
                            <div class="fieldwrapper">
                                <?php
                                        if ($key == 'country_name') {
                                        echo $this->Form->input("country", array('selected' => $fulluserinfo['country_code'], 'options' => $countries, 'disabled' => $class_off,'label' => array('text' => $value,'class' => 'styled input_medium'), 'div' => array('class' => 'thefield select')));
                                        } else {
                                        echo $this->Form->input($key, array('value' => $fulluserinfo[$key], 'disabled' => $class_off,'label' => array('text' => $value,'class' => 'styled input_medium'), 'div' => array('class' => 'thefield')));
                                        }
                                        ?>
                            </div>
                            <?php } ?>
                        </div>
                        <script type="text/javascript">
                            $(function () {
                                var input = $("input#MemberDataAvatar");
                                input.hide();
                                $("#file_browse_wrapper").click(function () {
                                    input.trigger("click");
                                });
                            });
                        </script>
                        <div class="yui3-u-1-3" style="width: 240px; padding:0 20px;">
                            <img alt="Avatar" src="<?php echo $fulluserinfo['avatar']; ?>" style="width: 128px;">
                            <?php //echo $this->Html->image(($fulluserinfo['sex']) ? 'user_male_big.png' : 'user_female_big.png', array('alt' => 'Avatar'))?>
                            <div style="position:relative;">
                                <div title="Выбрать" id="file_browse_wrapper" style="margin-top:6px;"></div>
                            </div>
                            <?php echo $this->Form->file('avatar'); ?>
                        </div>
                        <?php echo $this->Form->end('Сохранить'); ?>
                        <div class="yui3-u-1-3">

                        </div>
                    </div>

                    <div class="tab-pane" id="tab-2">
                        <?php echo $this->Form->create('MemberAdditionalData',array("url" => "/member/index")); ?>
                        <div class="yui3-u-1-3">
                            <?php foreach ($label['tabs2'] as $key => $value){
                                    $class_off = ($key == 'first_name' || $key == 'last_name') ? 'disabled' : ''; ?>
                            <div class="fieldwrapper">
                                <?php echo $this->Form->input($key, array('value' => $fulluserinfo[$key],'label' => array('text' => $value,'class' => 'styled input_medium'), 'div' => array('class' => 'thefield')));?>
                            </div>
                            <?php } ?>
                        </div>
                        <?php echo $this->Form->end('Сохранить'); ?>
                    </div>

                    <div class="tab-pane" id="tab-3">
                        <?php echo $this->Form->create('ChangePassword', array('url' => "/member/index")); ?>
                        <div class="yui3-u-1-3">
                            <div class="fieldwrapper">
                                <?php echo $this->Form->input('password', array('label' => array('text' => 'текущий пароль','class' => 'styled input_medium'), 'div' => array('class' => 'thefield')));?>
                            </div>
                            <div class="fieldwrapper">
                                <?php echo $this->Form->input('passwd', array('label' => array('text' => 'новый пароль','class' => 'styled input_medium'), 'div' => array('class' => 'thefield')));?>
                            </div>
                            <div class="fieldwrapper">
                                <?php echo $this->Form->input('psword', array('label' => array('text' => 'повторить пароль','class' => 'styled input_medium'), 'div' => array('class' => 'thefield')));?>
                            </div>
                        </div>
                        <?php echo $this->Form->end('Сохранить'); ?>
                    </div>

                    <div class="tab-pane" id="tab-4">
                        <?php echo $this->Form->create('ChangeEmail'); ?>
                        <div class="yui3-u-1-3">
                            <div class="fieldwrapper">
                                <?php echo $this->Form->input('password', array('label' => array('text' => 'текущий пароль','class' => 'styled input_medium'), 'div' => array('class' => 'thefield')));?>
                            </div>
                            <div class="fieldwrapper">
                                <?php echo $this->Form->input('newemail', array('label' => array('text' => 'Новый адрес','class' => 'styled input_medium'), 'div' => array('class' => 'thefield')));?>
                            </div>
                        </div>
                        <?php echo $this->Form->end('Сохранить'); ?>
                    </div>

                    <div class="tab-pane" id="tab-5">
                        <p>Верифицированный аккаунт обеспечит Вам максимальную безопасность и позволит пользоваться
                            дополнительными
                            услугами, связанными с финансовыми операциями, совершаемыми по Вашему счету.</p>
                        <b>Верифицированный аккаунт откроет Вам дополнительные возможности:</b>
                        <ul>
                            <li type="square"><span style="font-weight: normal;">выводить средства общим размером 1000 USD и более;</span>
                            </li>
                            <li type="square"><span style="font-weight: normal;">мгновенно переводить средства между
                                своими счетами и
                                счетами других участников
                            </li>
                            <li type="square"><span style="font-weight: normal;">использовать международный валютный банковский перевод для осуществления ввода средств</span>
                            </li>
                            <li type="square"><span style="font-weight: normal;">заказать купоны PayCard</span></li>
                        </ul>
                        <b>Для прохождения верификации необходимо предоставить следующие документы:</b>
                        <ul>
                            <li type="disc">Скан разворота паспорта с ФИО и личными данными его владельца</li>
                            <li type="disc">Скан разворота паспорта со страницей прописки или регистрации, либо другой
                                документ,
                                подтверждающий место проживания (счет за коммунальные услуги)
                            </li>
                        </ul>
                        <?php echo $this->Form->create('verification', array("url" => "/member/index", "type" => "file")); ?>
                        <div style="margin-bottom:6px;"><?php echo $this->Form->file('memberpass'); ?></div>
                        <div style="margin-bottom:6px;"><?php echo $this->Form->file('regdata'); ?></div>
                        <?php echo $this->Form->end('Отправить'); ?>
                    </div>
                    <!--<div  class="tab-pane" id="tab-6">-->

                    <!--</div>-->
                </div>
            </div>
        </div>
    </div>
</div>



