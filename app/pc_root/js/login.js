// JavaScript Document
jQuery(document).ready(function($) {		
	$(document).mouseup(function() {
		$("#loginform").mouseup(function() {
			return false
		});
		$("a.close").click(function(e){
			e.preventDefault();
			$("#loginform").hide();
			$(".lock").fadeIn();
		});
		$("input#cancel_submit").click(function(e){
			e.preventDefault();
			$("#loginform").hide();
			$(".lock").fadeIn();
		});
		if ($("#loginform").is(":hidden"))
		{
			$(".lock").fadeOut();
		} /*else {
			$(".lock").fadeIn();
		}*/				
		$("#loginform").fadeIn();
	});
  
  
	// I dont want this form be submitted
	$("input#signin_submit").click(function() {
		$("form#signin").submit();
	});
  
	$("#restore_submit").click(function() {
		var message = '';
		var err = false;
		var expression = new RegExp("^([a-zA-Z0-9!@#$%]{6,15})$");
		if (!expression.test($("#password").val()))
		{					
			$("#password").css('background', '#ffcccc');
			message = "Длина пароля должна быть 6 символа или более.&lt;br /&gt;";
			err = true;
		}
		if ($("#password").val() != $("#password_confirmation").val())
		{
			message = message + "Введенные вами пароли не совпадают.";
			$("#password_confirmation").css('background', '#ffcccc');
			err = true;
		}
		if (err)
		{
			$('.message').show().html(message).addClass('warning');
			return;
		}
		$("form#pass_restore").submit();
	});

    $("#resend_password_link").click(function(e) {
        e.preventDefault();
        var email = $("#username").val();
        var input = {"email" : email};
        $.ajax({
            data : input,
            url : "/login/recallpassword",
            type: "post"
        })
            .success(function(callback) {
                var output = JSON.parse(callback);
                if (output['success']) {
                    alert("Вам на e-mail отправлено письмо с новым паролем!");
                } else {
                    alert(output['error']);
                }
            });
    });
});// This is example of other button