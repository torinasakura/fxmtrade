<div class="navbar navbar-inverse">
<div class="navbar-inner">
<div class="container-fluid">
<a class="brand" href="#">
<small>
<i class="icon-leaf"></i>

</small>
</a>
<ul class="nav ace-nav pull-right">
<li class="light-blue user-profile">
<a class="user-menu dropdown-toggle" href="#" data-toggle="dropdown">
<img class="nav-user-photo" alt="Jason's Photo" src="assets/avatars/user.jpg">
<span id="user_info">
<small>Management</small>
Accaunt
</span>
<i class="icon-caret-down"></i>
</a>
<ul id="user_menu" class="pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
<li>
<a href="#">
<i class="icon-cog"></i>
Settings
</a>
</li>
<li>
<a href="#">
<i class="icon-user"></i>
Profile
</a>
</li>
<li class="divider"></li>
<li>
<a href="#">
<i class="icon-off"></i>
Logout
</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<div id="main-container" class="container-fluid">
<a id="menu-toggler" href="#">
<span></span>
</a>
<div id="sidebar" class="">
<div id="sidebar-shortcuts">
<div id="sidebar-shortcuts-large">
<button class="btn btn-small btn-success">
<i class="icon-signal"></i>
</button>
<button class="btn btn-small btn-info">
<i class="icon-pencil"></i>
</button>
<button class="btn btn-small btn-warning">
<i class="icon-group"></i>
</button>
<button class="btn btn-small btn-danger">
<i class="icon-cogs"></i>
</button>
</div>
<div id="sidebar-shortcuts-mini">
<span class="btn btn-success"></span>
<span class="btn btn-info"></span>
<span class="btn btn-warning"></span>
<span class="btn btn-danger"></span>
</div>
</div>
<ul class="nav nav-list">
<li>
<a href="index.html">
<i class="icon-dashboard"></i>
<span>Общие сведения</span>
</a>
</li>
<li>
<a href="typography.html">
<i class="icon-text-width"></i>
<span>Личные данные</span>
</a>
</li>
<li class="open">
						<a class="dropdown-toggle" href="#">
							<i class="icon-desktop"></i>
							<span>Платежи</span>

							<b class="arrow icon-angle-down"></b>
						</a>

						<ul class="submenu" style="display: block;">
							<li>
								<a href="elements.html">
									<i class="icon-double-angle-right"></i>
									Пополнение депозита
								</a>
							</li>

							<li>
								<a href="buttons.html">
									<i class="icon-double-angle-right"></i>
									Перевод средств на расчетный счет
								</a>
							</li>

							<li>
								<a href="treeview.html">
									<i class="icon-double-angle-right"></i>
									Вывод средств
								</a>
							</li>
                            
							<li>
								<a href="treeview.html">
									<i class="icon-double-angle-right"></i>
									История заявок
								</a>
							</li>
						</ul>
					</li>
<li>
<a href="tables.html">
<i class="icon-list"></i>
<span>Агентская сеть</span>
</a>
</li>
<li>
<a class="dropdown-toggle" href="#">
<i class="icon-edit"></i>
<span>Помощь</span>
</a>
</li>
<li>
<a href="widgets.html">
<i class="icon-list-alt"></i>
<span>Выход</span>
</a>
</li>
</ul>
<div id="sidebar-collapse">
<i class="icon-double-angle-left"></i>
</div>
</div>
<div id="main-content" class="clearfix">
<div id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="icon-home"></i>
<a href="#">Главная</a>
</li>
</ul>
<div id="nav-search">
<form class="form-search">
<span class="input-icon">
<input id="nav-search-input" class="input-small search-query" type="text" autocomplete="off" placeholder="Search ...">
<i id="nav-search-icon" class="icon-search"></i>
</span>
</form>
</div>
</div>
<div id="page-content" class="clearfix">
<div class="row-fluid">





<div class="row-fluid">
		<div class="span2">
			<table class="table table-striped table-bordered box">
			  <thead>
          <tr>
            <th colspan="2">Партнерский счёт: 1472129</th>
          </tr>
			  </thead>
			  <tbody>
          <tr>
            <td> Дата открытия счета:</td>
            <td><strong> 11.07.2013 </strong></td>
          </tr>
          <tr>
            <td>Депозит:</td>
            <td><strong>15.00 USD</strong></td>
          </tr>
          <tr>
            <td>Снятие:</td>
            <td><strong>0.00 USD</strong></td>
          </tr>
          <tr>
            <td>Баланс:</td>
            <td><strong>0.00 USD</strong></td>
          </tr>
          <tr>
            <td>Прибыль:</td>
            <td><strong>0.00 USD</strong></td>
          </tr>
			  </tbody>
			</table>
		</div>
    <div class="span2">
			<table class="table table-striped table-bordered box">
			  <thead>
          <tr>
            <th colspan="2">Торговый счёт: 1472128</th>
          </tr>
			  </thead>
			  <tbody>
          <tr>
            <td>Дата открытия счета:</td>
            <td><strong> 11.07.2013 </strong></td>
          </tr>
          <tr>
            <td>Депозит:</td>
            <td><strong>1000.00 USD</strong></td>
          </tr>
          <tr>
            <td>Снятие:</td>
            <td><strong>-100.00 USD</strong></td>
          </tr>
          <tr>
            <td>Баланс:</td>
            <td><strong>0.00 USD</strong></td>
          </tr>
          <tr>
            <td>Прибыль:</td>
            <td><strong>0.00 USD</strong></td>
          </tr>
			  </tbody>
			</table>
		</div>
    <div class="span2">
			<table class="table table-striped table-bordered box">
			  <thead>
          <tr>
            <th colspan="2">Расчётный счёт: 2048</th>
          </tr>
			  </thead>
			  <tbody>
          <tr>
            <td>Дата открытия счета:</td>
            <td><strong> 04.04.2012 </strong></td>
          </tr>
          <tr>
            <td>Снятие:</td>
            <td><strong>0.00 USD</strong></td>
          </tr>
          <tr>
            <td>Баланс:</td>
            <td><strong>450.00 USD</strong></td>
          </tr>
			  </tbody>
			</table>
		</div>
    <div class="span2">
			<table class="table table-striped table-bordered box">
			  <thead>
          <tr>
            <th colspan="2">Информация</th>
          </tr>
			  </thead>
			  <tbody>
          <tr>
            <td> Ваш статус:</td>
            <td><strong>Generaldirektor</strong></td>
          </tr>
          <tr>
            <td>Всего участников:</td>
            <td><strong>49</strong></td>
          </tr>
          <tr>
            <td>Оплатившие:</td>
            <td><strong>35</strong></td>
          </tr>
          <tr>
            <td>Не оплатившие:</td>
            <td><strong>14</strong></td>
          </tr>
			  </tbody>
			</table>
		</div>
	</div>




 </div>
</div>
</div>
</div>
<a id="btn-scroll-up" class="btn btn-small btn-inverse" href="#">
<i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
<script type="text/javascript">
<script src="assets/js/bootstrap.min.js">
<script src="assets/js/ace-elements.min.js">
<script src="assets/js/ace.min.js">