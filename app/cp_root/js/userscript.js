	jQuery(function($){
		
		
		$("#keyType_activation_mailsend_button").click(function(){
			send_mail();//checked go
		});
		
		$("#all_checkbox").change(function(){
			toggleCheckBoxes(".all-checkable");
		});
	});

	function send_mail()
	{
		var aCheckBox = get_selected_rows('#keyType_userlisting_notactive_table');
		if(aCheckBox.length > 0)
		{
			var aCheckedIDs = new Array();
			aCheckBox.each(function(){
				aCheckedIDs.push($(this).val());
			});
			ajaxRequestSendActivationMail(aCheckedIDs);
		}
		else
			errorOutput();
	}
  
  function ajaxRequestSendActivationMail(aUser_ID)
	{
		$.ajaxSetup({
			beforeSend	: function (xhr) { $('#keyType_ajax_img').show();  },
			complete : function (xhr){ $('#keyType_ajax_img').hide(); }
		})
		$.ajax({
			async    : false,
			dateType : 'json',
			type     : 'POST',
			url      : "/email/sendactivationmail",
			data		 : {"aIDs":aUser_ID},
			success  : function(data){
				return data;
			}				
		});
	}