<?php
  
  App::uses('AppModel', 'Model');
  
  class UserStatus extends AppModel
  {
    public $name = 'UserStatus';
  
    public $primaryKey = 'id';
    public $useTable = 'user_status';
    
    public function getListOfStatus() {
        $query = "SELECT id, status_name FROM `fr_user_status` ";
        $result = $this->query($query);
        return $result;
    }
    
  }
?>