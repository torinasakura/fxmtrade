<!DOCTYPE html>
<html>
<head>
  <?php echo $this->Html->charset(); ?>
  <?php echo $this->Html->meta("description", "Investoria Plus Controlcenter");?>
  <?php echo $this->Html->meta("keywords", "Forex");?>
  <?php echo $this->Html->css('screen'); ?>
  
  <meta charset="utf-8">
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <title>Авторизация участника проекта Investoria Plus</title>
  <link href="../../stylesheets/application.css" media="screen" rel="stylesheet" type="text/css" />
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
  
  <script type="text/javascript" src="https://www.google.com/jsapi?key=IJ-TE2aYjkIGbHdhHpOjAnT6"></script>
  <script type="text/javascript">
		google.load("jquery", "1");
	  	google.load("jqueryui", "1");
  </script>
  <?php echo $this->Html->script('login'); ?>
  
</head>
<body class="login">
<?php echo $this->fetch('content'); ?>
</body>
</html>
