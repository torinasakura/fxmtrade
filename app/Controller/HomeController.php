<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class HomeController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Home';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('User', 'UserInfo', 'UserTree', 'Account', 'Invoice');
	public $components = array('Session','MTQ','UserAccount', 'Util');
		
/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function index()
	{
        App::import("Vendor", "wr/class-mtq");
		/*getting count of users of current user reccurently*/
        $AllUsersOfCurrent = $this->User->getAllLowersOfUser(USER_ID);
        $AllUsersOfCurrentCount = count($AllUsersOfCurrent);
        $this->set("allusersofcurrent", $AllUsersOfCurrentCount);

        /*getting count of users who payed of current user*/
        $AllUsersOfCurrentPayed = $this->User->getAllLowersOfUserPayed($AllUsersOfCurrent);
        $AllUsersOfCurrentPayedCount = count($AllUsersOfCurrentPayed);
        $this->set("allusersofcurrentpayed", $AllUsersOfCurrentPayedCount);

        /*getting count of users who payed of current user*/
        $AllUsersOfCurrentPayedCount = $AllUsersOfCurrentCount - $AllUsersOfCurrentPayedCount;
        $this->set("allusersofcurrentunpayed", $AllUsersOfCurrentPayedCount);

        $partnerLink = $this->User->getPartnerLink(USER_ID);
        $this->set("partnerLink", $partnerLink );

        $partnerLinkr = $this->User->getPartnerLinkr(USER_ID);
        $this->set("partnerLinkr", $partnerLinkr );


        /*create MTQ object to get full info about current user from mt4*/
        if (ENVIRONMENT == 'pc') {
            $aUserHistoryAccount = $this->UserAccount->getHistoryOfAccount();
            $this->set("fulluserinfodemo", $aUserHistoryAccount['demoAccount']);
            $this->set("fulluserinfoagent", $aUserHistoryAccount['agentAccount']);
        }
        $aFullUserInfo = $this->UserInfo->getUserFullInfo(USER_ID);
        $this->set("regdatedep", $this->Util->convertDateToFormat($aFullUserInfo['user_registered'],"d.m.Y"));

        $iDepositBalance = $this->Account->getDepositBalance();
        $this->set("depbalance", $iDepositBalance);

        $iDepositWithdrawal = $this->Account->getDepositWithdrawal();
        $this->set("depwithdrawal", $iDepositWithdrawal);

        $iMemberCount = $this->UserTree->getTotalMembersOfUserCount(USER_ID); //131 is the rootuser off all members
        $this->set("membercount", $iMemberCount);
        
        $iAllUserCount = $this->User->getAllUsersCount();
        $this->set("allusercount", $iAllUserCount);
        
        $iAllUnpaidUserCount =  $this->User->getAllUnpaidUsersCount();
        $this->set("allunpaidusercount", $iAllUnpaidUserCount);
		
		$aRefAccount =  $this->Account->getUserStatement();
        $this->set("refaccaunt", $aRefAccount);
		
		$agent_account = $this->Session->read('UserInfo.agent_account') ? $this->Session->read('UserInfo.agent_account') : 'Нет';
		$this->set("agent_account", $agent_account);

		$partner_account = $this->Session->read('UserInfo.partner_account') ? $this->Session->read('UserInfo.partner_account') : 'Нет';
		$this->set("partner_account", $partner_account);


		if(ENVIRONMENT == 'cp')
		{
			$aAccounts =  $this->Account->getStatement();
       		$this->set("accaunts", $aAccounts);
			$today = $this->User->getAllUsersCount('today');
			$this->set("today", $today);
			$week = $this->User->getAllUsersCount('week');
			$this->set("week", $week);
			$month = $this->User->getAllUsersCount('month');
			$this->set("month", $month);
			$quarter = $this->User->getAllUsersCount('quarter');
			$this->set("quarter", $quarter);
			
			$payInvoices = count($this->Invoice->getInvoicesByType(1));
			$this->set("payInvoices", $payInvoices);
			$inInvoices = count($this->Invoice->getInvoicesByType(2));
			$this->set("inInvoices", $inInvoices);
			$internInvoices = count($this->Invoice->getInvoicesByType(4));
			$this->set("internInvoices", $internInvoices);
			$outInvoices = count($this->Invoice->getInvoicesByType(3));
			$this->set("outInvoices", $outInvoices);
			
		}
			
		$this->render(ENVIRONMENT."_index");
		
	}
}
