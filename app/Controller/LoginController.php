<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

//import the mail controller handling
App::import("Controller", "Email");

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class LoginController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Login';
    public $components = array('UserAccount');

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Login', 'User');

	public function index()
	{
    if (ENVIRONMENT == 'pc') {
		$this->layout = "login-pc";
        }
        else {
        $this->layout = "login";
        }
	}
	
	
	public function loginuser()
	{
		$this->autoRender = false;
		if($this->request->is("post"))
		{
			if(key_exists("username", $this->request->data) && !empty($this->request->data["username"]))
			{
				if(key_exists("password", $this->request->data) && !empty($this->request->data["password"]))
				{
					$aUser = $this->Login->loginUser($this->request->data["username"], $this->request->data["password"]);
					if($aUser != null)
					{
						$this->Session->write('isLogin', true);
						$this->Session->write('UserInfo', $aUser);
						$this->redirect('/');
					}
					else
					{print_r('Error003');/* error password and useremail not match!*/}
				}
				else
				{print_r('Error002');/* error password not exists*/}
			}
			else
			{print_r('Error001');/* error username not exists*/}
			
			
			//$this->redirect("/account/onetimebonuslisting/1");
		}
	}
	public function logout()
	{
		$this->Session->delete('isLogin');
		$this->Session->delete('UserInfo');
		$this->redirect('/');
	}

    public function recallpassword() {
        if($this->request->is("post")) {
            if ($result = $this->Login->getUserByEmail($this->request->data['email'])) {

                $iUserID = $result[0]['fr_user']['id'];
                if ($iUserID) {
                    $usrpwd = $this->UserAccount->createpwd();
                    $user_pass = $usrpwd[0];
                    if (!$user_pass){
                        $aJsonOutput = array("success" => 0, "error" => "Внутренняя проблема сервера, обратитесь к администратору");
                        die (json_encode($aJsonOutput));
                    } else {
                        $this->Login->setNewPassForUser($iUserID, $usrpwd[1]);
                        App::import('Controller', 'Email');
                        $Email = new EmailController;
                        $Email->sendMail($result[0]['fr_user']['user_email'], "Смена пароля", "<h1>Добрый день</h1><p>Ваш новый пароль для входа в личный кабинет: $user_pass</p>");
                    }
                } else {
                    $aJsonOutput = array("success" => 0, "error" => "Внутренняя проблема сервера, обратитесь к администратору");
                    die (json_encode($aJsonOutput));
                }

                $aJsonOutput = array("success" => 1);
            } else {
                $aJsonOutput = array("success" => 0, "error" => "Пользователь с таким логином не найден!");
            }
            echo json_encode($aJsonOutput);
            die();
        } else {
            echo "Err005";
        }
    }
}
