<?php
  
App::uses('AppModel', 'Model');

class Login extends AppModel
{
  public $name = 'Login';
  
  public $primaryKey = 'id';
  public $useTable = 'user';
  public $uses = array('User', 'Email', 'Form', 'UserInfo');
  
  
  public function loginUser($sUserEmail, $sUserPassword)
  {
    $aResults = null;
    
    $sUserEmail = $this->login_data_escape( $sUserEmail );
    $sUserPassword = $this->login_data_escape($sUserPassword, "password");
    
    //$aResults = $this->find('first', array( "conditions" => array("user_email" => $sUserEmail, "user_pass" => $sUserPassword), ));
    $query = "	Select fr_user.id, fr_user.last_name, fr_user.first_name,
			  	fr_user_settings.partner_id, fr_user_settings.trading_account, fr_user_settings.agent_account
				From fr_user Inner Join
				  fr_user_settings On fr_user.id = fr_user_settings.user_id
				Where fr_user.user_email = '" .$sUserEmail. "' And fr_user.user_pass = '" .$sUserPassword. "'";
	$sql = $this->query( $query );
      if($sql != null && is_array($sql) )
      foreach ($sql as $myrow){
		  $aResults = array ( 
		  					'id' => $myrow['fr_user']['id'],
							'last_name' => $myrow['fr_user']['last_name'],
							'first_name' => $myrow['fr_user']['first_name'],
							'partner_id' => $myrow['fr_user_settings']['partner_id'],
							'trading_account' => $myrow['fr_user_settings']['trading_account'],
							'agent_account' => $myrow['fr_user_settings']['agent_account'] );
	  }
	  $aReturn = $aResults;
    return $aReturn;
  }
  
  private   function login_data_escape($sValue, $typ = false)
  {
    $sValue = stripslashes($sValue);
    $sValue = htmlspecialchars($sValue);
    $sValue = trim($sValue);
    if ($typ == 'password'){
      $sValue = strrev(md5($sValue))."b3p6f";	
    }
    return $sValue;
  }

  /*сбиваем пароль для пользователя*/
  public function resetPassword($sEmail) {


  }

  public function getUserByEmail($sEmail) {
      $query = "SELECT * from fr_user WHERE user_email = '$sEmail'";
      $result = $this->query($query);
      return $result;
  }

  public function setNewPassForUser($iUserID, $user_pass) {
      $query = "UPDATE fr_user SET user_pass = '$user_pass' WHERE id=$iUserID";
      $this->query($query);
  }
}
?>