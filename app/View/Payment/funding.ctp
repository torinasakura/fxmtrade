<script type="text/javascript"
        src="http://investoriaplus.com/wp-content/themes/yoo_infinite_wp/warp/js/regexp.js"></script>
<script type="text/javascript"
        src="http://investoriaplus.com/wp-content/themes/yoo_infinite_wp/warp/js/valid.js"></script>
<script type="text/javascript"
        src="http://investoriaplus.com/wp-content/themes/yoo_infinite_wp/warp/js/fxf.js"></script>
<link rel="stylesheet" href="http://investoriaplus.com/wp-content/themes/yoo_infinite_wp/warp/js/validate.css"/>
<script>
$(function () {
    var $fxf = $('#InvoiceIndexForm');

    $fxf.validationEngine();
});
</script>

<div class="container-fluid">
<div class="row-fluid">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="smaller">
                    Пополнение депозита
                </h4>
            </div>
            <div class="widget-body">


                <div id="withdrawal_container">
                    <!--<div class="fxr-siderbar-header"><p>Пополнение депозита</p></div>-->
                    <div id="withdrawal_body">
                        <?php echo $this->Form->create('Invoice', array('url' => '/payment/funding')); ?>
                        <!-- <div class="warning">
                            <p>
                                Ваш предыдущий платеж за номером (Invoice No: <b></b>) все еще находится в обработке.<br />
                                Чтобы зарегистрировать платеж через другой способ оплаты, Вам необходимо отменить предыдущий платеж.<br />
                                Для этого Вам необходимо перейти в раздел "История заявок" в меню Платежи и там отменить этот платеж.<br />
                                По всем возникшим  вопросам обращайтесь к нам через службу поддержки. При обращении к нам, пожалуйста, указывайте номер вашего платежа (Invoice No: <b></b>).
                            </p>
                            <p>С уважением,<br />Отдел платежей FXM Financial Group</p>
                        </div> -->
                        <div id="message_board"></div>
                        <div class="yui3-g">
                            <div class="span6 box">


                                <?php
                                        foreach ($paysystem as $value)
                                        {?>
                                <div id="flashMessage"
                                     class="example_class"><?php echo $this->Session->flash(); ?></div>
                                <div class="area" id="ps<?php echo $value['fr_payment']['id'] ?>">
                                    <span class="paylogo pay_system<?php echo $value['fr_payment']['id'] ?>"></span>
                                <span>
                                    <input type="radio" style="margin-right: 10px;" name="data[addInvoice][payment_system_id]"
                                           value="<?php echo $value['fr_payment']['id'] ?>"/>
                                    <label><?php echo $value['fr_payment']['currency_code']. ' ' .$value['fr_payment']['name_payment_systems'] ?></label>
                                </span>
                                </div>
                                <?php } ?>
                                <div class="section_break"></div>
                                <p style="font-size:14px; font-weight: bold;">*Выберите удобную для вас платежную
                                    систему</p>
                            </div>

                            <div class="span6 box">
                                <?php foreach ($paysystem as $value)
                                        {?>
                                <?php
                                        if($value['fr_payment']['class'] == 'ep')
                                        {
                                        ?>
                                <div id="infoboxps<?php echo $value['fr_payment']['id'] ?>" class="step"
                                     style="display:none">
                                    <div style="margin: 0 0 10px 0;">
                                        Пополнить счет с
                                        кошелька <?php echo $value['fr_payment']['name_payment_systems'] ?> очень
                                        просто.<br/><br/>
                                        Для этого необходимо:<br/>
                                        Вам перевести сумму в платежной системе на номер
                                        кошелька <?php echo $value['fr_payment']['payee_purse'] ?><br/>
                                        валюта платежа: <b><?php echo $value['fr_payment']['currency_code'] ?></b><br/>

                                        <div class="section_break"></div>
                                        <div class="fieldwrapper medium">
                                            <label class="styled input">*сумма платежа:</label>

                                            <div class="thefield">
                                                <!-- <select class="input_small paysum" id="element_<?php echo $value['fr_payment']['id'] ?>" name="pay_sum">
                                                     <option value="1"><?php echo $sum[$value['fr_payment']['currency_code']][0] ?></option>
                                                     <option value="2"><?php echo $sum[$value['fr_payment']['currency_code']][1] ?></option>
                                                 </select>
                                                 -->
                                                <input id="element_<?php echo $value['fr_payment']['id'] ?>"
                                                       class="input_small paysum element text validate[custom[integer], required]"
                                                       type="text" style="width: 160px;" name="pay_sum" maxlength="11"
                                                       value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section_break"></div>
                                    <p>
                                        После нажатия на кнопку "Продолжить",<br/>
                                        -Вы потверждаете свое согласие на пополнение счета<br/>
                                        -Вам будет сгенерирован инвойс, который придет на Ваш адрес электронной
                                        почты.<br/>
                                        -Ваша заявка на пополнение счета будет немедленно обработана.<br/>
                                    </p>
                                </div>
                                <?php
                                        }
                                        elseif($value['fr_payment']['class'] == 'bank')
                                        {?>
                                <div id="infoboxps<?php echo $value['fr_payment']['id'] ?>" class="step"
                                     style="display:none">
                                    <div style="margin: 0 0 10px 0;">
                                        Валютный банковский перевод (Invoice)
                                        : <?php echo $value['fr_payment']['currency_code'] ?><br><br>
                                        Реквизиты для банковского перевода<br><br>
                                        Currency USD <br>
                                        Bank Name Bank of Valleta plc<br>
                                        Address 58,Zachary Street<br>
                                        City Valetta VLT 1130<br>
                                        Country Malta<br>
                                        Account Owner Surplus Finance S.A.<br>
                                        SWIFT VALLMTMT<br>
                                        IBAN MT31VALL22013000000040019382478<br>

                                        Currency EUR<br>
                                        Bank Name Bank of Valleta plc<br>
                                        Address 58,Zachary Street<br>
                                        City Valetta VLT 1130<br>
                                        Country Malta<br>
                                        Account Owner Surplus Finance S.A.<br>
                                        SWIFT VALLMTMT<br>
                                        IBAN MT54VALL22013000000040019382452<br>

                                        <div class="section_break"></div>
                                        <div class="fieldwrapper medium">
                                            <label class="styled input">*сумма платежа:</label>

                                            <div class="thefield">
                                                <!--<select class="input_small paysum" id="element_<?php echo $value['fr_payment']['id'] ?>" name="pay_sum">
                                                    <option value="1"><?php echo $sum[$value['fr_payment']['currency_code']][0] ?></option>
                                                    <option value="2"><?php echo $sum[$value['fr_payment']['currency_code']][1] ?></option>
                                                </select>
                                                -->
                                                <input id="element_<?php echo $value['fr_payment']['id'] ?>"
                                                       class="input_small paysum element text validate[custom[integer], required]"
                                                       type="text" style="width: 160px;" name="pay_sum" maxlength="11"
                                                       value=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section_break"></div>
                                    <p>
                                        После нажатия на кнопку "Продолжить",<br/>
                                        -Вы потверждаете свое согласие на пополнение счета<br/>
                                        -Вам будет сгенерирован инвойс, который придет на Ваш адрес электронной
                                        почты.<br/>
                                        -Ваша заявка на пополнение счета будет немедленно обработана.<br/>
                                    </p>
                                </div>
                                <?php
                                        }
                                        else
                                        {?>
                                <div id="infoboxps<?php echo $value['fr_payment']['id'] ?>" class="step"
                                     style="display:none">
                                    <div style="margin: 0 0 10px 0;">
                                        Оплата подарочным купоном: <?php echo $value['fr_payment']['currency_code'] ?>
                                        <br><br>

                                        <div class="section_break"></div>
                                        <div class="fieldwrapper medium">
                                            <label class="styled input">*Код купона:</label>

                                            <div class="thefield">
                                                <input id="element_3" class="input_small" type="text" name="element_3"
                                                       maxlength="255"
                                                       value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section_break"></div>
                                    <p>
                                        После нажатия на кнопку "Продолжить",<br/>
                                        -Вы потверждаете свое согласие на пополнение счета<br/>
                                        -Вам будет сгенерирован инвойс, который придет на Ваш адрес электронной
                                        почты.<br/>
                                        -Ваша заявка на пополнение счета будет немедленно обработана.<br/>
                                    </p>
                                </div>
                                <?php
                                        }
                                        } ?>
                                <div id="payinfo" style="display:none;">
                                    <!--Для того, чтобы на депозит Вашего торгового места была добавлена подушка безопасности, и Ваше прибыльное торговое место на бирже начало полноценно на Вас работать и приносить Вам так же первый вид пассивного дохода в проекте - ИНВЕСТИЦИОННУЮ ПРИБЫЛЬ, Вам необходимо будет пополнить депозит своего торгового места до стандарта - 1000$.<br /><br />
                                    Если Вы хотите пополнить депозит своего торгового места из заработанных в проекте денег или после прохождения Вами Главного бонусного цикла, просьба, сообщить нам об этом.-->
                                </div>

                            </div>
                        </div>
                        <input type="hidden" value="1" name="data[addInvoice][paysum]" id="paysum"/>
                        <div><input id="submit" type="submit" value="Продолжить"></div>


                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
</div>