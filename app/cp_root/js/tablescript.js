	var sorter;	
	
	jQuery(function($){
		if( $(".tinytable").length != 0)
		{		
			sorter = new TINY.table.sorter('sorter','table',{
				headclass:'head',
				ascclass:'asc',
				descclass:'desc',
				evenclass:'evenrow',
				oddclass:'oddrow',
				evenselclass:'evenselected',
				oddselclass:'oddselected',
				paginate:true,
				size:10,
				colddid:'columns',
				currentid:'currentpage',
				totalid:'totalpages',
				startingrecid:'startrecord',
				endingrecid:'endrecord',
				totalrecid:'totalrecords',
				hoverid:'selectedrow',
				pageddid:'pagedropdown',
				navid:'tablenav',
				sortcolumn:2,
				sortdir:-1,
				//sum:[8],
				//avg:[6,7,8,9],
				//columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
				init:true
			});
		}
	});
	jQuery(function($){
		$("span.cancel").click(function(){
			var request = $.ajax({
			  url: "",
			  type: "POST",
			  data: {action : 'i_cancel'},
			  dataType: "html"
			});			
			request.done(function(data) {
				console.log( data );
			});
		});
		$("#navigation").treeview({
			persist: "location",
			collapsed: true,
			unique: true
		});
	});
	jQuery(function($){
		$("span.cancel").click(function(){
			alert('Функция временно отключена!');
		});
		
		function show(id, file){  
            $.ajax({  
                type: "POST",
				url: "content/ajax/"+file,  
                cache: false,
				data: "id="+id,
                success: function(html){
                    $("#dialog").html(html);
                }  
            });
        };
		
		function getContent(id, url){  
            $.ajax({  
                type: "GET",
				url: url,  
                cache: false,
				success: function(html){
                    $("#dialog").html(html);
                }  
            });
        };
		
		function action(id, action, summ, file){  
            $.ajax({  
                type: "POST",
				url: "content/ajax/"+file,  
                cache: false,
				data: { "id":id, "summ":summ, "action":action },
                success: function(result){
                    return result;
                }
            });
        };
		
		$("span.link, span.enroll, input[type=button]").click(function(){
			var id = $(this).attr('id');
			
			
			if ($(this).attr('name') == 'paid'){
				var a = {
					title: 'Зачисления денежных средств: '+$(this).html(),
					width: 320,
					buttons: {
						"Подтвердить": function() {
							var summ = $("[name='summ']").val();
							console.log( summ );
							action(id, 'add', summ, 'addpay.php');
							$( 'span#'+id ).html( '<img id="loading" style="display:none; margin-left: 10px; border: solid 1px #999;" alt="loading" src="/img/css/wp-image/ajax-loader.gif">');
							$( this ).dialog( "close" );
						}
					},
					close: function() {$("#dialog").html('')},
				};
				show( id, 'addpay.php' );
			} else {
				var a = {
					title: 'Контактные данные: '+$(this).html(),
					width: 880,
					buttons: {},
					close: function() {$("#dialog").html('')},
				};
				getContent( id, '/user/userinfo/'+id );
			}
			
			$( "#dialog" ).dialog(a);
			//console.log( $(this).attr('id') );
		});
	});