	jQuery(function($){
		
		
		$(".structure-button").click(function(){
			pay_structure();
		});
		
		$('input[type=checkbox]').change(function(){
			checkStructureButtonToggle();
		});
	
	});

 function pay_structure_dialog(id)
  {
      console.log( id );
      var a = {
        title: 'Распределение средств инвойс: '+id,
        width: 335,
        close: function() {$("#dialog").html('')},
      };
      show_structur(id);
      $( "#dialog" ).dialog(a);
  };

  function show_structur(id)
  {
    $.ajax({  
      type: "POST",
			url: "/account/get_structur",  
      cache: false,
      data: "id="+id,
			success: function(html){
        $("#dialog").html(html);
      }  
    });
  }

  
  function ajaxRequestStructurPayConfirm(id, value)
	{
		$.ajaxSetup({
			beforeSend	: function (xhr) { $('#keyType_ajax_img').show();  },
			complete : function (xhr){ $('#keyType_ajax_img').hide(); }
		})
		$.ajax({
			async    : false,
			dateType : 'json',
			type     : 'POST',
			url      : "/account/payconfirm",
			data     : { "invoice_id":id, "value":value },
			success  : function(data){
				return data;
			}				
		});
	}
  
  function pay_structure()
  {
    if(check_one_row_selected('#keyType_statement_table'))
		{
			//prüfen obwirklich nur eine row selectiert ist
			var aCheckBox = get_selected_rows('#keyType_statement_table');
			if(aCheckBox.length == 1)
			{
				pay_structure_dialog($(aCheckBox[0]).attr('value'));
			}
			else{
				errorOutput();
			}
		}
		else{
			errorOutput();
		}
  }
  
  function checkStructureButtonToggle()
	{
		var countChecked = get_selected_rows('#keyType_statement_table');
		
		if(countChecked != null && (countChecked.length > 1 || checkStructureAlreadyPaid())){
			//disable add_button
			$('#keyType_structur_button').removeClass("structur-button").removeClass("fxr-button").addClass("disable_button");
			$('#keyType_structur_button').unbind("click");
		}
		else
		{
			//enable add_button
			$('#keyType_structur_button').removeClass("disable_button").addClass("add_button").addClass("fxr-button");
			$('#keyType_structur_button').unbind("click");
			$('#keyType_structur_button').bind("click", function(){
				pay_structure();
			});
		}
	}
	
	function checkStructureAlreadyPaid()
	{
		var isDone = false;
		var aCheckBox = get_selected_rows('#keyType_statement_table');
		if(aCheckBox.length == 1)
		{
			var sum_total = null;
			var id = $(aCheckBox[0]).val();
			$('input[value='+id+']').each(function(){
				var parent = $(this).parent().siblings('.sum-amount');
				sum = parseInt(parent.text());
				sum_total += sum;
			});
			
			if(sum_total===0)
				isDone = true;
		}
		
		return isDone;
	}