<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class EmailController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Email';

/**
 *
 * @var array
 */
	public $uses = array('Email', 'User', 'UserInfo', 'Invoice');

	public $components = array('UserInfoTyp', 'MTQ', 'UserInfoTyp');

	private $testMailSend = false;

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
    public function index()
		{
			$this->set("aEmailInformation", $this->Email->getMailList());
			$this->render("index");
		}

		public function modify_email($iEmailID)
		{
			if(is_numeric($iEmailID))
			{
				$this->set("aModfyEmail", $this->Email->getMailInformation($iEmailID));
			}
			else
			{
				//$this->redirect("/email/new_email");
			}
		}

		public function save($iEmailID, $bAddNew = false)
		{
			$this->autoRender = false;
			if($this->request->is("post"))
			{
				$sReturnMessage = "";
				if(isset($iEmailID) && is_numeric($iEmailID))
				{
					//save the eding in the mail
					$sReturnMessage = $this->Email->saveEditMail($iEmailID, $this->request->data, USER_ID); //replace this with the id form the login user
				}
				else if(isset($iEmailID) && $iEmailID == "new")
				{
					//add a new mail
					$sReturnMessage = $this->Email->saveNewMail($this->request->data, USER_ID); //replace this with the id form the login user
				}
				else{
					//error no email selected
					$sReturnMessage = "FULL ERROR, NOT EMAIL SELECTED";
				}
				CakeLog::write("info", $sReturnMessage);
			}
			$this->redirect("/email");
		}




		/** mail sending logic**/

		public function sendtransactioninformationmail($aTransactionInfo)
		{
			//TODO: mail senden machen
			/*
			 array
  0 =>
    array
      'fr_user' =>
        array
          'id' => string '538' (length=3)
          'last_name' => string 'Spirkina' (length=8)
          'first_name' => string 'Tatyana' (length=7)
          'sex' => boolean false
      'fr_invoice_history' =>
        array
          'add_date' => string '2012-08-29 14:42:40' (length=19)
      'fr_user_statement_typ' =>
        array
          'name' => string 'Бонус за привлечение' (length=38)
      'fr_invoice_registered' =>
        array
          'sum' => string '515.00' (length=6)
      0 =>
        array
          'full_sum' => string '16.00' (length=5)
      'fr_user_settings' =>
        array
          'partner_id' => string '2454' (length=4)
  1 =>
    array
      'fr_user' =>
        array
          'id' => string '202' (length=3)
          'last_name' => string 'Trokhymenko' (length=11)
          'first_name' => string 'Stanislav' (length=9)
          'sex' => boolean true
      'fr_invoice_history' =>
        array
          'add_date' => string '2012-08-29 14:42:40' (length=19)
      'fr_user_statement_typ' =>
        array
          'name' => string 'Бонус за привлечение' (length=38)
      'fr_invoice_registered' =>
        array
          'sum' => string '515.00' (length=6)
      0 =>
        array
          'full_sum' => string '48.00' (length=5)
      'fr_user_settings' =>
        array
          'partner_id' => string '2118' (length=4)
  2 =>
    array
      'fr_user' =>
        array
          'id' => string '182' (length=3)
          'last_name' => string 'Golov' (length=5)
          'first_name' => string 'Roman' (length=5)
          'sex' => boolean true
      'fr_invoice_history' =>
        array
          'add_date' => string '2012-08-29 14:42:40' (length=19)
      'fr_user_statement_typ' =>
        array
          'name' => string 'Бонус за привлечение' (length=38)
      'fr_invoice_registered' =>
        array
          'sum' => string '515.00' (length=6)
      0 =>
        array
          'full_sum' => string '96.00' (length=5)
      'fr_user_settings' =>
        array
          'partner_id' => string '2098' (length=4)
  3 =>
    array
      'fr_user' =>
        array
          'id' => string '172' (length=3)
          'last_name' => string 'Sorokin' (length=7)
          'first_name' => string 'Aleksandr' (length=9)
          'sex' => boolean true
      'fr_invoice_history' =>
        array
          'add_date' => string '2012-08-29 14:42:40' (length=19)
      'fr_user_statement_typ' =>
        array
          'name' => string 'Бонус за привлечение' (length=38)
      'fr_invoice_registered' =>
        array
          'sum' => string '515.00' (length=6)
      0 =>
        array
          'full_sum' => string '248.00' (length=6)
      'fr_user_settings' =>
        array
          'partner_id' => string '2088' (length=4)
  4 =>
    array
      'fr_user' =>
        array
          'id' => string '131' (length=3)
          'last_name' => string 'FR' (length=2)
          'first_name' => string 'Accaunt' (length=7)
          'sex' => boolean true
      'fr_invoice_history' =>
        array
          'add_date' => string '2012-08-29 14:42:40' (length=19)
      'fr_user_statement_typ' =>
        array
          'name' => string 'Бонус за привлечение' (length=38)
      'fr_invoice_registered' =>
        array
          'sum' => string '515.00' (length=6)
      0 =>
        array
          'full_sum' => string '2042.00' (length=7)
      'fr_user_settings' =>
        array
          'partner_id' => string '2048' (length=4)



			*/
		}

		public function sendmailsfordeletedinvoices()
		{
			//send mails, that the inpayment_invoice was deleted
			$this->autoRender = false;
			$this->layout = null;

			$aUserIDs =  $this->Invoice->getDeletedInvoicesFromToday();
			$this->testMailSend = true;
			foreach($aUserIDs as $iUserID)
			{
				$this->prepareMailByUserID(13, $iUserID["fr_user"]["id"]);
			}
		}

		/**
		 * ajax based function
		 */
	public function sendactivationmail()
	{
		$aJsonOutput = array("messagegTyp" => "error", "message" => "Error Message.");
		$this->autoRender = false;
		$this->layout = "ajax";
		if($this->request->is("post"))
		{
			foreach($this->request->data["aIDs"] as $iUserID)
			{
				$this->prepareMailByUserID(3, $iUserID);
			}
			$aJsonOutput = array("messagegTyp" => "ok", "message" => "success");
		}
		echo json_encode($aJsonOutput);
	}

	public function prepareMailByInvoiceID($iEmailID, $iInvoiceID)
    {
		$aUser = $this->User->getUserFullInformationByInvoiceID($iInvoiceID);
		$aInvoice = $this->Invoice->getInvoiceByID($iInvoiceID);

		$this->prepareSystemMail($iEmailID, $aUser, $aInvoice[0]);
    }

    public function prepareMailByUserID($iEmailID, $iUserID, $extraInfo = array())
    {
		$aUser = $this->User->getUserFullInformationByUserID($iUserID);
        $this->prepareSystemMail($iEmailID, $aUser, $extraInfo);
    }

	private function prepareSystemMail($iEmailID, $aUser, $aAdditionalContent = array())
    {
		$aExtraUserInfos = $this->UserInfo->getExtraUserInfoType($aUser["fr_user"]["id"]);
		$this->UserInfoTyp = $this->Components->load("UserInfoTyp");
		$this->UserInfoTyp->addUserInfoTypeInformation($aExtraUserInfos);
		$aUserInfoTypes = $this->UserInfoTyp->getUserInfoTyp();

		$aUserParent = $this->User->getUserFullInformationByUserID($aUser["fr_user"]["userParent_id"]);
		if($aUserParent == null && !is_array($aUserParent)){
		  $aUserParent = array();
		}

		$iEmailID = $iEmailID;
		$this->loadModel("Email");
		$aEmailInformation = $this->Email->getMailInformation($iEmailID);

		$sTitle = ($aUser['fr_user']['sex']) ? 'Господин' : 'Госпожа';
		$sActivation = md5($aUser['fr_user']['id']).md5($aUser['fr_user']['user_email']);

		if($this->testMailSend == true){
			$sTo = "ta@cyberthone.org";
		}
		else{
			$sTo = $aUser['fr_user']['user_email'];
		}

		$sBonusRound = ($aUser['fr_user_settings']['auto_bonus']) ? 'Активирован' : 'Деактивирован';
		$sScheme = ($aUser['fr_user_settings']['programm_typ']) ? '$ 1450 СТАРТ' : '$ 450 ПРЕДСТАРТ';

		$sSubject = $aEmailInformation["fr_mail"]["subject"];
		$sMessage = $aEmailInformation["fr_mail"]["body"];

        $aReplaceConstants = array(
                      "%TITLE%",
                      "%LNAME%",
                      "%FNAME%",
                      "%PASSCLR%",
                      "%MAIL%",
                      "%CODE%",
                      "%PARTNERNUM%",
                      "%BONUSRUND%",
                      "%PROGRAMMTYPE%",
                      "%PHONE_1%",
                      "%SKYPE%",
                      "%INVOICE%",
                      "%SUM%",
                      "%PAYMENT%",
                      "%DATEOFPAYMENT%",
                      "%RECOVERYEKAY%",
                      "%COUNTRY%",
                      "%PARENT_INFO%",
					  "%RADEPOSIT%",
					  "%PAYDATA%",
                      "%%",
                      "%TRADINGACCOUNT%",
                      "%MTQPASSWORD%",
                      "%REFURL%"
        );

		$aReplaceVars =  array(
                      $sTitle,
                      $aUser['fr_user']['last_name'],
                      $aUser['fr_user']['first_name'],
                      isset($aUser['fr_user']['user_pass_clr']) ? $aUser['fr_user']['user_pass_clr'] : "",
                      $aUser['fr_user']['user_email'],
                      $sActivation,
                      $aUser['fr_user_settings']['partner_id'],
                      $sBonusRound,
                      $sScheme,
                      (array_key_exists('added_user_value', isset($aUserInfoTypes[1]) ? $aUserInfoTypes[1] : array()) ? $aUserInfoTypes[1]['added_user_value'] : ""),//Phone_1
                      (array_key_exists('added_user_value', isset($aUserInfoTypes[5]) ? $aUserInfoTypes[5] : array()) ? $aUserInfoTypes[5]['added_user_value'] : ""), // Skype_name
                      $aUser['fr_invoice_registered']['id'],
                      (array_key_exists('fr_invoice_registered', $aAdditionalContent) && array_key_exists('sum', $aAdditionalContent["fr_invoice_registered"]) ? $aAdditionalContent["fr_invoice_registered"]['sum'].' '. $aAdditionalContent["fr_payment"]['currency_code'] : ""),//sum
                      (array_key_exists('fr_payment', $aAdditionalContent) && array_key_exists('name_payment_systems', $aAdditionalContent["fr_payment"]) ? $aAdditionalContent["fr_payment"]['name_payment_systems'] : ""),//name_payment_systems
                      (array_key_exists('fr_invoice_history', $aAdditionalContent) && array_key_exists('add_date', $aAdditionalContent["fr_invoice_history"]) ? $aAdditionalContent["fr_invoice_history"]['add_date'] : ""),//add date,
                      (array_key_exists('recovery_key', $aAdditionalContent) ? $aAdditionalContent['recovery_key'] : ""), //RecoveryKey
                      $aUser['cc']['cn'],
                      (array_key_exists('fr_user', $aUserParent) ? $aUserParent['fr_user']['last_name'].' '.$aUserParent['fr_user']['first_name'].' #'.$aUserParent['fr_user_settings']['partner_id'] : ""),
											"",
											"",
					  (array_key_exists('fr_invoice_history', $aAdditionalContent) && array_key_exists('add_date', $aAdditionalContent["fr_invoice_history"]) ? $aAdditionalContent["fr_invoice_history"]['add_date'] : ""),
                      (isset($aAdditionalContent['trading_account']) ? $aAdditionalContent['trading_account'] : ""),
                      (isset($aAdditionalContent['MTQpassword']) ? $aAdditionalContent['MTQpassword'] : ""),
                      (isset($aUser['fr_user']['hash_url']) ? 'http://investoriaplus.com/registratsiya/?hash=' . $aUser['fr_user']['hash_url'] : "")
					  //PAYDATA
					  );

		$sSubject = str_replace($aReplaceConstants, $aReplaceVars, $sSubject);
		$sMessage = str_replace	($aReplaceConstants, $aReplaceVars, $sMessage);

		$this->sendMail($sTo, $sSubject, $sMessage);
	}


	function sendMail($sTo, $sSubject, $sMessage)
	{
		App::uses('CakeEmail', 'Network/Email');

		$oEmail = new CakeEmail('smtp');
		$oEmail->to($sTo);
		$oEmail->subject($sSubject);
		$oEmail->emailFormat('both');
		$oEmail->sender("no-reply@fxmbackoffice.com", "Investoria Plus");
		$oEmail->send($sMessage);
	}
}
