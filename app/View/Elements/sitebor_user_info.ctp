<div class="fxr-siderbar-header">
	<span class="fx-icon fx-media off"></span>
    <p>Ваши данные</p>
</div>
<div class="fxr-sidebar-block">
    Добро пожаловать<br />
	Имя: <b><?php echo $this->Session->read('UserInfo.last_name').' '.$this->Session->read('UserInfo.first_name'); ?></b><br><br>
    Ваш ID: <b><?php echo $this->Session->read('UserInfo.partner_id') ?></b><br>
    Партнерский счет: <b><?php echo $fulluserinfo['agent_account']; ?></b><br>
    Торговый счет: <b><?php echo $fulluserinfo['trading_account']; ?></b><br>
</div>
<div class="fxr-siderbar-header">
	<span class="fx-icon fx-website person"></span>
    <p>Ваш Спонсор</p>
</div>
<div class="fxr-sidebar-block">
	<?php if( $parentinfo == null ){ ?>
        <p>у Вас нету спонсора.</p>
        <p>выбрать спонсора</p>
    <?php } else { ?>
        Имя: <b><?php echo $parentinfo['last_name'].' '.$parentinfo['first_name']; ?></b><br>
        E-mail: <b><?php echo $parentinfo['user_email']; ?></b>
    <?php } ?>
</div>
<div class="fxr-siderbar-header">
	<span class="fx-icon fx-website chat"></span>
    <p>Техподдержка</p>
</div>
<div style="background-color:#FFF" class="fxr-sidebar-block">
	<!--
    Skype 'Мой статус' button
    http://www.skype.com/go/skypebuttons
    -->
    <script src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js" type="text/javascript"></script>
    <a href="skype:torin.asakura?chat"><img width="182" height="44" alt="Техническая поддержка сайта" style="border: none;" src="http://mystatus.skype.com/bigclassic/torin%2Easakura"></a>
</div>