<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

//import the mail controller handling
App::import("Controller", "Email");
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AccountController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Account';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Account', 'User');

	public function viewtype($type = null)
	{
		if($type == null)
			$aInfo = $this->Account->GetAllStatementType($this->params['type']);
		else
			$aInfo = $this->Account->GetAllStatementType($type);
		$this->set("aAccountInformations", $aInfo);
		$this->set("viewtype", $this->params['type']);
		$this->render("statement");
	}
	
	public function get_structur()
	{
		$this->autoRender = false;
		if($this->request->is("ajax") && $this->request->is("post"))
		{
			$iInvoiceID = $this->request->data["id"];
			$aUser = $this->User->getUserByInvoiceID($iInvoiceID);
			$aUserParents = $this->Account->GetParentInvoiceForPayStructur($aUser["fr_user"]["id"]);
			
			if($aUserParents != null && is_array($aUserParents))
			{
				
				for($i = 0; $i < count($aUserParents); $i++)
				{
					$aUserParents[$i] = $this->UserStatusInfo->calculateUserStatus($aUserParents[$i]);
				}
				$this->set("aUserParents", array_reverse($aUserParents));
				$this->set("iInvoiceID", $iInvoiceID);
				$this->layout = "ajax";
				$this->render("modalstatement");
			}
		}
		else
			CakeLog::write("warning", "action 'get_structur' was called without ajax request");
	}
	
	public function payconfirm()
	{
		
		/*
		 array
			'confirm' => 
				array
					403 => 
						array
							'bonus' => string '16' (length=2)
							'user_id' => string '403' (length=3)
					172 => 
						array
							'bonus' => string '40' (length=2)
							'user_id' => string '172' (length=3)
					131 => 
						array
							'bonus' => string '24' (length=2)
							'user_id' => string '131' (length=3)
		 
		*/
		if($this->request->is("post") && array_key_exists("confirm", $this->request->data))
		{
			$this->autoRender = false;
			foreach($this->request->data["confirm"] as $aData)
			{
				//insert only the entries without 0
				if(((int)$aData["bonus"]) > 0)
				{
					$this->Account->insertStatements($this->request->data["invoice_id"], $aData["bonus"], $aData["user_id"]);
				}
			}
			$aTransactionUserStatement = $this->Account->getInfoTransactionUserStatement($this->request->data["invoice_id"]);
			
			$oEmailController = new EmailController();
			foreach($aTransactionUserStatement as $aTransaction)
			{
				$oEmailController->sendtransactioninformationmail($aTransaction);
			}
			
			$this->redirect("/account/onetimebonuslisting/1");
		}
	}
	
	public function debug()
	{
		$this->autoRender = false;
		var_dump($this->Account->getInfoTransactionUserStatement(1298));
	}
}
