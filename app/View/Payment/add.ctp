<?php
//App::import('Vendor','xtcpdf');
//$tcpdf = new XTCPDF();
//$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'

//$tcpdf->SetAuthor("London East Academy");
//$tcpdf->SetAutoPageBreak( false );
//$tcpdf->setPrintHeader(true);
//$tcpdf->setPrintFooter(true);
//$tcpdf->setHeaderFont(array($textfont,'',40));
//$tcpdf->xheadercolor = array(0,0,0);
//$tcpdf->xheadertext = '';
//$tcpdf->xfootertext = '';

// add a page (required with recent versions of tcpdf)
//$tcpdf->AddPage();

// create some HTML content
//$html = file_get_contents('pdf.ctp', FILE_USE_INCLUDE_PATH);

// Now you position and print your page content
// example: 
//$tcpdf->SetTextColor(0, 0, 0);
//$tcpdf->SetFont($textfont,'B',10);
// output the HTML content
//$tcpdf->writeHTML($html, true, false, true, false, '');
// ...
// etc.
// see the TCPDF examples 

//echo $tcpdf->Output(USERSPATH . PARTNER_ID .DS .  $invoiceId.'.pdf', 'F');

?> 
<div id="withdrawal_container" style="position:relative; margin-top: 0px; width: 100%;">
	<div class="fxr-siderbar-header"><p>Оплата торгового места</p></div>
    <div id="withdrawal_body">
        <div class="success">
            Ваша заявка на оплату торгового места Nr. <b><?php echo  $invoiceId ?></b> зарегистрирована.
            <ul>
                <li>Сразу же после поступления средств на счет компании Вы получите уведомление на Ваш электронный адрес.</li>
                <li>Во избежании задержки приема платежей убедительно просим Вас при переводе средств указывать ваш номер платежа: <b><?php echo  $invoiceId ?></b>.</li>
                <li>По всем вопросам относительно Ваших платежей обращайтесь к нам через службу поддержки, при обращении к нам, пожалуйста, указывайте номер вашего платежа (Invoice No: <b><?php echo  $invoiceId ?></b>)</li>
                <li>Чтобы распечатать платежные реквизиты (Invoice), зайдите в пункт меню <a href="/?page_id=27">"История заявок"</a> и в колонке "посмотреть" откройте документ и распечатайте его.</li>
            </ul>
            <p>С уважением,<br />Финансовый отдел проекта Investoria Plus</p>
        </div>
    </div>
</div>
