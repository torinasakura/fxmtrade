<?php

App::uses('AppModel', 'Model');

class Form extends AppModel
{
	public $name = 'Form';
	public $primaryKey = 'id';
	public $useTable = 'user';
	public $uses = array("UserInfo");

	 var $validate = array(
		'login' => array(
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'required' => true,
				'message' => 'Alphabets and numbers only'
			),
			'between' => array(
				'rule' => array('between', 5, 15),
				'message' => 'Between 5 to 15 characters'
			)
		),
		'password' => array(
			'rule' => array('minLength', '8'),
			'message' => 'Mimimum 8 characters long'
		),
		'email' => 'email',
			'born' => array(
			'rule' => 'date',
			'message' => 'Enter a valid date',
			'allowEmpty' => true
		)
	);

	public function checkDataForm($data)
	{
		$valid['err'] = true;
		$valid['user_id'] = USER_ID;

		$field = $this->getTableField('fr_user');

 		foreach ($data as $key => $value)
		{
			if(!in_array ($key,array('user_email')))
				if(!is_array($value))
					$value = $this->data_escape($value, $key);
			/*добавим исключение для личного кабинета настроек пользователя*/
            if(empty($value) && !in_array($key, array('phone1', 'phone2', 'phone3', 'fax', 'skype', 'tel', 'nickname', 'pay_details', 'pay_details1', 'pay_details2', 'pay_details3', 'pay_details4', 'pay_details5', 'pay_details5_1', 'pay_details5_2', 'pay_details5_3', 'pay_details5_4', 'pay_details6', 'pay_details7', 'pay_details8')))
			{
				$valid['err'] = 'Неверно заполнено поле '.$key.'<br />';
				return $valid;
			}
			if(in_array ($key,$field['fr_user']))
				$valid['fr_user'][$key] = "'".$value."'";
		}
		$valid['err'] = false;
		return $valid;
	}

	function check_data_form_newuser($data)
	{
		$result = NULL;
		$err = NULL;
        foreach ($data as $key => $value)
		{
			$data_escape = $this->data_escape($value, $key);
			if ($data_escape == false){
				$err[] = $key;
			$this->log("k: ".$key."  v: ".$value);
			}else{
				$result['fr_user'][$key] = $data_escape;
			}
		}
		if(is_array($err)) {
            $result['fr_user']['err'] = $err;
        }
        return $result;
	}

	function data_escape($value, $key = false)
	{
 		$value = trim($value);
		if($key == 'account_option' || $key == 'base_currency' || $key == 'leverage' || $key == 'source_capital' || $key == 'experience' || $key == 'exchange_experience'){
			/*if(is_int($value)){
				$this->log($value."   is int");*/
				return $value;
			/*}else{
				return false;
			}*/
		}else if ($key == 'user_pass')
		{
			$value = stripslashes($value); //Удаляет экранирование символов
			$value = htmlspecialchars($value); //Преобразует специальные символы в HTML сущности
			$value = strrev(md5($value))."b3p6f";
			return $value;
		}
		else if ($key == 'zip_code')
			$value = strtoupper($value); //Преобразует строку в верхний регистр
		else if ($key == 'user_email')
		{
			return (!empty($value)  && preg_match('/^[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9_\-\.]+$/', $value)) ? $value : false;
		}
		else if ($key == 'sex')
			return true;
		else if ($key == 'country')
		{
			if(strlen($value) == 2)
				return $value;
			else
				return false;
		}
		$value = strip_tags($value); //Удаляет HTML и PHP тэги из строки
		$value = ucwords(strtolower($value));
		$value = str_replace(array("%", "_"), array("", " "), $value);

		$value = preg_replace('/[^a-zA-Z0-9-_ .]/', '', $value); // strips slashes
		if(strlen($value) < 3)
			$value = false;

		return $value;
	}

	function getTableField($sTableName)
	{
		$field[$sTableName] = false;

		$query = 'SELECT COLUMN_NAME as Field
				FROM information_schema.COLUMNS
				WHERE TABLE_SCHEMA = DATABASE()
				  AND TABLE_NAME = "' .$sTableName. '"
				ORDER BY ORDINAL_POSITION';
		$sql = $this->query($query);
		foreach ($sql as $myrow)
		{
			$field[$sTableName][] = $myrow['COLUMNS']['Field'];
		}
		return $field;
	}

	public function getUserInfo($iUserID, $sField)
    {
		$query = "Select " .$sField. " From fr_user Where fr_user.id =".$iUserID;
		$sql = $this->query($query);
		return $sql[0]['fr_user'][$sField];
	}

	function setPassword($sFormPassword)
	{
		$sValidPassword = null;
		if(strlen($sFormPassword) > 5)
			$sValidPassword = $this->data_escape($sFormPassword,'user_pass');
		return $sValidPassword;
	}

	function checkPasswords($sFormPassword)
	{
		$user_pass = $this->getUserInfo(USER_ID, 'user_pass');
		if($user_pass == $this->data_escape($sFormPassword,'user_pass'))
			return true;
		else
			return false;
	}


  	public function checkUnique($data, $fieldName) {
        $valid = false;

        if (isset($fieldName) && $this->hasField($fieldName)) {
            $valid = $this->isUnique(array(
                $fieldName => $this->data['User'][$fieldName]
            ));
        }

        return $valid;
    }

	function setAvatar($file)
	{
		$message = null;
		if ($file['error']=== UPLOAD_ERR_OK)
		{
			if(!preg_match('/(jpg|jpeg|png)/i', $file['type']))
			{
				$message = 'Аватар должен быть в формате <strong>JPG или PNG</strong>';
				return $message;
			}
			if (!preg_match('/\.([A-Za-z]*)$/',$file['name'],$ext))
			{
				$ext[0] = '.bin';
			}
			$f = file_get_contents($file['tmp_name']);
			if (!is_dir(USERSPATH . PARTNER_ID))
			{
				mkdir(USERSPATH . PARTNER_ID , 0777);
			}
			copy($file['tmp_name'], USERSPATH . PARTNER_ID . DS . 'tmp_' . PARTNER_ID.$ext[0]);
			$tempfile = USERSPATH . PARTNER_ID . DS . 'tmp_' . PARTNER_ID.$ext[0];
			if(preg_match('/[.](PNG)|(png)$/', $file['name']))
			{
				$im = imagecreatefrompng($tempfile) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
			}
			if(preg_match('/[.](JPG)|(jpg)|(jpeg)|(JPEG)$/',$file['name']))
			{
				$im = imagecreatefromjpeg($tempfile); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
			}
			//СОЗДАНИЕ КВАДРАТНОГО ИЗОБРАЖЕНИЯ И ЕГО ПОСЛЕДУЮЩЕЕ СЖАТИЕ
			// Создание квадрата 90x90
			// dest - результирующее изображение
			// w - ширина изображения
			// ratio - коэффициент пропорциональности
			$w = 128; // квадратная 128x128. Можно поставить и другой размер.
			// создаём исходное изображение на основе
			// исходного файла и определяем его размеры
			$w_src = imagesx($im); //вычисляем ширину
			$h_src = imagesy($im); //вычисляем высоту изображения
			// создаём    пустую квадратную картинку
			// важно именно    truecolor!, иначе будем иметь 8-битный результат
			$dest = imagecreatetruecolor($w,$w);
			// вырезаем квадратную серединку по x, если фото горизонтальное
			if ($w_src > $h_src)
				imagecopyresampled($dest, $im, 0, 0,
			round((max($w_src,$h_src)-min($w_src,$h_src))/2),0,$w,$w,min($w_src,$h_src), min($w_src,$h_src));
			// вырезаем квадратную верхушку по y,
			// если фото вертикальное (хотя можно тоже серединку)
			if ($w_src < $h_src)
				imagecopyresampled($dest, $im, 0, 0,    0, 0, $w, $w,
			min($w_src,$h_src),    min($w_src,$h_src));
			// квадратная картинка    масштабируется без вырезок
			if ($w_src==$h_src)
				imagecopyresampled($dest, $im, 0, 0, 0, 0, $w, $w, $w_src, $w_src);
			$date=time(); //вычисляем время в настоящий момент.
			imagejpeg($dest, USERSPATH . PARTNER_ID . DS . 'avatar_' . PARTNER_ID.'.jpg');
			file_put_contents(USERSPATH . PARTNER_ID .DS .  '_' . PARTNER_ID . $ext[0],$f);
			unlink($tempfile);//удаляем оригинал загруженного изображения, он нам больше не нужен. Задачей было - получить миниатюру.
		}
	}
}
?>