<?php
App::uses('CakeEmail', 'Network/Email');

class EmailComponent extends Component
{
	public $name = 'Email';
	private $testMailSend = false;
	
		
	public function prepareSystemMail($emailInformation, $userData = array(), $additionalContent = array())
    {
		if($this->testMailSend == true){
			$to = "admin@fxmtrade.com";
		}
		else{
			$to = $userData['user_email'];
		}
		
		$subject = $emailInformation["fr_mail"]["subject"];
		$message = $emailInformation["fr_mail"]["body"];
      
		$replaceConstants = array(	
                      "%TITLE%", 
                      "%LNAME%", 
                      "%FNAME%", 
                      "%MAIL%", 
                      "%CODE%", 
                      "%PARTNERNUM%", 
                      "%BONUSRUND%",
                      "%PROGRAMMTYPE%",
                      "%PHONE_1%",
                      "%SKYPE%",
                      "%INVOICE%",
                      "%SUM%",
                      "%PAYMENT%",
                      "%DATEOFPAYMENT%",
                      "%RECOVERYEKAY%",
                      "%COUNTRY%",
                      "%PARENT_INFO%",
					  "%RADEPOSIT%",
					  "%PAYDATA%");
			
		$replaceVars =  array(
                      "%TITLE%", 
                      "%LNAME%", 
                      "%FNAME%", 
                      "%MAIL%", 
                      "%CODE%", 
                      "%PARTNERNUM%", 
                      "%BONUSRUND%",
                      "%PROGRAMMTYPE%",
                      "%PHONE_1%",
                      "%SKYPE%",
                      "%INVOICE%",
                      "%SUM%",
                      "%PAYMENT%",
                      "%DATEOFPAYMENT%",
                      "%RECOVERYEKAY%",
                      "%COUNTRY%",
                      "%PARENT_INFO%",
					  "%RADEPOSIT%",
					  "%PAYDATA%"
					  );
			
		$subject = str_replace	($replaceConstants, $replaceVars, $subject);
		$message = str_replace	($replaceConstants, $replaceVars, $message);
			
		$this->sendMail($to, $subject, $message, 'fxm');
	}
	
	private function sendMail($to, $subject, $message, $from)
	{
		$Email = new CakeEmail($from);
		$Email->to($to);
		$Email->subject($subject);
		$Email->emailFormat('both');
		$Email->sender("no-reply@fxmbackoffice.com", "Investoria Plus");
		$Email->send($message);
	}
}
?>