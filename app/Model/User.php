<?php

App::uses('AppModel', 'Model');

class User extends AppModel
{
	public $name = 'User';
	public $primaryKey = 'id';
	public $useTable = 'user';


	public function addDemoUser($data){

		$query = 'REPLACE INTO main_backoffice.fxm_users_demo (user_email, user_registered, last_name, first_name, sex) VALUES  ("'.$data['user_email'].'", NOW(), "'.$data['last_name'].'", "'.$data['first_name'].'", 1)';
		$results = $this->query($query);
		return $results;
	}
    public function addRealUser($data){

        $query = 'REPLACE INTO main_backoffice.fr_user (id, user_pass, user_email, user_registered, activation, last_name, first_name, sex, address, zip_code, city, country, birthday, userParent_id, lastvisitDate, newsletter, group_id, access, hash_url) VALUES  (NULL, "'.$data['user_pass'].'", "'.$data['user_email'].'", NOW(), 1, "'.$data['last_name'].'", "'.$data['first_name'].'", "'.$data['sex'].'", "'.$data['address'].'", "'.$data['zip_code'].'", "'.$data['city'].'", "'.$data['country'].'", "'.$data['birthday'].'", "' . $data['userParent_id'] . '", "0000-00-00 00:00:00", 1, 0 ,0, "' . $data['hash_url'] . '")';
        $results = $this->query($query);

        $q2 = 'SELECT id FROM main_backoffice.fr_user ORDER BY id DESC LIMIT 1';
        $results2 = $this->query($q2);

        return $results2[0]['fr_user']['id'];
    }
	public function addRealUserAdv($data, $user_id){
		$query = 'REPLACE INTO main_backoffice.fr_user_adv (id, user_id, account_option, base_currency, initial_deposit, leverage, source_capital, experience, exchange_experience) VALUES  (NULL, "'.$user_id.'", "'.$data['account_option'].'", "'.$data['base_currency'].'","'.$data['initial_deposit'].'", "'.$data['leverage'].'", "'.$data['source_capital'].'", "'.$data['experience'].'", "'.$data['exchange_experience'].'")';
		$results = $this->query($query);


		$qs2 = 'SELECT partner_id FROM main_backoffice.fr_user_settings ORDER BY partner_id DESC LIMIT 1';
		$pa = $this->query($qs2);
		$partid = $pa[0]['fr_user_settings']['partner_id']+1;
		$this->log("pid: ".$partid);

		$q1 = 'REPLACE INTO main_backoffice.fr_user_settings (user_id, auto_bonus, partner_id, trading_account, agent_account, programm_typ, status_id) VALUES ("'.$user_id.'", 1, "'.$partid.'", 0, 0, 0, 0)';
		$results = $this->query($q1);

		return $partid;
	}
	public function updateUserInfo($aData)
	{
		$this->updateAll($aData['fr_user'], array('id' => USER_ID));
	}
	public function getUserById ($iUserID)
	{
		$aReturn = null;
		$aUser = $this->User->find("first", array("conditions" => array("id" => $iUserID), "fields" => array("first_name", "last_name", "country", "zip_code", "city", "address")));

		if($aUser != null && is_array($aUser))
		$aReturn = $aUser;
		return $aReturn;
	}
	public function getAllUsersCount($time = false)
	{
		$iCount = 0;
		if($time == 'today')
			$time = ' AND year(user_registered) = year(current_timestamp) AND month(user_registered) = month(current_timestamp) AND day(user_registered) = day(current_timestamp)';
		elseif($time == 'week')
			$time = ' AND year(user_registered) = year(current_timestamp) AND week(user_registered) = week(current_timestamp)';
		elseif($time == 'month')
			$time = ' AND year(user_registered) = year(current_timestamp) AND month(user_registered) = month(current_timestamp)';
		elseif($time == 'quarter')
			$time = ' AND year(user_registered) = year(current_timestamp) AND QUARTER(user_registered) = QUARTER(current_timestamp)';

		$query = "SELECT COUNT( fr_user.id ) AS countusers
              FROM fr_user
              WHERE fr_user.activation = 1 AND fr_user.access = 0 $time";
		$aResults = $this->query($query);

		if($aResults != null && is_array($aResults) && $aResults[0][0]["countusers"] != null)
			$iCount = $aResults[0][0]["countusers"];

		return $iCount;
	}

	public function getAllActivatedUsers()
	{
		$aReturn = null;

		$query = "SELECT fr_user.id, fr_user.last_name, fr_user.first_name, fr_user.lastvisitDate, fr_user.user_email, fr_user_settings.partner_id, fr_user.userParent_id,
			fr_user_settings.programm_typ, fr_user.sex, fr_user_info.value AS skypename, COALESCE(fr_invoice_registered.status_id, 0) as invoice_status_id, fr_user_settings.status_id as user_status_id,
			(SELECT fr_user_parent.last_name FROM fr_user AS fr_user_parent WHERE fr_user_parent.id = fr_user.userParent_id) AS parent_last_name, fr_user.user_registered, fr_user.activation,
			(SELECT ROUND((n.rgt - n.lft -1) /2) FROM fr_user_tree AS n, fr_user_tree AS p WHERE n.lft BETWEEN p.lft AND p.rgt AND n.user_id = fr_user.id GROUP BY n.user_id) as countpartner
			  FROM fr_user
			  INNER JOIN fr_user_settings On fr_user.id = fr_user_settings.user_id
			  LEFT JOIN fr_user_info On fr_user.id = fr_user_info.user_id AND fr_user_info.type = 5
			  LEFT JOIN fr_invoice_registered ON fr_user.id = fr_invoice_registered.user_id
			  WHERE fr_user.activation = 1 AND access = 0
			  GROUP BY fr_user.id
			  ORDER BY fr_invoice_registered.id ASC";
		$aResults = $this->query($query);

		if($aResults != null && is_array($aResults))
		$aReturn = $aResults;

		return $aReturn;
	}

  public function getAllNotActivatedUsers()
  {
    $aReturn = null;

    $query = "SELECT fr_user.id, fr_user.last_name, fr_user.first_name, fr_user.lastvisitDate, fr_user.user_email, fr_user_settings.partner_id, fr_user.userParent_id,
            fr_user_settings.programm_typ, fr_user.sex, fr_user_info.value AS skypename, COALESCE(fr_invoice_registered.status_id, 0) as invoice_status_id, fr_user_settings.status_id as user_status_id,
            (SELECT fr_user_parent.last_name FROM fr_user AS fr_user_parent WHERE fr_user_parent.id = fr_user.userParent_id) AS parent_last_name, fr_user.user_registered, fr_user.activation,
            (SELECT ROUND((n.rgt - n.lft -1) /2) FROM fr_user_tree AS n, fr_user_tree AS p WHERE n.lft BETWEEN p.lft AND p.rgt AND n.user_id = fr_user.id GROUP BY n.user_id) as countpartner
              FROM fr_user
              INNER JOIN fr_user_settings On fr_user.id = fr_user_settings.user_id
              LEFT JOIN fr_user_info On fr_user.id = fr_user_info.user_id AND fr_user_info.type = 5
              LEFT JOIN fr_invoice_registered ON fr_user.id = fr_invoice_registered.user_id
              WHERE fr_user.activation = 0 AND access = 0
              GROUP BY fr_user.id
              ORDER BY fr_user.id ASC";
    $aResults = $this->query($query);

    if($aResults != null && is_array($aResults))
      $aReturn = $aResults;

    return $aReturn;
  }

  /*
   DELETE FROM fr_invoice_registered WHERE fr_invoice_registered.id IN (SELECT fr_invoice_registered.id
    FROM fr_invoice_registered
    LEFT JOIN fr_user ON ( fr_invoice_registered.user_id = fr_user.id )
    WHERE fr_invoice_registered.id IS NOT NULL
    AND fr_user.id IS NULL
    )
  */

  public function getAllUnpaidUsersCount()
  {
    $iCount = 0;

    $aUnpaidUsers = $this->getAllUnpaidUsers();
    if($aUnpaidUsers != null)
      $iCount = count($aUnpaidUsers);

    return $iCount;
  }

  public function getAllUnregisteredUnpaidUsers()
  {
    $aReturn = null;

    $query = "SELECT fr_user.user_email, fr_user.last_name, fr_user.first_name, f1.status_id
              FROM fr_invoice_registered f1
              RIGHT JOIN fr_user ON fr_user.id = f1.user_id
              LEFT JOIN fr_invoice_registered f2 ON f1.user_id = f2.user_id AND (f2.status_id = 2 OR f2.status_id = 1)
              WHERE (fr_user.activation = 1 AND fr_user.access = 0) AND f2.status_id IS NULL
              GROUP BY fr_user.id";
    $aResult = $this->query($query);

    if($aResults != null && is_array($aResults))
      $aReturn = $aResults;

    return $aResults;
  }

  public function getAllUnpaidUsers()
  {
    $aReturn = null;

    $query = "SELECT fr_user.user_email, fr_user.last_name, fr_user.first_name, f1.status_id
              FROM fr_invoice_registered f1
              RIGHT JOIN fr_user ON fr_user.id = f1.user_id
              LEFT JOIN fr_invoice_registered f2 ON f1.user_id = f2.user_id AND f2.status_id = 2
              WHERE (fr_user.activation = 1 AND fr_user.access = 0) AND f2.status_id IS NULL
              GROUP BY fr_user.id ";
    $aResults = $this->query($query);

    if($aResults != null && is_array($aResults))
      $aReturn = $aResults;
    else
      CakeLog::write('info', 'No unpaid users');

    return $aReturn;

    /*SELECT fr_user.user_email, fr_user.last_name, fr_user.first_name, fr_invoice_registered.status_id
              FROM fr_user
              LEFT JOIN fr_invoice_registered ON fr_user.id = fr_invoice_registered.user_id
              WHERE (fr_user.activation = 1 AND fr_user.access = 0)
              AND (fr_invoice_registered.user_id IS NULL OR fr_invoice_registered.status_id NOT IN ( SELECT status_id FROM fr_invoice_registered WHERE fr_invoice_registered.status_id = 2))
              GROUP BY fr_user.id

    /*all aktiv mit invoice status != 2:
     SELECT *
              FROM fr_invoice_registered f1
              LEFT JOIN fr_invoice_registered f2 ON f1.user_id = f2.user_id AND f2.status_id = 2
              INNER JOIN fr_user ON fr_user.id = f1.user_id
              WHERE (fr_user.activation = 1 AND fr_user.access = 0) AND f2.status_id IS NULL AND f1.typ = 1
              GROUP BY f1.user_id


      alle aktiv ohne invoice
      SELECT fr_user.user_email, fr_user.last_name, fr_user.first_name, fr_invoice_registered.status_id
              FROM fr_user
              LEFT JOIN fr_invoice_registered ON fr_user.id = fr_invoice_registered.user_id
              WHERE (fr_user.activation = 1 AND fr_user.access = 0) AND (fr_invoice_registered.user_id IS NULL) GROUP BY fr_user.id


    $query = "SELECT fr_user.id as fr_user_id, fr_user.user_email, fr_user.last_name, fr_user.first_name
              FROM fr_user
              WHERE fr_user.activation = 1 AND fr_user.access = 0";
    $aUserResults = $this->query($query);
    //var_dump($aUserResults);

    $query = "SELECT fr_user.id as fr_user_id
              FROM fr_user
              INNER JOIN fr_invoice_registered ON fr_user.id = fr_invoice_registered.user_id
              WHERE (fr_user.activation = 1 AND fr_user.access = 0)
              GROUP BY fr_user.id ";
    $aInvoiceResults = $this->query($query);
    //var_dump($aInvoiceResults);


    $aInvoiceIDPaidUsers = array();
    for($i = 0; $i < count($aInvoiceResults) ; $i++)
      $aInvoiceIDPaidUsers[] = $aInvoiceResults[$i]["fr_user"]["fr_user_id"];

    for($i = 0; $i < count($aUserResults); $i++)
    {
      if(in_array($aUserResults[$i]["fr_user"]["fr_user_id"], $aInvoiceIDPaidUsers))
      {
        unset($aUserResults[$i]);
      }
    }
    */
  }

  public function getAllUsersWithoutParents()
  {
    $query = "SELECT fr_user.id
              FROM fr_user
              LEFT JOIN fr_user fr_user_parent ON (fr_user.userParent_id=fr_user_parent.id)
              WHERE fr_user.id IS NOT NULL AND fr_user_parent.id IS NULL AND fr_user.access = 0";

  }

  /**
   *@return case
   *      NULL : user don't have a parent
   *      NOT NULL: info about the users parent
   *          status_id = null: parent hasn't given a paid
   *          status_id = 2: parent has paid the fee
   */
  public function checkUserParentHasPaid($iUserID)
  {
    $aReturn = null;
	$query = "SELECT fr_user_parent.id, fr_user_parent.user_registered, fr_invoice_registered.status_id
              FROM fr_user
              LEFT JOIN fr_invoice_registered ON fr_user.userParent_id = fr_invoice_registered.user_id AND fr_invoice_registered.status_id = 2
              INNER JOIN fr_user fr_user_parent ON fr_user.userParent_id = fr_user_parent.id
              WHERE fr_user.activation = 1 AND fr_user.access = 0 AND fr_user.id = $iUserID";

    $aResults = $this->query($query);
	if($aResults != null && is_array($aResults))
      $aReturn = $aResults[0];
    else
      CakeLog::write('user_error', 'User with id: '.$iUserID .' hasn\'t got a parent');

    return $aReturn;
  }

  public function getNextParentHasPaid($iUserID, $iParentLimit = 2)
  {
    $bHasPaid = false;
    $iLoopCounter = 0;
    $iCheckID = $iUserID;

    while($bHasPaid == false && $iParentLimit > $iLoopCounter)
    {
      $aParentInformation = $this->checkUserParentHasPaid($iCheckID);
      $iCheckID = $aParentInfo["fr_user_parent"]["id"];

      if($aParentInformation["fr_invoice_registered"]["status_id"] == 2){
        $bHasPaid = true;
      }

      $iLoopCounter++;
    }

    if($iLoopCounter >= $iParentLimit){
      //full fatal error!!!
    }

    return $iCheckID;
  }

  /**
   * Sets the new parent_id of the user. Only in the fr_user table, not i fr_user_tree
   */
  public function setUserNewParent($iUserID, $iParentID)
  {
    $iCountAffectedRows = 0;
    if($iParentID > 0)
    {
      $query = "UPDATE fr_user SET userParent_id = $iParentID WHERE id = $iUserID ";
      $this->query($query);
      $iCountAffectedRows = $this->getAffectedRows();
    }
    return $iCountAffectedRows;
  }

  public function getUserParentAgentAccount($iUserID)
  {
    $aReturn = null;

    $query = "SELECT fr_user_parent.userParent_id as id, fr_user_parent_settings.trading_account, fr_user_parent_settings.agent_account
              FROM fr_user fr_user_parent
              INNER JOIN fr_user_settings fr_user_parent_settings ON fr_user_parent.userParent_id = fr_user_parent_settings.user_id
              WHERE fr_user_parent.id = $iUserID";

    $aResults = $this->query($query);
    if($aResults != null && is_array($aResults) && $aResults[0]["fr_user_parent_settings"]["trading_account"] > 0)
      $aReturn = $aResults[0];
    else
      CakeLog::write('fatal', 'The parent of the user with user_id: '.$iUserID.' has no trading account');

    return $aReturn;
  }

  public function getUserByInvoiceID($iInvoiceID)
  {
    $aReturn = null;

    $query = "SELECT fr_user.*
              FROM fr_invoice_registered
              INNER JOIN fr_user ON fr_user.id = fr_invoice_registered.user_id
              WHERE fr_invoice_registered.id = $iInvoiceID";
    $aResults = $this->query($query);

    if($aResults != null && is_array($aResults))
      $aReturn = $aResults[0];
    else
      CakeLog::write('info', 'No user with invoice id: '.$iInvoiceID);

    return $aReturn;
  }

  public function getUserFullInformationByInvoiceID($iInvoiceID)
  {
    $aReturn = null;

    $query = "SELECT fr_user.*, fr_user_settings.auto_bonus, fr_user_settings.partner_id, fr_user_settings.agent_account, fr_user_settings.programm_typ,
					  fr_user_settings.trading_account, cc.cn, fr_payment.name_payment_systems, fr_payment.id, fr_payment.currency_code,
					  fr_invoice_registered.id, fr_invoice_registered.deposit_type
              FROM fr_user
              INNER JOIN cc ON fr_user.country = cc.cc
              INNER JOIN fr_user_settings ON fr_user.id = fr_user_settings.user_id
              INNER JOIN fr_invoice_registered ON fr_invoice_registered.user_id = fr_user.id
              INNER JOIN fr_payment ON fr_invoice_registered.payments_id = fr_payment.id
              WHERE fr_invoice_registered.id = $iInvoiceID";
    $aResults = $this->query($query);

    if($aResults != null && is_array($aResults))
      $aReturn = $aResults[0];
    else
      CakeLog::write('info', 'No user with invoice id: '.$iInvoiceID);

    return $aReturn;
  }

  public function getUserFullInformationByUserID($iUserID)
  {
    $aReturn = null;

    $query = "SELECT fr_user.*, fr_user_settings.auto_bonus, fr_user_settings.partner_id, fr_user_settings.agent_account, fr_user_settings.programm_typ,
					  fr_user_settings.trading_account, cc.cn, fr_payment.name_payment_systems, fr_payment.id, fr_payment.currency_code, fr_invoice_registered.id
              FROM fr_user
              INNER JOIN cc ON fr_user.country = cc.cc
              INNER JOIN fr_user_settings ON fr_user.id = fr_user_settings.user_id
              LEFT JOIN fr_invoice_registered ON fr_invoice_registered.user_id = fr_user.id
              LEFT JOIN fr_payment ON fr_invoice_registered.payments_id = fr_payment.id
              WHERE fr_user.id = $iUserID";
    $aResults = $this->query($query);

    if($aResults != null && is_array($aResults))
      $aReturn = $aResults[0];
    else
      CakeLog::write('info', 'No user with user id: '.$iUserID);

    return $aReturn;
  }

  public function updateUserTradingSettings($sTradingAccount, $iUserID)
  {
    $iCountAffectedRows = 0;
    if($iUserID > 0)
    {
      $query = "UPDATE fr_user_settings SET trading_account = '$sTradingAccount' WHERE user_id = $iUserID ";
      CakeLog::write('debug', "query: " .$query);
      $this->query($query);
      $iCountAffectedRows = $this->getAffectedRows();
    }
    return $iCountAffectedRows;
  }

  public function updateUserAgentSettings($sAgentAccount, $iUserID)
  {
    $iCountAffectedRows = 0;
    if($iUserID > 0)
    {
      $query = "UPDATE fr_user_settings SET agent_account = '$sAgentAccount' WHERE user_id = $iUserID ";
      CakeLog::write('debug', "query: " .$query);
      $this->query($query);
      $iCountAffectedRows = $this->getAffectedRows();
      CakeLog::write('debug', "affectedrows, updateUserAgentSettings: " .$iCountAffectedRows);
    }
    return $iCountAffectedRows;
  }

    /*@method: getAllLowersOfUser
    *Getting all childs of current user
    *@params:
    *$iUserID - user identificator
    *@author: Svirsky Evgeny
    *@date: 29.03.2013
    */
    public function getAllLowersOfUser($iUserID) {
        $newIDs = array($iUserID);
        $allIDs = array();
        $listOfIDs = "'$iUserID'";
        while (count($newIDs) > 0) {
            $query = "SELECT id FROM `fr_user` WHERE userParent_id IN ($listOfIDs)";
            $qr = $this->query($query);
            $newIDs = array();
            $listOfIDs = "";
            foreach($qr as $row) {
                $id = $row['fr_user']['id'];
                $newIDs[] = $id;
                $allIDs[] = $id;
                $listOfIDs .= "'$id',";
            }
            $listOfIDs = trim($listOfIDs, ",");
        }
        return $allIDs;
    }

    /*@method: getAllLowersOfUserPayed
    *Getting all childs which payed start invoice of current user
    *@params:
    *$iUserID - user identificator
    *@author: Svirsky Evgeny
    *@date: 29.03.2013
    */
    public function getAllLowersOfUserPayed($allUsers) {
        if (count($allUsers)) {
            $listOfIDs = "";
            foreach($allUsers as $id) {
                $listOfIDs .= "'$id',";
            }
            $listOfIDs = trim($listOfIDs, ",");
            $query = "SELECT user_id FROM `fr_invoice_registered` WHERE typ='1' AND status_id='2' AND user_id IN ($listOfIDs)";
            $qr = $this->query($query);
            foreach($qr as $row) {
                $IDs[] = $row['fr_invoice_registered']['user_id'];
            }
            if (isset($IDs)) {
                return $IDs;
            }
        }
    }

    /*@method: getPartnerLink
    *Making partner link
    *@params:
    *$iUserID - user identificator
    *@author: Svirsky Evgeny
    *@date: 29.03.2013
    */
    public function getPartnerLinkr($iUserID) {
        if ($iUserID) {
            $query = "SELECT a.hash_url, b.status_id, b.typ FROM `fr_user` a, `fr_invoice_registered` b WHERE b.typ='1' AND b.status_id='2' AND a.id=b.user_id AND a.id='$iUserID'";
            $result = $this->query($query);
            if (is_array($result) && $result) {
                return "http://investoriaplus.com/registratsiya/?hash={$result['0']['a']['hash_url']}";
            } else {
                return false;
            }
        }
    }
    public function getPartnerLink($iUserID) {
        if ($iUserID) {
            $query = "SELECT a.hash_url, b.status_id, b.typ FROM `fr_user` a, `fr_invoice_registered` b WHERE b.typ='1' AND b.status_id='2' AND a.id=b.user_id AND a.id='$iUserID'";
            $result = $this->query($query);
            if (is_array($result) && $result) {
                return "http://investoriaplus.com/?hash={$result['0']['a']['hash_url']}";
            } else {
                return false;
            }
        }
    }

    /*@method: getHistoryOfAccount
    *getting history of current account from mt4
    *@params:
    *@author: Svirsky Evgeny
    *@date: 03.04.2013
    */
    public function getHistoryOfAccount($user_info) {
        App::import("Vendor", "wr/class-mtq");
        $myMTQ = new MTQ();
        return array('demoAccount' => $myMTQ->history(false, $user_info), "agentAccount" => $myMTQ->history(true, $user_info));
    }

    /*@method: getChildsRecursively
    *collect childs recursively
    *@params:
    *$aResult - link to array of collection
    *$ID - user identification number
    *@author: Svirsky Evgeny
    *@date: 04.04.2013
    */
    public function getChildsRecursively($aResult, $iID) {
        $query = "SELECT id FROM `fr_user` WHERE userParent_id = $iID";
        $qr = $this->query($query);
        $childs = array();
        foreach($qr as $k => $v) {
            $query = "SELECT first_name, last_name FROM `fr_user` WHERE id='{$v['fr_user']['id']}'";
            $qr = $this->query($query);
            $name = $qr[0]['fr_user']['first_name'] . " " . $qr[0]['fr_user']['last_name'];
            $childs[] = array("id" =>$v['fr_user']['id'], "name" => $name, "childs" => $this->getChildsRecursively($aResult, $v['fr_user']['id']));
        }
        return $childs;
    }

    /*@method: getChildsHTMLRecursively
    *generate HTML for childs recursively
    *@params:
    *$aResult - link to array of collection
    *$ID - user identification number
    *@author: Svirsky Evgeny
    *@date: 04.04.2013
    */
    public function getChildsHTMLRecursively($aResult, $iID) {
        $query = "SELECT id FROM `fr_user` WHERE userParent_id = $iID";
        $qr = $this->query($query);
        $childs = "<ul show='1'>";
        foreach($qr as $k => $v) {
            $query = "SELECT first_name, last_name, id FROM `fr_user` WHERE id='{$v['fr_user']['id']}'";
            $qr = $this->query($query);
            $name = $qr[0]['fr_user']['first_name'] . " " . $qr[0]['fr_user']['last_name'];
            $childs .= "<li><a href='javascript:void(0)' id='{$qr[0]['fr_user']['id']}'>$name</a>";
            $childs .= $this->getChildsHTMLRecursively($aResult, $v['fr_user']['id']);
            $childs .= "</li>";
        }
        $childs .= "</ul>";
        return $childs;
    }

    /*@method: getFirstLevelLowers
    *get all of the first childs
    *@params:
    *@author: Svirsky Evgeny
    *@date: 04.04.2013
    */
    public function getFirstLevelLowers($iUserID) {
        $query = "SELECT * FROM `fr_user` WHERE userParent_id='$iUserID'";
        $aResult = $this->query($query);
        if (is_array($aResult) && $aResult) {
            return $aResult;
        } else {
            return false;
        }
    }

    /*@method: addUserToWhiteList
    *push user to WL list
    *@params:
    *@author: Svirsky Evgeny
    *@date: 06.04.2013
    */
    public function addUserToWhiteList($data) {
        $query = 'REPLACE INTO `fr_wl` (id, user_email, user_registered, flname, company, post, site, tel, contact_type, contact_time, description)
                                             VALUES  (NULL, "'.$data['user_email'].'", NOW(), "'.$data['flname'].'", "'.$data['company'].'", "'.$data['post'].'", "'.$data['site'].'", "'.$data['tel'].'", "'.$data['contact_type'].'", "'.$data['contact_time'].'", "'.$data['description'] .'")';
        $results = $this->query($query);

        $q2 = 'SELECT id FROM main_backoffice.fr_wl ORDER BY id DESC LIMIT 1';
        $results2 = $this->query($q2);

        return $results2[0]['fr_wl']['id'];
    }

    /*@method: addUserToWhiteList
    *push user to IB list
    *@params:
    *@author: Svirsky Evgeny
    *@date: 06.04.2013
    */
    public function addUserToIBList($data) {
        $answers = '';
        foreach ($data['anketa'] as $k => $answer) {
            $answers .= "$k-$answer;";
        }
        $query = 'REPLACE INTO `fr_ib` (id, user_email, user_registered, flname, company, post, site, tel, contact_type, description, anketa)
                                             VALUES  (NULL, "'.$data['user_email'].'", NOW(), "'.$data['flname'].'", "'.$data['company'].'", "'.$data['post'].'", "'.$data['site'].'", "'.$data['tel'].'", "'.$data['contact_type'].'", "'.$data['description'] .'", "' . $answers . '")';
        $results = $this->query($query);

        $q2 = 'SELECT id FROM main_backoffice.fr_ib ORDER BY id DESC LIMIT 1';
        $results2 = $this->query($q2);

        return $results2[0]['fr_ib']['id'];
    }

    /*@method: addUserToWhiteList
    *push user to PAMM list
    *@params:
    *@author: Svirsky Evgeny
    *@date: 06.04.2013
    */
    public function addUserToPAMMList($data) {
        $answers = '';
        foreach ($data['anketa'] as $k => $answer) {
            $answers .= "$k-$answer;";
        }
        $query = 'REPLACE INTO `fr_pamm` (id, user_email, user_registered, flname, company, post, site, tel, contact_type, description, anketa)
                                             VALUES  (NULL, "'.$data['user_email'].'", NOW(), "'.$data['flname'].'", "'.$data['company'].'", "'.$data['post'].'", "'.$data['site'].'", "'.$data['tel'].'", "'.$data['contact_type'].'", "'.$data['description'] .'", "' . $answers . '")';
        $results = $this->query($query);

        $q2 = 'SELECT id FROM main_backoffice.fr_pamm ORDER BY id DESC LIMIT 1';
        $results2 = $this->query($q2);

        return $results2[0]['fr_pamm']['id'];
    }

    /*updating user global status*/
    public function updateStatus($id, $status) {
        $query = "UPDATE `fr_user_settings` set status_id=$status WHERE user_id = $id";
        $this->query($query);
    }

}
?>