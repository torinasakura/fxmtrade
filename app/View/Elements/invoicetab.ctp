<div class="container-fluid"><div class="span12"><p></p></div></div>
<div class="container-fluid">
	<div class="row-fluid">
    <div class="span12">

<div style="margin-bottom: 18px;" class="tabbable box">

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#tab1">Оплата</a></li>
    <li class=""><a data-toggle="tab" href="#tab2">Пополнение</a></li>
    <li class=""><a data-toggle="tab" href="#tab3">Внутренний перевод</a></li>
    <li class=""><a data-toggle="tab" href="#tab4">Снятие</a></li>
  </ul>
  <div class="tab-content">
<?php
  foreach($aInvoiceTabsTypes as $keyType => $aInvoiceTabKey)
  {
    if($keyType == 'pay')
	{
		echo '
		  <div id="tab1" class="tab-pane active">
			<div id="masseg_box" style="display: none; margin: 30px 0 0 0;"></div>
			<div class="fxr-table-header">
			  <div>
				<span class="button add_button" id="'.$keyType.'_add_button" prefix="'.$keyType.'">Подтвердить</span>
				<span class="button delete_button" id="'.$keyType.'_delete_button" prefix="'.$keyType.'">Удалить</span>
			  </div>
			</div>
            <div class="tab-header" >
            </div>
            <table class="table table-striped data-table">
			  <form id="'.$keyType.'_invoice_form" action="/invoice/" method="post" >
				<thead> 
				  <tr>
					<th style="width: 65px;">#</th>
					<th style="width: 80px;">Дата</th>
					<th>Фамилия Имя</th>
					<th style="width: 60px;">Способ</th>
					<th style="width: 100px;">Сумма</th>
					<th style="width: 40px;">БЦ</th>
					<th style="width: 65px;">Состояние</th>
				  </tr>
				</thead>
				<tbody>';
				if(isset($aInvoiceTabsContent[$aInvoiceTabKey]) && is_array($aInvoiceTabsContent[$aInvoiceTabKey]))
				{
				  foreach ($aInvoiceTabsContent[$aInvoiceTabKey] as $aInvoice)
				  {
					$sAccount = array_key_exists("fr_invoice_details", $aInvoice) ? $aInvoice["fr_invoice_details"]["data"] : "";
					$status_class_type = ( (time() - strtotime( $aInvoice["fr_invoice_history"]["add_date"] ) ) / (60*60*24) > 2) ? 'clock_l' : 'clock' ;
					$chekOneLavel = ($aInvoice[0]["countMember"]) ? 'info' : 'chek' ;
					echo '
					  <tr>
						<td><input type="checkbox" name="invoice[]" prefix="'.$keyType.'" value="'.$aInvoice["fr_invoice_registered"]["id"].'" /><spsn style="vertical-align:-1px; margin-left: 10px;">' .$aInvoice["fr_invoice_registered"]["id"]. '</span></td>
						<td>' .strftime("%d.%m.%Y", strtotime($aInvoice["fr_invoice_history"]["add_date"])).'</td>
						<td><span id="' .$aInvoice["fr_invoice_registered"]["user_id"]. '" class="link">' .$aInvoice["fr_user"]["first_name"]. ', '.$aInvoice["fr_user"]["last_name"].'</span></td>
						<td><span style=" left: 0px; top: 0px;" class="paylogo pay_system'.$aInvoice["fr_payment"]["id_payment"].'"></span>'.$sAccount.'</td>
						<td>' .$aInvoice["fr_invoice_registered"]["sum"].' '.$aInvoice["fr_payment"]["currency_code"].'</td>
						<td><span style="margin-left:8px;" class="cc-icon cc-set '.$chekOneLavel.'"></td>
						<td><span style="margin-left:15px;" id="'.$aInvoice["fr_invoice_registered"]["id"].'"  class="cc-icon cc-set '.$status_class_type.'"></td>
					  </tr>
					';
				  }
				}
				  echo '
				</tbody>
			  </form>
			</table>
		  </div>';
	}
	elseif($keyType == 'out')
	{
		echo '
		  <div id="tab2" class="tab-pane">
			<div id="masseg_box" style="display: none; margin: 30px 0 0 0;"></div>
			<div class="fxr-table-header">
			  <div>
				<span class="button add_button" id="'.$keyType.'_add_button" prefix="'.$keyType.'">Подтвердить</span>
				<span class="button delete_button" id="'.$keyType.'_delete_button" prefix="'.$keyType.'">Удалить</span>
			  </div>
			</div>
            <div class="tab-header" >
            </div>
            <table class="table table-striped data-table">
			  <form id="'.$keyType.'_invoice_form" action="/invoice/" method="post" >
				<thead> 
				  <tr>
					<th >#</th>
					<th >Дата</th>
					<th>Фамилия Имя</th>
					<th >Способ</th>
					<th >Сумма</th>
					<th >Платежная информация</th>
					<th >Состояние</th>
				  </tr>
				</thead>
				<tbody>';
				if(isset($aInvoiceTabsContent[$aInvoiceTabKey]) && is_array($aInvoiceTabsContent[$aInvoiceTabKey]))
				{
				  foreach ($aInvoiceTabsContent[$aInvoiceTabKey] as $aInvoice)
				  {
					$sAccount = array_key_exists("fr_invoice_details", $aInvoice) ? $aInvoice["fr_invoice_details"]["data"] : "";
					$status_class_type = ( (time() - strtotime( $aInvoice["fr_invoice_history"]["add_date"] ) ) / (60*60*24) > 2) ? 'clock_l' : 'clock' ;
                    switch ($aInvoice['fr_invoice_registered']['deposit_type']) {
                        case "6" :
                            $sDepositType = "Перевод с торгового счета на расчетный счет.";
                        break;
                        case "7" :
                            $sDepositType = "Перевод с партнёрского счета на расчетный счет.";
                        break;
                        case "8" :
                            $sDepositType = "Вывод средств с расчетного счета";
                        break;
                        case NULL :
                            $sDepositType = "";
                        break;
                    }
					echo '
					  <tr>
						<td><input type="checkbox" name="invoice[]" prefix="'.$keyType.'" value="'.$aInvoice["fr_invoice_registered"]["id"].'" /><spsn style="vertical-align:-1px; margin-left: 10px;">' .$aInvoice["fr_invoice_registered"]["id"]. '</span></td>
						<td>' .strftime("%d.%m.%Y", strtotime($aInvoice["fr_invoice_history"]["add_date"])).'</td>
						<td><span id="' .$aInvoice["fr_invoice_registered"]["user_id"]. '" class="link">' .$aInvoice["fr_user"]["first_name"]. ', '.$aInvoice["fr_user"]["last_name"].'</span></td>
						<td><span style=" left: 0px; top: 0px;" class="paylogo pay_system'.$aInvoice["fr_payment"]["id_payment"].'"></span></td>
						<td>' .$aInvoice["fr_invoice_registered"]["sum"].' '.$aInvoice["fr_payment"]["currency_code"].'</td>
						<td>' .$sDepositType. '</td>
						<td><span style="margin-left:15px;" id="'.$aInvoice["fr_invoice_registered"]["id"].'"  class="cc-icon cc-set '.$status_class_type.'"></td>
					  </tr>
					';
				  }
				}
				  echo '
				</tbody>
			  </form>
			</table>
		  </div>';
	}
	elseif($keyType == 'ref')
	{
		echo '
		  <div id="tab3" class="tab-pane">
			<div id="masseg_box" style="display: none; margin: 30px 0 0 0;"></div>
			<div class="fxr-table-header">
			  <div>
				<span class="button add_button" id="'.$keyType.'_add_button" prefix="'.$keyType.'">Подтвердить</span>
				<span class="button delete_button" id="'.$keyType.'_delete_button" prefix="'.$keyType.'">Удалить</span>
			  </div>
			</div>
            <div class="tab-header" >
            </div>
            <table class="table table-striped data-table">
			  <form id="'.$keyType.'_invoice_form" action="/invoice/" method="post" >
				<thead> 
				  <tr>
					<th style="width: 65px;">#</th>
					<th style="width: 80px;">Дата</th>
					<th>Фамилия Имя</th>					
					<th style="width: 100px;">Сумма</th>
					<th style="width: 35px;">Счет</th>
					<th style="width: 65px;">Состояние</th>
				  </tr>
				</thead>
				<tbody>';
				if(isset($aInvoiceTabsContent[$aInvoiceTabKey]) && is_array($aInvoiceTabsContent[$aInvoiceTabKey]))
				{
				  foreach ($aInvoiceTabsContent[$aInvoiceTabKey] as $aInvoice)
				  {
					$sAccount = array_key_exists("fr_invoice_details", $aInvoice) ? $aInvoice["fr_invoice_details"]["data"] : "";
					$status_class_type = ( (time() - strtotime( $aInvoice["fr_invoice_history"]["add_date"] ) ) / (60*60*24) > 14) ? 'clock_l' : 'clock' ;
					switch($aInvoice["fr_payment"]["id_payment"]){
						case 12:
							$accaunt_class = 'refaccaunt';
							break;
						case 13:
							$accaunt_class = 'agentaccaunt';
							break;
						case 14:
							$accaunt_class = 'tradinaccaunt';
							break;
					}
					$accaunt_class_type = ($aInvoice["fr_invoice_history"]["add_date"] == 12) ? 'clock_l' : 'clock' ;
					echo '
					  <tr>
						<td><input type="checkbox" name="invoice[]" prefix="'.$keyType.'" value="'.$aInvoice["fr_invoice_registered"]["id"].'" /><spsn style="vertical-align:-1px; margin-left: 10px;">' .$aInvoice["fr_invoice_registered"]["id"]. '</span></td>
						<td>' .strftime("%d.%m.%Y", strtotime($aInvoice["fr_invoice_history"]["add_date"])).'</td>
						<td><span id="' .$aInvoice["fr_invoice_registered"]["user_id"]. '" class="link">' .$aInvoice["fr_user"]["first_name"]. ', '.$aInvoice["fr_user"]["last_name"].'</span></td>						
						<td>' .$aInvoice["fr_invoice_registered"]["sum"].' '.$aInvoice["fr_payment"]["currency_code"].'</td>
						<td><span style=" left: 0px; top: 0px;" class="cc-icon cc-set '.$accaunt_class.'"></span>'.$sAccount.'</td>
						<td><span style="margin-left:15px;" id="'.$aInvoice["fr_invoice_registered"]["id"].'"  class="cc-icon cc-set '.$status_class_type.'"></td>
					  </tr>
					';
				  }
				}
				  echo '
				</tbody>
			  </form>
			</table>
		  </div>';
	}
	else
	{
		echo '
		  <div id="tab4" class="tab-pane">
			<div id="masseg_box" style="display: none; margin: 30px 0 0 0;"></div>
			<div class="fxr-table-header">
			  <div>
				<span class="button add_button" id="'.$keyType.'_add_button" prefix="'.$keyType.'">Подтвердить</span>
				<span class="button delete_button" id="'.$keyType.'_delete_button" prefix="'.$keyType.'">Удалить</span>
			  </div>
			</div>
            <div class="tab-header" >
            </div>
            <table class="table table-striped data-table">
			  <form id="'.$keyType.'_invoice_form" action="/invoice/" method="post" >
				<thead> 
				  <tr>
					<th style="width: 65px;">#</th>
					<th style="width: 80px;">Дата</th>
					<th>Фамилия Имя</th>
					<th style="width: 60px;">Способ</th>
					<th style="width: 100px;">Сумма</th>
					<th style="width: 40px;">БЦ</th>
					<th style="width: 65px;">Состояние</th>
				  </tr>
				</thead>
				<tbody>';
				if(isset($aInvoiceTabsContent[$aInvoiceTabKey]) && is_array($aInvoiceTabsContent[$aInvoiceTabKey]))
				{
				  foreach ($aInvoiceTabsContent[$aInvoiceTabKey] as $aInvoice)
				  {
					$sAccount = array_key_exists("fr_invoice_details", $aInvoice) ? $aInvoice["fr_invoice_details"]["data"] : "";
					$status_class_type = ( (time() - strtotime( $aInvoice["fr_invoice_history"]["add_date"] ) ) / (60*60*24) > 2) ? 'clock_l' : 'clock' ;
					$chekOneLavel = ($aInvoice[0]["countMember"]) ? 'info' : 'chek' ;
					echo '
					  <tr>
						<td><input type="checkbox" name="invoice[]" prefix="'.$keyType.'" value="'.$aInvoice["fr_invoice_registered"]["id"].'" /><spsn style="vertical-align:-1px; margin-left: 10px;">' .$aInvoice["fr_invoice_registered"]["id"]. '</span></td>
						<td>' .strftime("%d.%m.%Y", strtotime($aInvoice["fr_invoice_history"]["add_date"])).'</td>
						<td><span id="' .$aInvoice["fr_invoice_registered"]["user_id"]. '" class="link">' .$aInvoice["fr_user"]["first_name"]. ', '.$aInvoice["fr_user"]["last_name"].'</span></td>
						<td><span style=" left: 0px; top: 0px;" class="paylogo pay_system'.$aInvoice["fr_payment"]["id_payment"].'"></span>'.$sAccount.'</td>
						<td>' .$aInvoice["fr_invoice_registered"]["sum"].' '.$aInvoice["fr_payment"]["currency_code"].'</td>
						<td><span style="margin-left:8px;" class="cc-icon cc-set '.$chekOneLavel.'"></td>
						<td><span style="margin-left:15px;" id="'.$aInvoice["fr_invoice_registered"]["id"].'"  class="cc-icon cc-set '.$status_class_type.'"></td>
					  </tr>
					';
				  }
				}
				  echo '
				</tbody>
			  </form>
			</table>
		  </div>';
	}
  }           
?>
 </div>
</div>

    </div>
  </div>
</div>