<?php
// app/Controller/Component/NavigationComponent.php
class NavigationComponent extends Component
{
    private $treeToUse = array();

    private $tree_cp = array(
        array(1, 'Обзор', '/', null, 'icon-home', '/'),
        array(5, 'Участники', '/user/userlisting_active', null, 'icon-user', '/user'),
        array(16, 'подтвержденные', '/user/userlisting_active', 5, '', null),
        array(17, 'не подтвержденные', '/user/userlisting_notactive', 5, '', null),
        array(2, 'Заявки', '/invoice/listing', null, 'icon-inbox', '/invoice'),
        //array(3, 'Статистика', '#', null, 'icon-bar-chart', null),
        array(4, 'Счета', '/account/onetimebonuslisting/1', null, 'icon-list-alt', '/account'),
        array(6, 'Открытые', '/invoice/listing', 2, '', null),
        array(7, 'Закрытые', '/invoice/paidlisting', 2, '', null),
        array(8, 'Частично оплаченые', '#', 2, '', null),
        array(9, 'Разовый бонус', '/account/onetimebonuslisting/1', 4, '', null),
        array(10, 'Бонусный цикл', '/account/bonuslooplisting/4', 4, '', null),
        array(11, 'Поступления', '/account/revenuelisting/5', 4, '', null),
        array(12, 'Резервный фонд', '/account/reservefondslisting/3', 4, '', null),
        array(13, 'Участники', '/account/participantlisting/0', 4, '', null),
        array(14, 'Бонусы', '/bonus/', null, 'icon-money', '/bonus'),
        array(15, 'Письма', '/email', null, 'icon-envelope', '/email'),
        //array(18, 'Настройки', '#', null, 'icon-wrench', null),
        array(19, 'Платежи', '#', 18, '', null),
        array(20, 'Пользователи', '#', 18, '', null),
        array(21, 'Матрица бонусных циклов', '/bonus/', 14, '', '/bonus'),
        array(22, 'Обзор', '#', 14, '/bonus/info/', null),
        array(23, 'Выход', '/login/logout', null, 'icon-remove', null)
    );

    private $tree_pc = array(
        array(1, 'Общие сведения', '/', null, null, null),
        array(2, 'История счета', '/member/history/', 1, null, null),
        array(3, 'Личные данные', '/member/', null, null, null),
        array(4, 'Партнеры', '/member/partners', null, null, null),
        array(5, 'Агентская сеть', '/member/agents/', null, null, null),
        array(6, 'Платежи', '#', null, null, null),
        array(7, 'Пополнение торгового счета', '/payment/', 6, null, null),
        array(8, 'Пополнение депозита ', '/payment/funding/', 6, null, null),
        array(8, 'Перевод средств на расчетный счет', '/payment/transfer/', 6, null, null),
        array(9, 'Вывод средств', '/payment/withdrawal/', 6, null, null),
        array(10, 'История заявок', '/payment/history/', 6, null, null),
        //array(11,'Менеджер бонусов', '/bonus/', null),
        array(12, 'Помощь', '/member/support', null, null, null),
        array(13, 'Служба поддержки', '/member/support/', 12, null, null),
        array(14, 'Для рекламы', '/member/advice/', 12, null, null),
        //array(15,'Форум', 'http://forum.forex-backoffice.com/', 12,null,null),
        array(16, 'Выход', '/login/logout', null, null, null),
    );

    private $tree_pc_pay = array(
        array(1, 'Общие сведения', '#', null, null, null),
        array(2, 'История счета', '/member/history/', 1, null, null),
        //array(3, 'Личные данные', '#', null, null, null),
        array(4, 'Партнеры', '#', null, null, null),
        array(5, 'Агентская сеть', '#', null, null, null),
        array(6, 'Платежи', '#', null, null, null),
        //array(7,'Пополнение торгового счета',	'/payment/', 6),
        array(8, 'Пополнение депозита ', '/payment/funding/', 6, null, null),
        array(8, 'Перевод средств на расчетный счет', '/payment/transfer/', 6, null, null),
        array(9, 'Вывод средств', '/payment/withdrawal/', 6, null, null),
        array(10, 'История заявок', '/payment/history/', 6, null, null),
        //array(11,'Менеджер бонусов', '/bonus/', null),
        array(12, 'Помощь', '#', null, null, null),
        array(13, 'Служба поддержки', '/member/support/', 12, null, null),
        //array(14,'Для рекламы', '/member/advice/', 12,null,null),
        //array(15,'Форум', 'http://forum.forex-backoffice.com/', 12,null,null),
        //array(16, 'Выход', '/login/logout', null, null, null),
        array(17, 'Общие сведения', '/', 1, null, null),
        //array(18, 'Личные данные', '/member/', 3, null, null),
        array(19, 'Партнеры', '/member/partners', 4, null, null),
        array(20, 'Агентская сеть', '/member/agents/', 5, null, null),
        //array(21, 'Выход', '/login/logout', 16, null, null),
    );

    private $tree_pc_nonpay = array(
        array(1, 'Общие сведения', '#', null, null, null),
        array(2, 'История счета', '/member/history/', 1, null, null),
        //array(3, 'Личные данные', '#', null, null, null),
        array(4, 'Партнеры', '#', null, null, null),
        array(5, 'Агентская сеть', '#', null, null, null),
        array(6, 'Платежи', '#', null, null, null),
        array(7, 'Пополнение торгового счета', '/payment/', 6, null, null),
        //array(8,'Пополнение депозита ', '/payment/funding/', 6),
        array(8, 'Перевод средств на расчетный счет', '/payment/transfer/', 6, null, null),
        array(9, 'Вывод средств', '/payment/withdrawal/', 6, null, null),
        array(10, 'История заявок', '/payment/history/', 6, null, null),
        //array(11,'Менеджер бонусов', '/bonus/', null),
        array(12, 'Помощь', '#', null, null, null),
        array(13, 'Служба поддержки', '/member/support/', 12, null, null),
        //array(14,'Для рекламы', '/member/advice/', 12,null,null),
        //array(15,'Форум', 'http://forum.forex-backoffice.com/', 12,null,null),
        //array(16, 'Выход', '/login/logout', null, null, null),
        array(17, 'Общие сведения', '/', 1, null, null),
        //array(18, 'Личные данные', '/member/', 3, null, null),
        array(19, 'Партнеры', '/member/partners', 4, null, null),
        array(20, 'Агентская сеть', '/member/agents/', 5, null, null),
        //array(21, 'Выход', '/login/logout', 16, null, null),

    );

    private function getChildren($parentId, $is_pay = false)
    {
        if (defined("ENVIRONMENT")) {
            if (ENVIRONMENT == 'cp') {
                $access_property_tree = "tree_" . ENVIRONMENT;
            } else {
                switch ($is_pay) {
                    case true:
                        $access_property_tree = "tree_pc_pay";
                        break;
                    case false:
                        $access_property_tree = "tree_pc_nonpay";
                        break;
                }
            }
            $this->treeToUse = $this->$access_property_tree;
        }

        $aResult = array();
        foreach ($this->treeToUse as $element) {
            if ($element[3] == $parentId)
                $aResult[] = $element;
        }
        return $aResult;
    }


    private function cact($mass, $url)
    {
        $result = 1;
        foreach ($mass as $element) {
            $tmp = array_search($url, $element, true);
            if ($tmp > 0) {
                $result = $element[3];
            }
        }
        //echo array_search($url, $mass[0], true);
//		echo $result;
        return $result;
    }


    private function buildLevel($parentId, $n = false, $is_pay = false)
    {
        $sOutput = "";
        // take the right level
        $aChildren = $this->getChildren($parentId, $is_pay);
        if (count($aChildren) > 0) {
            // Отрисуем уровень
            // $sOutput .= '<ul id="menu">';

            foreach ($aChildren as $element) {
                // $sOutput .= '<li class="icon-plus-sign" id="menu-item-'. $element[0]. '">';
                // $sOutput .= '<a href="' .$element[2]. '">' .$element[1]. '</a>';
                if ($parentId == 0) {
                    if (strpos($_SERVER["REQUEST_URI"], $element[5]) === 0) {
                        if ($element[0] != 1) {
                            $sOutput .= '<li class="active">';
                        } else {
                            if (strlen($_SERVER["REQUEST_URI"]) === 1) {
                                $sOutput .= '<li class="active">';
                            } else {
                                $sOutput .= '<li>';
                            }
                        }
                    } else {
                        $sOutput .= '<li>';
                    }
                } else {
                    if (strpos($_SERVER["REQUEST_URI"], $element[2]) === 0) {
                        //if ($element[2] == $_GET["url"]){
                        $sOutput .= '<li class="active">';
                    } else {
                        $sOutput .= '<li>';
                    }
                }
                //$sOutput .= '<li class="active">';

                $sOutput .= '<a href="' . $element[2] . '">';
                $sOutput .= '<i class="' . $element[4] . '"></i>' . $element[1];
                $sOutput .= '</a>';
                // $sOutput .= '</li>';

                // Рекурсивно отрисуем вложенный уровень
                // $sOutput .= $this->buildLevel($element[0], $element[3], $is_pay);
                $sOutput .= '</li>';
            }
            // $sOutput .= '</ul>';
        }

        return $sOutput;
    }


    private function buildLevelpc($parentId, $n = false, $is_pay = false)
    {
        $sOutput = "";
        // take the right level
        $aChildren = $this->getChildren($parentId, $is_pay);
        $ca = $this->cact($this->tree_pc_pay, $_SERVER["REQUEST_URI"]);
        if (count($aChildren) > 0) {
            // Отрисуем уровень
            $sOutput .= '<ul class="nav nav-list">';

            foreach ($aChildren as $element) {
                if ($ca == $element[0]) {
                    //if ($element[2] == $_GET["url"]){
                    $sOutput .= '<li class="active open">';
                } else {
                    $sOutput .= '<li>';
                }

                $sOutput .= '<a class="dropdown-toggle" href="' . $element[2] . '"><span>' . $element[1] . '</span></a>';

                // Рекурсивно отрисуем вложенный уровень
                $sOutput .= $this->buildLevelpcc($element[0], $element[3], $is_pay);
                $sOutput .= '</li>';
            }
            $sOutput .= '</ul>';
        }

        return $sOutput;
    }

    private function buildLevelpcc($parentId, $n = false, $is_pay = false)
    {
        $sOutput = "";
        // take the right level
        $aChildren = $this->getChildren($parentId, $is_pay);
        if (count($aChildren) > 0) {
            // Отрисуем уровень
            $sOutput .= '<ul class="submenu">';

            foreach ($aChildren as $element) {
                if ($_SERVER["REQUEST_URI"] == $element[2]) {
                    //if ($element[2] == $_GET["url"]){
                    $sOutput .= '<li class="active">';
                } else {
                    $sOutput .= '<li>';
                }
                $sOutput .= '<a href="' . $element[2] . '"><span>' . $element[1] . '</span></a>';
            }
            $sOutput .= '</ul>';
        }

        return $sOutput;
    }

    public function buildNavigation()
    {
        return $this->buildLevel(0);
    }

    public function buildNavigation_s()
    {
        $url = $_SERVER["REQUEST_URI"];
        $par = 1;
        $tree = array(
            array(5, 'Участники', '/user/userlisting_active', '/user/userlisting_notactive'),
            array(2, 'Заявки', '/invoice/listing', '/invoice/paidlisting'),
            array(4, 'Счета', '/account/onetimebonuslisting/1', '/account/bonuslooplisting/4', '/account/revenuelisting/5', '/account/reservefondslisting/3', '/account/participantlisting/0'),
            array(14, 'Менеджер бонусов', '/bonus/', '/bonus/info/'),
            array(15, 'Письма', '/email', null, 'icon-envelope'),
            //array(18, 'Настройки', '#', null, 'icon-wrench'),
        );
        foreach ($tree as $tmp) {
            if (in_array($url, $tmp)) {
                $par = $tmp[0];
                break;
            }
        }
        return $this->buildLevel($par);
    }

    /*Построить навигацию для пользователя не оплатившего*/
    public function buildNavigationNonPay()
    {
        return $this->buildLevelpc(0, false, false);
    }

    /*Построить навигацию для оплатившего пользователя*/
    public function buildNavigationPayed()
    {
        return $this->buildLevelpc(0, false, true);
    }
}
?>
