<?php

  class UserStatusInfoComponent extends Component
  {
    private $aUserStatusInfo = null;
    
    
    public function setUserStatusInfo($aUserStatuses)
    {
      if($aUserStatuses != null && count($aUserStatuses) > 0 )
      {      
        $this->aUserStatusInfo = array();
        foreach($aUserStatuses as $aUserStatus)
        {
          $this->aUserStatusInfo[$aUserStatus["UserStatus"]["id"]] = $aUserStatus["UserStatus"];
        }
        /** Information of the Global UserStatus 
         *
          1 => 
            array
              'id' => string '1' (length=1)
              'status_name' => string 'Agent' (length=5)
              'status_sku' => string 'A' (length=1)
              'total_partner' => string '5' (length=1)
              'bonus_1' => string '8.00' (length=4)
              'bonus_2' => string '16.00' (length=5)
              'brokerage_profits' => string '1.00' (length=4)
              'brokerage_transaction' => string '1.00' (length=4)
              'icon' => string 'award_star_bronze_1' (length=19)
              'info_text' => string 'ко-во партнеров от 1 до 5' (length=42)
          2 => 
            array
              'id' => string '2' (length=1)
              'status_name' => string 'Consultant' (length=10)
              'status_sku' => string 'C' (length=1)
              'total_partner' => string '15' (length=2)
              'bonus_1' => string '12.00' (length=5)
              'bonus_2' => string '24.00' (length=5)
              'brokerage_profits' => string '3.00' (length=4)
              'brokerage_transaction' => string '0.30' (length=4)
              'icon' => string 'award_star_bronze_2' (length=19)
              'info_text' => string 'ко-во партнеров от 6 до 15' (length=43)
         *
         */
      }
      //var_dump(Configure::read("UserStatus"));
    }
    
    public function getUserStatusInfo()
    {
      return $this->aUserStatusInfo;
    }
    
    public function getUserStatusForInvite($iCountUserInvites)
    {
      $aReturnUserStatus = null;
      if(is_numeric($iCountUserInvites) && $iCountUserInvites >= 0)
      {
        foreach($this->aUserStatusInfo as $aUserStatus)
        {
          if($iCountUserInvites <= (int)$aUserStatus["total_partner"])
          {
            $aReturnUserStatus = $aUserStatus;
            break;
          }
        }
      }
      return $aReturnUserStatus;
    }
    
    public function calculateUserStatus($aUser)
    {
      $iCountTotalMemberOfUser = (int)$aUser[0]["countpartner"];
			$aUserStatusInfoInvite = $this->getUserStatusForInvite($iCountTotalMemberOfUser);
        
      if($aUser["fr_user_settings"]["user_status_id"] > 0)
      {
        $iUserStatusInfoID = $aUser["fr_user_settings"]["user_status_id"];
        $aAllUserStatusInfos = $this->getUserStatusInfo();
        $aUserStatusInfo = $aAllUserStatusInfos[$iUserStatusInfoID];
        
        $iTotalParnterInvite = $aUserStatusInfoInvite["total_partner"];
        $iTotalParnter = $aUserStatusInfo["total_partner"];
        
        if($iTotalParnterInvite >= $iTotalParnter)
        {
          $aUser["fr_user_settings"]["user_status_name"] = $aUserStatusInfoInvite["status_name"];
          $aUser["fr_user_status"] = $aUserStatusInfoInvite;
        }
        else
        {
          $aUser["fr_user_settings"]["user_status_name"] = $aUserStatusInfo["status_name"];
          $aUser["fr_user_status"] = $aUserStatusInfo;
        }
      }
      else
      {
        $aUser["fr_user_settings"]["user_status_name"] = $aUserStatusInfoInvite["status_name"];
        $aUser["fr_user_status"] = $aUserStatusInfoInvite;
      }
      return $aUser;
    }
}
?>