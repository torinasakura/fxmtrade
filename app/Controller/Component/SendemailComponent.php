<?php
App::uses('CakeEmail', 'Network/Email');

class SendemailComponent extends Component
{
	public $name = 'Sendemail';
	private $testMailSend = false;

	public function prepareSystemMail($emailInformation, $userData, $additionalContent = array())
    {
		if($this->testMailSend == true){
			$to = "admin@fxmtrade.com";
		}
		else{
			$to = $userData['user_email'];
		}

		$subject = $emailInformation["fr_mail"]["subject"];
		$message = $emailInformation["fr_mail"]["body"];
		$title = ($userData['sex']) ? 'Господин' : 'Госпожа';

		$replaceConstants = array(
                      "%TITLE%",
                      "%LNAME%",
                      "%FNAME%",
                      "%MAIL%",
					  "%PARTNER%",
                      "%PASSWORD%",
					  "%PAYLINK%",
                      "%CODE%",
                      "%PARTNERNUM%",
                      "%BONUSRUND%",
                      "%PROGRAMMTYPE%",
                      "%PHONE_1%",
                      "%SKYPE%",
                      "%INVOICE%",
                      "%SUM%",
                      "%PAYMENT%",
                      "%DATEOFPAYMENT%",
                      "%RECOVERYEKAY%",
                      "%COUNTRY%",
                      "%PARENT_INFO%",
					  "%RADEPOSIT%",
					  "%PAYDATA%");

		$replaceVars =  array(
                      $title,
                      $userData['last_name'],
                      $userData['first_name'],
                      $userData['user_email'],
					  $userData['partner_id'],
                      $userData['user_pass_clr'],
                      "http://pc.fxmbackoffice.com/payment",
                      "%CODE%",
                      "%PARTNERNUM%",
                      "%BONUSRUND%",
                      "%PROGRAMMTYPE%",
                      "%PHONE_1%",
                      "%SKYPE%",
                      "%INVOICE%",
                      "%SUM%",
                      "%PAYMENT%",
                      "%DATEOFPAYMENT%",
                      "%RECOVERYEKAY%",
                      "%COUNTRY%",
                      "%PARENT_INFO%",
					  "%RADEPOSIT%",
					  "%PAYDATA%"
					  );

		$subject = str_replace	($replaceConstants, $replaceVars, $subject);
		$message = str_replace	($replaceConstants, $replaceVars, $message);

		$this->sendMail($to, $subject, $message, 'fxm');
	}

	private function sendMail($to, $subject, $message, $from)
	{
		$Email = new CakeEmail($from);
		$Email->to($to);
		$Email->subject($subject);
		$Email->emailFormat('both');
		$Email->sender("no-reply@fxmtrade.com", "Investoria Plus");
		$Email->send($message);
	}
}
?>