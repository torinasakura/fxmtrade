<?php

  class UtilComponent extends Component
  {
      /*@method: convertDateToFormat
      *Formatting the string like "2011-02-30 20:20:59" to any other format
      *@params:
      *$string - input string
      *$format - format to convert like d.m.Y
      *@author: Svirsky Evgeny
      *@date: 30.03.2013
      */
      public function convertDateToFormat($string, $format) {
          $dateAndTime = explode(" ", trim($string));
          $eDate = explode("-", $dateAndTime[0]);
          $eTime = explode(":", $dateAndTime[1]);
          $dateTimeStamp = mktime($eTime[0], $eTime[1], $eTime[2], $eDate[1], $eDate[2], $eDate[0]);
          return date($format, $dateTimeStamp);
      }
  }
?>