<script type="text/javascript" src="http://investoriaplus.com/wp-content/themes/yoo_infinite_wp/warp/js/regexp.js"></script>
<script type="text/javascript" src="http://investoriaplus.com/wp-content/themes/yoo_infinite_wp/warp/js/valid.js"></script>
<script type="text/javascript" src="http://investoriaplus.com/wp-content/themes/yoo_infinite_wp/warp/js/fxf.js"></script>
<link rel="stylesheet" href="http://investoriaplus.com/wp-content/themes/yoo_infinite_wp/warp/js/validate.css"/>

<script>
    $( function() {
        var $fxf = $( '#InvoiceIndexForm' );

        $fxf.validationEngine();
    });
</script>
<div id="withdrawal_container" style="position:relative; margin-top: 0px; width: 100%;">
	<div class="fxr-siderbar-header"><p>Перевод средств на расчетный счет</p></div>
    <div id="withdrawal_body">
    <div id="flashMessage" class="example_class"><?php echo $this->Session->flash(); ?></div>
        <div class="yui3-u-1-3">
            <div class="fx_statement_box">
                <div class="box_header">Партнерский счёт: <span><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['user_account'] : "Нет"; ?></span></div>
                <div style="padding:10px;">
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Дата открытия счета:</label>
                        <div class="thefield">
                            <span><b><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['reg_date'] : "Нет"; ?></b></span>
                        </div>
                    </div>
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Депозит:</label>
                        <div class="thefield">
                            <span class="digit"><b><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['deposit'] . " USD" : "Нет"; ?></b></span>
                        </div>
                    </div>
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Снятие:</label>
                        <div class="thefield">
                            <span class="digit"><b><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['withdrawal'] . " USD" : "Нет"; ?></b></span>
                        </div>
                    </div>
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Баланс:</label>
                        <div class="thefield">
                            <span class="digit"><b><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['profit/loss'] . " USD" : "Нет"; ?></b></span>
                        </div>
                    </div>
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Прибыль:</label>
                        <div class="thefield">
                            <span class="digit"><b><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfodemo['profit'] . " USD" : "Нет"; ?></b></span>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
        <div class="yui3-u-1-3">
            <div class="fx_statement_box">
                <div class="box_header">Торговый счёт: <span><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['user_account'] : "Нет"; ?></span></div>
                <div style="padding:10px;">
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Дата открытия счета:</label>
                        <div class="thefield">
                            <span><b><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['reg_date'] : "Нет"; ?></b></span>
                        </div>
                    </div>
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Депозит:</label>
                        <div class="thefield">
                            <span class="digit"><b><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['deposit'] . " USD" : "Нет"; ?></b></span>
                        </div>
                    </div>
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Снятие:</label>
                        <div class="thefield">
                            <span class="digit"><b><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['withdrawal'] . " USD" : "Нет"; ?></b></span>
                        </div>
                    </div>
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Баланс:</label>
                        <div class="thefield">
                            <span class="digit"><b><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['profit/loss'] . " USD" : "Нет"; ?></b></span>
                        </div>
                    </div>
                    <div class="fieldwrapper">
                        <label class="styled input_medium">Прибыль:</label>
                        <div class="thefield">
                            <span class="digit"><b><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['profit'] . " USD" : "Нет"; ?></b></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <form method="post" action="/payment/transfer">
                <p>Выберите счет с которого хотите вывести денежные средства: </p>
                <div class="fieldwrapper medium">
                    <div class="thefield">
                        <input id="agent" class="" type="radio" name="deposit_account" value="agent" />
                    </div>
                    <label class="styled">Партнерский счёт</label>
                </div>
                <div class="fieldwrapper medium">
                    <div class="thefield">
                        <input id="demo" class="" type="radio" name="deposit_account" value="demo" />
                    </div>
                    <label class="styled">Торговый счет</label>
                </div>
                <div class="fieldwrapper medium">
                    <label class="styled input">Сумма:</label>
                    <div class="thefield">
                        <input id="sum" class="" type="text" name="sum" value="" />
                    </div>
                </div>
                <input id="submit" type="submit" value="Продолжить">
            </form>
        </div>
    </div>
</div>
