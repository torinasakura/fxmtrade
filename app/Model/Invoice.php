<?php
  
App::uses('AppModel', 'Model');

class Invoice extends AppModel
{
  public $name = 'Invoice';
  
  public $primaryKey = 'id';
  public $useTable = 'invoice_registered';
  
  function getAllActivPaysystem()
  {
	  	$aResult = null;
		$query = "SELECT * FROM `fr_payment` WHERE aktiv = true AND typ = 1 ORDER BY id ASC";
		$sql = $this->query($query);
		$aResult = $sql;
		return $aResult;
  }
  

  /*adding type of paying
  * by default  - $typ = 1 - Paying for trade place
  * $typ = 2 - Paying for funding deposit
  * $typ = 3 - Withdraw money from deposit
  * $typ = 4 - Transfer money to another deposit inside the system
  */
  function getPaysystemById($iIdSystem, $typ = "1")
  {
	  	$return = null;
		$query = "SELECT * FROM `fr_payment` WHERE aktiv = true AND typ = 1 AND id = '$iIdSystem'";
		$sql = $this->query($query);
		$aResults = $sql[0];
		if($aResults != null && is_array($aResults))
		{
			$return = $aResults;
		}
        if ($typ != "1" && $typ) {
            $return['fr_payment']['typ'] = $typ;
        }
        return $return;
  }
  
  public function getProgrammByID($iProgrammID)
  {
    $aReturn = null;
    $query = "SELECT fr_programm.*
              FROM fr_programm
              WHERE fr_programm.id = $iProgrammID";
    $aResults = $this->query($query);
    
    if($aResults != null && is_array($aResults)){
      $aReturn = $aResults[0];
    }
    else{
      CakeLog::write('error', 'No programm with id: ' .$iProgrammID);
    }
    
    return $aReturn;
  }
  
	public function isInvoicAktiv()
	{
	  	$return = null;
		$query = "SELECT id FROM `fr_invoice_registered` WHERE user_id = ".USER_ID." AND status_id = 1 AND typ = 1";
		$sql = $this->query($query);
		if($sql != null && is_array($sql))
			$return = $sql[0]['fr_invoice_registered']['id'];
		return $return;	
	}
  
  public function addInvoice($newData, $typ = "1", $deposit_type = NULL)
  {
	  $iCountAffectedRows = 0;
      if ($typ != 1 && $typ) {
          $newData['typ'] = $typ;
      }
      if ($deposit_type) {
          $newData['deposit_type'] = $deposit_type;
      }
      $this->save($newData);
      $iInvoiceID = $this->getInsertID();
      $this->insertInvoicesHistory($aInvoiceIDs = array($iInvoiceID), $typ);
	  return $iInvoiceID;
  }
  
  public function getAllPaidInvoice()
  {
    $aResults = null;
    $query = "Select fr_invoice_registered.id, fr_invoice_registered.sum,
  fr_invoice_history.add_date, fr_payment.name_payment_systems,
  fr_payment.currency_code, fr_invoice_registered.user_id,
  fr_invoice_registered.status_id, fr_user.last_name, fr_user.first_name,
  fr_invoice_typ.typ_name, fr_invoice_typ.typ, o.last_name, o.first_name
From fr_invoice_registered Inner Join
  fr_invoice_history On fr_invoice_registered.id = fr_invoice_history.invoice_id
    And fr_invoice_registered.status_id = fr_invoice_history.status_id
  Inner Join
  fr_payment On fr_invoice_registered.payments_id = fr_payment.id Inner Join
  fr_user On fr_invoice_registered.user_id = fr_user.id Inner Join
  fr_invoice_typ On fr_invoice_registered.typ = fr_invoice_typ.id Inner Join
  fr_user o On fr_invoice_history.user_id = o.id
Where fr_invoice_registered.status_id = 2";
    $aResults = $this->query($query);
  
    return $aResults;
  }
  
  public function getInvoiceByID($iInvoiceID)
  {
    $aReturn = null;
    $query = "SELECT fr_invoice_registered.id, fr_invoice_registered.sum, fr_invoice_history.add_date, fr_payment.name_payment_systems, fr_payment.currency_code, fr_payment.id,
            fr_invoice_registered.user_id, fr_invoice_status.status_name, fr_invoice_registered.status_id, fr_user.last_name, fr_user.first_name, fr_user.id as user_id, fr_user.userParent_id
              FROM fr_invoice_registered
              INNER JOIN fr_invoice_history On fr_invoice_registered.id = fr_invoice_history.invoice_id
              INNER JOIN fr_payment ON fr_invoice_registered.payments_id = fr_payment.id
              INNER JOIN fr_invoice_status ON fr_invoice_registered.status_id = fr_invoice_status.id 
              INNER JOIN fr_user ON fr_invoice_registered.user_id = fr_user.id
              WHERE fr_invoice_registered.id = $iInvoiceID";
    $aResults = $this->query($query);
    if($aResults != null && is_array($aResults)){
      $aReturn = $aResults;
    }
    else{
      CakeLog::write('user_error', 'No invoiceinformation for invoice with id: ' .$iInvoiceID);
    }
    
    return $aReturn;
  }
  
  public function getInvoceHistoryByType($iType)
  {
	  $aReturn = null;
	  $query = "Select fr_invoice_registered.id, fr_invoice_registered.sum,
              fr_invoice_history.add_date, fr_payment.name_payment_systems,
              fr_payment.currency_code, fr_invoice_registered.user_id,
              fr_invoice_status.status_name, fr_invoice_registered.status_id, fr_invoice_registered.payments_id
            From fr_invoice_registered Inner Join
              fr_invoice_history On fr_invoice_registered.id = fr_invoice_history.invoice_id
              Inner Join
              fr_payment On fr_invoice_registered.payments_id = fr_payment.id Inner Join
              fr_invoice_status On fr_invoice_registered.status_id = fr_invoice_status.id
            Where fr_invoice_history.status_id = 1 AND fr_invoice_registered.typ = " .$iType. " AND fr_invoice_registered.user_id = ".USER_ID.' order by id desc';
		$aResults = $this->query($query);
		if($aResults != null && is_array($aResults))
    	{
			$aReturn = $aResults;
		}
		return $aReturn;
  }
  
  public function getInvoicesByType($iType)
  {
    $aReturn = null;
    
    $sSelect = "";
    $sLeftJoin = "";
    if($iType == 3)
	{ //catch the invoice_detail data
      $sSelect .= ", fr_invoice_details.data ";
      $sLeftJoin .= " LEFT JOIN fr_invoice_details ON fr_invoice_details.invoice_id = fr_invoice_registered.id ";
    }
    
    $query = "  SELECT fr_invoice_registered.id, fr_invoice_registered.sum, fr_invoice_registered.typ,fr_invoice_registered.deposit_type,  fr_invoice_history.add_date, fr_payment.name_payment_systems,
              fr_payment.id as id_payment, fr_payment.currency_code, fr_invoice_registered.user_id, fr_invoice_status.status_name, fr_invoice_registered.status_id,
              fr_user.last_name, fr_user.first_name, fr_bonus_tables.table_id, fr_invoice_registered.payments_id $sSelect
                FROM fr_invoice_registered
                INNER JOIN fr_invoice_history On fr_invoice_registered.id = fr_invoice_history.invoice_id
                INNER JOIN fr_payment On fr_invoice_registered.payments_id = fr_payment.id
                INNER JOIN fr_invoice_status On fr_invoice_registered.status_id = fr_invoice_status.id
                INNER JOIN fr_user On fr_invoice_registered.user_id = fr_user.id
				Left Join fr_bonus_tables On fr_user.userParent_id = fr_bonus_tables.user_id
                $sLeftJoin
                WHERE fr_invoice_registered.typ = " .$iType. " AND fr_invoice_registered.status_id = 1 ORDER BY id DESC";
    $aResults = $this->query($query);
    
    if($aResults != null && is_array($aResults))
    {
      if($iType == 3)//unserialize the invoice_detail data
      { 
        for($i=0 ; $i < count($aResults) ; $i++)
        {
          $aUnserialize = unserialize($aResults[$i]["fr_invoice_details"]["data"]);
          $aResults[$i]["fr_invoice_details"]["data"] = $aUnserialize[0];
        }
      }
      $aReturn = $aResults;
    }
        
    return $aReturn;
  }
  
  public function getDeletedInvoicesFromToday()
  {
    $aReturn = null;
    
    $query = "SELECT fr_user.id
              FROM fr_user
              INNER JOIN fr_invoice_registered ON fr_user.id = fr_invoice_registered.user_id
              INNER JOIN fr_invoice_history ON fr_invoice_history.invoice_id = fr_invoice_registered.id
              WHERE fr_invoice_history.status_id = 5 AND DATE(fr_invoice_history.add_date) = CURRENT_DATE()";
    
    $aResults = $this->query($query);
    if($aResults != null && is_array($aResults)){
      $aReturn = $aResults;
    }
    
    return $aReturn;
  }
  
  public function updateRejecteInvoices($aInvoiceIDs)
  {
    $iCountAffectedRows = 0;
    if($aInvoiceIDs != null && is_array($aInvoiceIDs))
    {
      $sImplodeIDs = implode(',', $aInvoiceIDs);
      $query = "UPDATE fr_invoice_registered SET status_id = 5 WHERE id IN ( ".$sImplodeIDs." )";
      $this->query($query);
      $iCountAffectedRows = $this->getAffectedRows();
    }
    return $iCountAffectedRows;
  }
  
  public function updateAcceptedInvoices($iInvoiceID, $fSum)
  {
    $iCountAffectedRows = 0;
    if($iInvoiceID > 0)
    {
      $query = "UPDATE fr_invoice_registered SET status_id = 2, sum = $fSum WHERE id = $iInvoiceID ";
      $this->query($query);
      CakeLog::write('debug', "query: " . $query);
      
      $iCountAffectedRows = $this->getAffectedRows();
      CakeLog::write('debug', "affectedrows, updateAcceptedInvoices: " .$iCountAffectedRows);
    }
    return $iCountAffectedRows;
  }
  
  public function insertInvoicesHistory($aInvoiceIDs)
  {
    $iCountAffectedRows = 0;
    if($aInvoiceIDs != null && is_array($aInvoiceIDs))
    {
      $sImplodeIDs = implode(',', $aInvoiceIDs);
      $query = "INSERT INTO fr_invoice_history(invoice_id, status_id, user_id, add_date) SELECT id, status_id, ".USER_ID.", NOW() FROM fr_invoice_registered WHERE id IN ( ".$sImplodeIDs." )";
      $this->query($query);
      $iCountAffectedRows = $this->getAffectedRows();
    }
    return $iCountAffectedRows;
  }
  
  public function insertStatement($iInvoiceID, $aCost, $iUserID)
  {
    $iCountAffectedRows = 0;
    if($iInvoiceID > 0)
    {
      $query = "INSERT INTO fr_statement(invoice_id, sum, statement_typ) VALUES
                ($iInvoiceID, ".$aCost["fr_programm"]["cost_bonus"].", 1),
                ($iInvoiceID, ".$aCost["fr_programm"]["cost_website"].", 3),
                ($iInvoiceID, ".$aCost["fr_programm"]["cost_bonus_loop"].", 4),
                ($iInvoiceID, ".$aCost["fr_programm"]["cost_income"].", 5)";
      $this->query($query);
      CakeLog::write('debug', "query: " .$query);
      $iCountAffectedRows = $this->getAffectedRows();
      CakeLog::write('debug', "affectedrows, insertStatement: " .$iCountAffectedRows);
    }
    return $iCountAffectedRows;
  }
}
?>