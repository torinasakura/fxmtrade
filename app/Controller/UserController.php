<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UserController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'User';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array("User");

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	
	public function display() {
    
	}
	
	function login() {
	}
	
	public function userinfo($iUserID)
	{
		if($this->request->is("ajax") && !empty($iUserID))
		{
			if(is_numeric($iUserID))
			{
				$aExtraUserInfoType = $this->UserInfo->getExtraUserInfoType($iUserID);
				//call to get the infos about the user
                $aUser = $this->UserInfo->getUserFullInfo($iUserID);
                //var_dump($aUser);
				//var_dump($aExtraUserInfoType);
				
				$this->set("label", array(
								'first_name' => array('Имя', 1),
								'last_name' => array('Фамилия', 1),
								'country_name' => array('Страна', 0),
								'zip_code' => array('Индекс', 0),
								'city' => array('Город', 0),
								'address' => array('Домашний адрес', 0),
								'trading_account' => array('Торговый счет', 1),
								'agent_account' => array('Агентский счет', 1),
								'partner_id' => array('Партнерский номер', 1),
                                'pay_details' => array('Реквизиты оплаты USD Webmoney', 1),
                                'pay_details1' => array('Реквизиты оплаты EUR Webmoney', 1),
                                'pay_details2' => array('Реквизиты оплаты USD Perfect Money', 1),
                                'pay_details3' => array('Реквизиты оплаты EUR Perfect Money', 1),
                                'pay_details4' => array('Реквизиты оплаты USD OKPAY', 1),
                                'pay_details5' => array('Реквизиты оплаты USD Валютный банковский перевод (Invoice)', 1),
                                'pay_details5_1' => array('Название банка', 1),
                                'pay_details5_2' => array('IBAN', 1),
                                'pay_details5_3' => array('Адрес банка', 1),
                                'pay_details5_4' => array('SWIFT', 1),
                                'pay_details6' => array('Реквизиты оплаты USD Moneybookers', 1),
                                'pay_details7' => array('Реквизиты оплаты USD PAYEER', 1),
                                'pay_details8' => array('Реквизиты оплаты USD QIWI', 1)
                                ));
				$this->set("label_r", array(
								'phone1' => array('Тел. Основной', 0),
								'phone2' => array('Тел. Мобильный', 0),
								'phone3' => array('Тел. Рабочий', 0),
								'fax' => array('Факс', 0),
								'nickname' => array('Skype', 0),
								'skypename' => array('Псевдоним', 0),
								'parent_full_name' => array('Спонсор', 1),
								'status_name' => array('Статус', 1),
								));
				$this->set("ajaxUser", $aUser);
				$this->UserInfoTyp->addUserInfoTypeInformation($aExtraUserInfoType);
				$this->set("aUserInfoTyps", $this->UserInfoTyp->getUserInfoTyp());
			}
		}
		else
		{
			
		}
		$this->layout = "ajax";
		
	}
	
	public function userlisting_active() {
		
		$aAllUsers = $this->User->getAllActivatedUsers();
		if($aAllUsers != null && is_array($aAllUsers))
		{
			//$aUser = $aAllUsers[0];
			//var_dump($aAllUsers);
			for($i = 0; $i < count($aAllUsers); $i++)
			{
				$aAllUsers[$i] = $this->UserStatusInfo->calculateUserStatus($aAllUsers[$i]);
			}
		}
        /*Передаем все статусы в view*/
        $this->set('statuses', $this->UserStatus->getListOfStatus());
    	$this->set("aAllUsers", $aAllUsers);
	}
	
	public function userlisting_notactive () {
		
		$aAllUsers = $this->User->getAllNotActivatedUsers();
		if($aAllUsers != null && is_array($aAllUsers))
		{
			//$aUser = $aAllUsers[0];
			//var_dump($aAllUsers);
			for($i = 0; $i < count($aAllUsers); $i++)
			{
				$aAllUsers[$i] = $this->UserStatusInfo->calculateUserStatus($aAllUsers[$i]);
			}
		}
		
    	$this->set("aAllUsers", $aAllUsers);
	}

    public function change_status($requestId) {
        if(($this->request->is("ajax") && $this->request->is("post")))
        {
            $status_id = $this->request->data['value'];
            if ($status_id) {
                $this->User->updateStatus($requestId, $status_id);
                echo json_encode(array('success' => 1));
                return;
            } else {
                die('some error!');
            }
        }
    }
}
