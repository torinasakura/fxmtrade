<div id="tabs" style="width: 100%;">
	<ul>
		<li><a href="#tabs-1">Главный БЦ</a></li>
		<li><a href="#tabs-2">Накопительный БЦ</a></li>
    </ul>
    
	<div id="tabs-1">
		<div class="fxr-table-header" style="margin-right:10px">
        <div style="margin-bottom:15px; float:left; top: 5px;">
			<form name="bonus_view" method="post" action="/bonus/edit">
				<select name="member" onchange="this.form.submit();" class="input_large" >
					<option value="">Выберете участника</option>
                    <?php echo $data ?>
				</select>
			</form>
		</div>
        <div style="float: right; margin-right: 7px;">
        	 <?php if($fulluserinfo['access'] == true)
			{?>
			<a href="/bonus/edit?tabelId"><span prefix="edit" id="edit_button" class="fxr-button save_button">Сохранить</span></a>
			<?php
			}?>
        </div>
        </div>
        <div style="clear:left;"></div>
			<div class="yui3-g" id="Level_1">                
				<?php echo $this->Form->create('EditBonus'); ?>
				<?php $i = 0; foreach($tableInfo['lavel_1'] as $value)
                {                    
					//if ($user_count) { echo '<span id="' .$a[$i]['user_id']. '" class="fx-countbox">' .$user_count. '</span>'; }
					$class_off = ($value['user_id']) ? '' : ' disabled';
					$class_aktiv = ($aktivUserId == $value['user_id']) ? ' aktiv' : '';
					$span_count = ($value['memberCont']) ? '<span id="' .$value['user_id']. '" class="fx-countbox">' .$value['memberCont']. '</span>' : '';
					echo
					'<div class="yui3-u-1-8">
                    	<div id="' .$value['user_id']. '" class="bonus-member-box' .$class_off.$class_aktiv. '">
                    		<span class="ui-icon ui-icon-person"></span>1.' .++$i. '
                    		<span>' .$value['dispaly_name']. '</span>
							<span>' .$value['first_name']. '</span>' .$span_count. '                   
                    	</div>
						<input type="hidden" value="' .$value['user_id']. '" name="user_id[]">
					</div>';
                }?>
            </div>
			<div id="Level_2" class="yui3-g" style="margin-top: 15px;">           
				<?php $i = 0; foreach($tableInfo['lavel_2'] as $value)
                {                    
					//if ($user_count) { echo '<span id="' .$a[$i]['user_id']. '" class="fx-countbox">' .$user_count. '</span>'; }
					$class_off = ($value['user_id']) ? '' : ' disabled';
					$class_aktiv = ($aktivUserId == $value['user_id']) ? ' aktiv' : '';
					$span_count = ($value['memberCont']) ? '<span id="' .$value['user_id']. '" class="fx-countbox">' .$value['memberCont']. '</span>' : '';
					echo
					'<div class="yui3-u-1-4">
                    	<div id="' .$value['user_id']. '" class="bonus-member-box' .$class_off.$class_aktiv. '">
                    		<span class="ui-icon ui-icon-person"></span>1.' .++$i. '
                    		<span>' .$value['dispaly_name']. '</span>
							<span>' .$value['first_name']. '</span>' .$span_count. '                   
                    	</div>
					</div>';
                }?>
            </div>
			<div id="Level_3" class="yui3-g" style="margin-top: 15px;">
            	<?php $i = 0; foreach($tableInfo['lavel_3'] as $value)
                {                    
					$class_off = ($value['user_id']) ? '' : ' disabled';
					$class_aktiv = ($aktivUserId == $value['user_id']) ? ' aktiv' : '';
					$span_count = ($value['memberCont']) ? '<span id="' .$value['user_id']. '" class="fx-countbox">' .$value['memberCont']. '</span>' : '';
					echo
					'<div class="yui3-u-1-2">
                    	<div id="' .$value['user_id']. '" class="bonus-member-box' .$class_off.$class_aktiv. '">
                    		<span class="ui-icon ui-icon-person"></span>1.' .++$i. '
                    		<span>' .$value['dispaly_name']. '</span>
							<span>' .$value['first_name']. '</span>' .$span_count. '                 
                    	</div>
					</div>';
                }?>
            </div>
			<div id="Level_4" class="yui3-g" style="margin-top: 15px;">
				<?php $i = 0; foreach($tableInfo['lavel_4'] as $value)
                {                    
					$class_off = ($value['user_id']) ? '' : ' disabled';
					$class_aktiv = ($aktivUserId == $value['user_id']) ? ' aktiv' : '';
					$span_count = ($value['memberCont']) ? '<span id="' .$value['user_id']. '" class="fx-countbox">' .$value['memberCont']. '</span>' : '';
					echo
					'<div class="yui3-u-1">
                    	<div id="' .$value['user_id']. '" class="bonus-member-box' .$class_off.$class_aktiv. '">
                    		<span class="ui-icon ui-icon-person"></span>1.' .++$i. '
                    		<span>' .$value['dispaly_name']. '</span>
							<span>' .$value['first_name']. '</span>' .$span_count. '                 
                    	</div>
					</div>';
                }?>
			</div>
    </div>
</div>
<script>
	$(function() {
		$( "#EditBonusEditForm" ).sortable({
			axis: "x",
		});
		$( "#EditBonusEditForm" ).disableSelection();
	});
</script>
