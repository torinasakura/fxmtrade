<div class="container-fluid">
    <div class="row-fluid">
    <div class="span12">
    <div class="tabbable">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#info" data-toggle="tab">
                    Информация
                </a>
            </li>

            <li class="">
                <a href="#ts" data-toggle="tab">
                    Торговый счёт: <?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['user_account'] : "Нет"; ?>
                </a>
            </li>

            <li class="">
                <a href="#prs" data-toggle="tab">
                    Партнерский счёт: <?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['user_account'] : "Нет"; ?>
                </a>
            </li>

            <li class="">
                <a href="#rs" data-toggle="tab">
                    Расчётный счёт:  <?php echo $this->Session->read('UserInfo.partner_id') ?>
                </a>
            </li>


        </ul>

        <div class="tab-content">
            <div class="tab-pane" id="info">
                <table class="table table-striped table-bordered box">
                    <tbody>
                    <tr>
                        <td> Ваш статус:</td>
                        <td><strong><?php echo $fulluserinfo['status_name'] ?></strong></td>
                    </tr>
                    <tr>
                        <td>Всего участников:</td>
                        <td><strong><?php echo $allusersofcurrent;/*$allusercount*/ ?></strong></td>
                    </tr>
                    <tr>
                        <td>Оплатившие:</td>
                        <td><strong><?php echo $allusersofcurrentpayed;/*$membercount*/?></strong></td>
                    </tr>
                    <tr>
                        <td>Не оплатившие:</td>
                        <td><strong><?php echo $allusersofcurrentunpayed;/*$allunpaidusercount*/?></strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab-pane" id="ts">
                <table class="table table-striped table-bordered box">
                    <tbody>
                    <tr>
                        <td>Дата открытия счета:</td>
                        <td>
                            <strong><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['reg_date'] : "Нет"; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Депозит:</td>
                        <td>
                            <strong><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['deposit'] . " USD" : "Нет"; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Снятие:</td>
                        <td>
                            <strong><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['withdrawal'] . " USD" : "Нет"; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Баланс:</td>
                        <td>
                            <strong><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['profit/loss'] . " USD" : "Нет"; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <td>
                            <strong><?php echo $fulluserinfodemo['reg_date'] ? $fulluserinfodemo['profit'] . " USD" : "Нет"; ?></strong>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <div class="tab-pane active" id="prs">
                <table class="table table-striped table-bordered box">
                    <tbody>
                    <tr>
                        <td> Дата открытия счета:</td>
                        <td>
                            <strong><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['reg_date'] : "Нет"; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Депозит:</td>
                        <td>
                            <strong><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['deposit'] . " USD" : "Нет"; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Снятие:</td>
                        <td>
                            <strong><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['withdrawal'] . " USD" : "Нет"; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Баланс:</td>
                        <td>
                            <strong><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['profit/loss'] . " USD" : "Нет"; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <td>
                            <strong><?php echo $fulluserinfoagent['reg_date'] ? $fulluserinfoagent['profit'] . " USD" : "Нет"; ?></strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="tab-pane" id="rs">
                <table class="table table-striped table-bordered box">
                    <tbody>
                    <tr>
                        <td>Дата открытия счета:</td>
                        <td><strong><?php echo $regdatedep; ?></strong></td>
                    </tr>
                    <tr>
                        <td>Снятие:</td>
                        <td><strong><?php echo $depwithdrawal; /*$refaccaunt['withdrawal'];*/ ?> USD</strong></td>
                    </tr>
                    <tr>
                        <td>Баланс:</td>
                        <td><strong><?php echo $depbalance/*$refaccaunt['deposit'];*/ ?> USD</strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>






    </div>







    <div class="row-fluid">
        <div class="widget-box">
            <div class="widget-header">
                <h4>Ваша партнерская ссылка: </h4>

                <span class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-up"></i>
                    </a>
                </span>
            </div>
            <div class="widget-body">
                <div class="widget-body-inner" style="display: block;">
                    <div class="widget-main">
                        <div class="row-fluid">
                            <div class="control-group success">
                                <div class="controls">
                                <span class="span6 input-icon input-icon-right">
                                        <?php echo $partnerLink ? "<input id='inputSuccess' class='ace-tooltip span12' data-placement='bottom' type='text' value='$partnerLink' readonly='readonly' data-original-title='Hello Tooltip!'>" : "Этот раздел будет доступен после оплаты торгового места"; ?>
                                    <i class="icon-ok-sign"></i>
                                </span>
                                <span class="span6 input-icon input-icon-right">
                                        <?php echo $partnerLinkr ? "<input id='inputSuccess' class='ace-tooltip span12' data-placement='bottom' type='text' value='$partnerLinkr' readonly='readonly' data-original-title='Hello Tooltip!'>" : "Этот раздел будет доступен после оплаты торгового места"; ?>
                                        <i class="icon-ok-sign"></i>
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
