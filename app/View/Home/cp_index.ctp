<div class="container-fluid">
<div class="row-fluid">
    <div class="span4">
        <div class="box">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Бонусный счёт</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Депозит:</td>
                    <td><strong><?php echo $accaunts['bonus']['deposit']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Снятие:</td>
                    <td><strong><?php echo $accaunts['bonus']['withdrawal']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Баланс:</td>
                    <td><strong><?php echo $accaunts['bonus']['balance']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Прибыль:</td>
                    <td><strong>0.00 USD</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="span4">
        <div class="box">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Поступления</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Депозит:</td>
                    <td><strong><?php echo $accaunts['profit']['deposit']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Снятие:</td>
                    <td><strong><?php echo $accaunts['profit']['withdrawal']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Баланс:</td>
                    <td><strong><?php echo $accaunts['profit']['balance']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Прибыль:</td>
                    <td><strong><?php echo $accaunts['profit']['profit']; ?> USD</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="span4">
        <div class="box">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Резервный фонд</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Депозит:</td>
                    <td><strong><?php echo $accaunts['reserve']['deposit']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Снятие:</td>
                    <td><strong><?php echo $accaunts['reserve']['withdrawal']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Баланс:</td>
                    <td><strong><?php echo $accaunts['reserve']['balance']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Прибыль:</td>
                    <td><strong><?php echo $accaunts['reserve']['profit']; ?> USD</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
        <div class="box">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Бонусы</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Депозит:</td>
                    <td><strong><?php echo $refaccaunt['deposit']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Снятие:</td>
                    <td><strong><?php echo $refaccaunt['withdrawal']; ?> USD</strong></td>
                </tr>
                <tr>
                    <td>Баланс:</td>
                    <td><strong><a
                            href="/?page_id=12&amp;mode=UserStatement"><?php echo $refaccaunt['deposit']; ?>
                        USD</a></strong></td>
                </tr>
                <tr>
                    <td>Прибыль:</td>
                    <td><strong><?php echo $refaccaunt['profit']; ?> USD</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="span4">
        <div class="box">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Регистрация</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Сегодня:</td>
                    <td><strong><?php echo $today; ?></strong></td>
                </tr>
                <tr>
                    <td>Неделя:</td>
                    <td><strong><?php echo $week; ?></strong></td>
                </tr>
                <tr>
                    <td>Месяц:</td>
                    <td><strong><?php echo $month; ?></strong></td>
                </tr>
                <tr>
                    <td>Крартал:</td>
                    <td><strong><?php echo $quarter; ?></strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="span4">
        <div class="box">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Заявки</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Оплата:</td>
                    <td><strong><?php echo $payInvoices; ?></strong></td>
                </tr>
                <tr>
                    <td>Пополнение:</td>
                    <td><strong><?php echo $inInvoices; ?></strong></td>
                </tr>
                <tr>
                    <td>Перевод:</td>
                    <td><strong><?php echo $internInvoices; ?></strong></td>
                </tr>
                <tr>
                    <td>Снятие:</td>
                    <td><strong><?php echo $outInvoices; ?></strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span4 center">
        <div class="box">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th colspan="2">Пользователи</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Всего участников:</td>
                    <td><strong><?php echo $allusercount ?></strong></td>
                </tr>
                <tr>
                    <td>Оплатившие:</td>
                    <td><strong><?php echo $membercount;?></strong></td>
                </tr>
                <tr>
                    <td>Не оплатившие:</td>
                    <td><strong><?php echo $allunpaidusercount;?></strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>