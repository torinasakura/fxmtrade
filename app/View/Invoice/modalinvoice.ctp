<div class="fieldwrapper" style="width: 300px;">
    <label class="styled">Номер заявки:</label>
		<div class="thefield">
			<?php echo $aInvoice["fr_invoice_registered"]["id"]; ?>
		</div>
</div>
<div class="fieldwrapper" style="width: 300px;">
    <label class="styled">Имя:</label>
		<div class="thefield">
			<?php echo $aInvoice["fr_user"]["last_name"].' '.$aInvoice["fr_user"]["first_name"]; ?>
    </div>
</div>
<div class="fieldwrapper" style="width: 300px;">
    <label class="styled">Сумма <?php echo $aInvoice["fr_payment"]["currency_code"]; ?>:</label>
		<div class="thefield" style="margin: 3px 0 0 0;">
			<input  style="width: 70px;" type="text" name="summ" id="" value="<?php echo $aInvoice["fr_invoice_registered"]["sum"]; ?>" />
    </div>
</div>
<div class="fieldwrapper" style="width: 300px;">
    <label class="styled">Партнерский счёт:</label>
    <div class="thefield">
        <?php echo (isset($fulluserinfoagent['user_account']) && $fulluserinfoagent['user_account']  ? $fulluserinfoagent['user_account'] : "Нет"); ?>
    </div>
</div>
<div class="fieldwrapper" style="width: 300px;">
    <label class="styled">Торговый счёт:</label>
    <div class="thefield">
        <?php echo (isset($fulluserinfodemo['user_account']) && $fulluserinfodemo['user_account'] ? $fulluserinfodemo['user_account'] : "Нет"); ?>
    </div>
</div>
