﻿<nav id="secondary" class="main-nav">
  <div class="profile-menu">
    <div class="pull-left">
      <div class="avatar">
        <img src="../../images/avatar.png" />
      </div>
    </div>
    <div class="pull-left">
      <div class="title">
          <?php echo $this->Session->read('UserInfo.last_name').' </br>'.$this->Session->read('UserInfo.first_name'); ?>
      </div>
<!--       <div class="title">
        Имя: <?php echo $this->Session->read('UserInfo.last_name').' '.$this->Session->read('UserInfo.first_name'); ?>
      </div> -->
      
      
      <!--<div class="btn-group">-->
        <!--<button class="button mini inset black icon-home"></button>-->
     <!--&lt;!&ndash;    <button class="button mini inset black">Projects</button> &ndash;&gt;-->
        <!--<button class="button mini inset black icon-off"><a href="/login/logout"></a></button>-->
      <!--</div>-->
    </div>

    <div class="pull-right profile-menu-nav-collapse">
      <button class="button black"><i class="icon-reorder"></i></button>
    </div>

  </div>

  <ul class="secondary-nav-menu">
    <?php 
      if(isset($navigationbuild_s))
      {
        echo $navigationbuild_s;
      }
      else
      { /*log*/ }
    ?>
  </ul>
  

</nav>