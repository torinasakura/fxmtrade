<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
  /**
   * Build the Navigation 
  */
	public $uses = array('UserInfo', 'UserTree', 'Account', 'User');
	public $components = array('Util', 'UserStatusInfo', 'UserInfoTyp', 'Session');
  
	public function beforeFilter()
	{		
		if(defined("ENVIRONMENT"))
			$this->layout = ENVIRONMENT;
		/*if (ENVIRONMENT == 'cp' && $this->Session->read('UserInfo.id') != 131 && $this->Session->read('UserInfo.id')) {
            die('Access denied!');
        }*/
		//check if user is valid
	    if($this->Session->check('isLogin') || $this->params['controller'] == "login" || $this->params['controller'] == "formbuilder" )
		{
			define("USER_ID", $this->Session->read('UserInfo.id'));

			if(USER_ID)
			{
				//Get Full User Info and Setings
				$aFullUserInfo = $this->UserInfo->getUserFullInfo(USER_ID);
                $this->set("fulluserinfo", $aFullUserInfo);
				define("PARTNER_ID", $aFullUserInfo['partner_id']);
				if($aFullUserInfo['userParent_id'])
				{
					//Get Full Parent Info and Setings From User
					$aFullParentInfo = $this->UserInfo->getUserFullInfo($aFullUserInfo['userParent_id']);
					$this->set("parentinfo", $aFullParentInfo);
				}
				else
				{
					$this->set("parentinfo", null);
				}
			}

            //Build the Navigation
            $this->Navigation = $this->Components->load("Navigation");
			/*Если торговое место оплачено, выводим меню без пункта для оплаты торгового места*/
            if (ENVIRONMENT == 'pc') {
                if ($this->User->getPartnerLink(USER_ID)) {
                    $sNavigationBuild = $this->Navigation->buildNavigationPayed();
                    $this->set("navigationbuild", $sNavigationBuild);
                } else {
                    $sNavigationBuild = $this->Navigation->buildNavigationNonPay();
                    $this->set("navigationbuild", $sNavigationBuild);
                }
            } else {
                $sNavigationBuild_f = $this->Navigation->buildNavigation();
                $sNavigationBuild_s = $this->Navigation->buildNavigation_s();
                $this->set("navigationbuild_f", $sNavigationBuild_f);
                $this->set("navigationbuild_s", $sNavigationBuild_s);
            }

			
			
			//Get all the status exist
			$this->loadModel("UserStatus");
			//$this->UserStatusInfo = $this->Components->load("UserStatusInfo");
			$userstatus = $this->UserStatus->find('all');
			$this->UserStatusInfo->setUserStatusInfo($userstatus);
			
			//Get all the userinfo exist;
			$this->loadModel("UserInfo");
			//$this->UserInfoTyp = $this->Components->load("UserInfoTyp");
			$usersinfotyps = $this->UserInfo->find('all');
			
			$this->UserInfoTyp->setUserInfoTyp($usersinfotyps);
		
		}
		else
		{
			$this->redirect("/login");
		}
	}
}
