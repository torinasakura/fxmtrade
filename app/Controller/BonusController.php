<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class BonusController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Bonus';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('User', 'UserInfo', 'UserTree', 'Bonus');
	public $components = array('Session');
		
/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	
	public function index()
	{
		$this->set("aktivUserId", null);
		if(($this->request->is("post")) && ($this->request->data['member']))
		{
			$userId = $this->request->data['member'];
			$tableInfo = $this->Bonus->getFullTableInfo($userId);
			$this->set("data", $this->Bonus->getAllBonusMember(USER_ID, $userId));
			$this->set("aktivUserId", $userId);
		} else {
			$this->set("data", $this->Bonus->getAllBonusMember(USER_ID));
			$tableInfo = $this->Bonus->getFullTableInfo(USER_ID);	
		}
		$this->set("tableInfo", $tableInfo);
	}
	public function edit()
	{
		$this->index();
	}
	public function member()
	{
		$this->set("OptionsAllMember", $this->Bonus->getAllBonusMember(164));
	}
	
	public function info()
	{
		$this->set("MainInfo", $this->Bonus->getAllMembersByLavel(4));
	}
}
