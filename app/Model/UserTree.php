<?php
  
class UserTree extends AppModel
{
  public $name = 'UserTree';
  
  public $primaryKey = 'id';
  public $useTable = 'user_tree';
  
  public function getTotalMembersOfUserCount($user_id)
  {
    $iCount = 0;
    $query = "SELECT n.user_id, COUNT( * ) -1 AS LEVEL , ROUND((n.rgt - n.lft -1) /2) AS offspring, n.id AS user_tree_id
                             FROM fr_user_tree AS n, fr_user_tree AS p
                             WHERE n.lft BETWEEN p.lft AND p.rgt AND n.user_id = $user_id";
    $aResults = $this->query($query);
    //CakeLog::write('debug', 'String in Format: ' .$query);
    if($aResults != null && is_array($aResults) && $aResults[0][0]["offspring"] != null)
      $iCount = $aResults[0][0]["offspring"];
    
    return $iCount;
  }
  
  public function getTotalMembersOfUser($user_id)
  {
    
  }
  
  public function addUser($iParentID, $iUserID)
  {
    $bIsUserAdded = false;
		$aResults = $this->query("SELECT rgt FROM fr_user_tree WHERE user_id = $iParentID");
    
    if($aResults != null && is_array($aResults))
    {
      $iRGT = $aResults[0]["fr_user_tree"]["rgt"];
      $this->query("UPDATE fr_user_tree SET rgt=rgt+2 WHERE rgt >= $iRGT ");
      
      if ( $iParentID != '131' )
      {
        $this->query("UPDATE fr_user_tree SET lft=lft+2 WHERE lft > $iRGT");
      }
      $this->query("INSERT INTO fr_user_tree(user_id, lft, rgt) VALUES($iUserID, $iRGT, ".($iRGT + 1).") ");
      
      if($this->getAffectedRows() > 0){
        $bIsUserAdded = true;
      }
    }

    return $bIsUserAdded;
  }
}
?>