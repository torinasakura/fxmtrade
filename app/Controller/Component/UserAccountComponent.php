<?php
/**
 * Created by Andrew Ghostuhin
 * User: T.A
 * Date: 07.03.13
 * Time: 17:48
 */
App::uses('Component', 'Controller');
class UserAccountComponent extends Component
{
    public $name = 'UserAccount';
    public $components = array('MTQ', 'UserInfoTyp', 'Util');
    public $uses = array('User');

    function createpwd ()
    {
        $clpwd = $this->genpwd();
        $pwd = stripslashes($clpwd);
        $pwd = htmlspecialchars($pwd);
        $pwd = trim($pwd);
        $pwd = strrev(md5($pwd))."b3p6f";
        return array($clpwd, $pwd);
    }
    function genpwd ($length=8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    /*@method: getHistoryOfAccount
    *getting history of current account from mt4
    *@params:
    *@author: Svirsky Evgeny
    *@date: 03.04.2013
    */
    function getHistoryOfAccount($details = false, $id = USER_ID) {
        global $rows;
        App::import("Vendor", "wr/class-mtq");
        $userInstance = ClassRegistry::init('User');
        $invoiceInstance = ClassRegistry::init('Invoice');
        $user_info = $this->MTQ->formatMTQAccountFromCakePHP($userInstance->getUserFullInformationByUserID($id), $this->UserInfoTyp->getUserInfoTyp());
        $aUserAccountInfo = $userInstance->getHistoryOfAccount($user_info);

        $myMTQ = new MTQ();
        $aUserAccountInfo['demoAccount'] = $myMTQ->history(false, $user_info, $details);
        $aUserAccountInfo['agentAccount'] = $myMTQ->history(true, $user_info, $details);

        $invoiceInfo = $invoiceInstance->getInvoceHistoryByType('1');
        if (isset($invoiceInfo[0]['fr_invoice_history']['add_date']) && $invoiceInfo[0]['fr_invoice_registered']['status_id'] == 2) {
            $aUserAccountInfo['demoAccount']['reg_date'] = $this->Util->convertDateToFormat($invoiceInfo[0]['fr_invoice_history']['add_date'],"d.m.Y");
            if (is_array($aUserAccountInfo['agentAccount'])) {
                $aUserAccountInfo['agentAccount']['reg_date'] = $this->Util->convertDateToFormat($invoiceInfo[0]['fr_invoice_history']['add_date'],"d.m.Y");
            }
        } else {
            $aUserAccountInfo['demoAccount']['reg_date'] = false;
            $aUserAccountInfo['agentAccount']['reg_date'] = false;
        }
        if (is_array($aUserAccountInfo) && $aUserAccountInfo) {
            return $aUserAccountInfo;
        }
    }


}
?>