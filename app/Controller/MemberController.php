<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class MemberController extends AppController {

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Member';

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('User', 'UserInfo', 'UserTree', 'Bonus', 'Form');
	public $components = array('Session','Util', 'UserAccount');
	
		
/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function index()
	{
		$label = array (
					'tabs1' => array('first_name' => 'Имя','last_name' => 'Фамилия','country_name' => 'Страна','zip_code' => 'Индекс','city' => 'Город','address' => 'Домашний адрес'),
					'tabs2' => array ('phone1' => 'Тел. Основной','tel' => 'Тел. Мобильный','phone3' => 'Тел. Рабочий','fax' => 'Факс','skype' => 'skype', 'nickname' => 'Псевдоним',
                        'pay_details' =>  'Реквизиты оплаты USD Webmoney',
                        'pay_details1' => 'Реквизиты оплаты EUR Webmoney',
                        'pay_details2' => 'Реквизиты оплаты USD Perfect Money',
                        'pay_details3' => 'Реквизиты оплаты EUR Perfect Money',
                        'pay_details4' => 'Реквизиты оплаты USD OKPAY',
                        'pay_details5' => 'Реквизиты оплаты USD Валютный банковский перевод (Invoice)',
                        'pay_details5_1' => 'Название банка',
                        'pay_details5_2' => 'IBAN',
                        'pay_details5_3' => 'Адрес банка',
                        'pay_details5_4' => 'SWIFT',
                        'pay_details6' => 'Реквизиты оплаты USD Moneybookers',
                        'pay_details7' => 'Реквизиты оплаты USD PAYEER',
                        'pay_details8' => 'Реквизиты оплаты USD QIWI',
                    ));
		$this->set("label", $label);
		
		$message = null;
        $countries = $this->UserInfo->getAllCountries();
        $this->set("countries", $countries);
        if($this->request->is("post"))
		{
			if(isset($this->request->data['MemberData']))
			{
				$escapeData = $this->Form->checkDataForm($this->data['MemberData']);
                if(!$escapeData['err'])
				{
					$this->User->updateUserInfo($escapeData);
                    $this->Session->setFlash("Данные успешно обновлены!");
                    $this->redirect('/member') ;
					
				} else {
					$message .= $escapeData['err'];
				}
				$message .= $this->Form->setAvatar($this->data['MemberData']['avatar']);
				if ($message != null)
				{
					$this->Session->setFlash($message);
					$this->redirect('/member');
				}
			}
			elseif (isset($this->request->data['ChangePassword']))
			{
				if($this->Form->checkPasswords($this->request->data['ChangePassword']['password']))
				{
					if($this->request->data['ChangePassword']['passwd'] == $this->request->data['ChangePassword']['psword']) {
						$sValidPassword = $this->Form->setPassword($this->request->data['ChangePassword']['passwd']);
						$this->User->updateUserInfo(array('fr_user' => array ('user_pass' => "'".$sValidPassword."'")));
                        $this->Session->setFlash("Пароль изменен успешно!");
                        $this->redirect('/member') ;
					} else {
                        $this->Session->setFlash("Пароли должны совпадать!");
                        $this->redirect('/member') ;
                    }

				} else {
                    $this->Session->setFlash("Введен неверный пароль!");
                    $this->redirect('/member') ;
				}
			} elseif(isset($this->request->data['verification'])) {
                $destination = __DIR__ . "/../Docs/";
                $error = false;
                foreach($this->request->data['verification'] as $key => $file) {
                    if (is_uploaded_file($file['tmp_name'])) {
                        // upload the file
                        $success = move_uploaded_file($file['tmp_name'], $destination . USER_ID ."-$key-{$file['name']}");
                        if (!$success) {
                            $error = true;
                        }
                    }
                }
                if ($error) {
                    $this->Session->setFlash("Проблема загрузки файла, попробуйте позже!");
                    $this->redirect('/member');
                } else {
                    $this->Session->setFlash("Документы для верификации успешно загружены!");
                    $this->redirect('/member');
                }
            } elseif (isset($this->request->data['MemberAdditionalData'])) {
                $escapeData = $this->Form->checkDataForm($this->data['MemberAdditionalData']);
                if(!$escapeData['err'])
                {
                    $this->User->updateUserInfo($escapeData);
                    $this->Session->setFlash("Данные успешно обновлены!");
                } else {
                    $message .= $escapeData['err'];
                    $this->Session->setFlash($message);
                }
                $this->redirect('/member') ;
            }
		}
	}

    /*@method: agents
    *action to show full agency net
    *@params:
    *@author: Svirsky Evgeny
    *@date: 05.04.2013
    */
    public function agents() {
        $fullInfo = $this->User->getUserFullInformationByUserID(USER_ID);
        $sResult = "<ul class='agent_tree'><li><a href='javascript:void(0)' id='{$fullInfo['fr_user']['id']}'>{$fullInfo['fr_user']['first_name']} {$fullInfo['fr_user']['last_name']}</a>";
        $sResult .= $this->User->getChildsHTMLRecursively($sResult, USER_ID);
        $sResult .= "</li></ul>";
        $this->set('treeHTML', $sResult);
    }

    /*@method: history
    *action to show history of deposit accounts
    *@params:
    *@author: Svirsky Evgeny
    *@date: 05.04.2013
    */
    public function history() {
        $aUserHistoryAccount = $this->UserAccount->getHistoryOfAccount(true);
        if (isset($aUserHistoryAccount['demoAccount']['details']) && isset($aUserHistoryAccount['agentAccount']['details'])) {
            $this->set('historyInfo', $aUserHistoryAccount);
        } else {
            $this->set('historyInfo', false);
            $this->Session->setFlash("К сожалению у вас пока нет ни одного партнёра!");
        }

    }

    /*@method: partners
    *action to show first level partners
    *@params:
    *@author: Svirsky Evgeny
    *@date: 05.04.2013
    */
    public function partners() {
        $aFirstLevelChilds = $this->User->getFirstLevelLowers(USER_ID);
        if ($aFirstLevelChilds) {
            $this->set('partners', $aFirstLevelChilds);
        } else {
            $this->set('partners', false);
            $this->Session->setFlash("К сожалению у вас пока нет ни одного партнёра!");
        }
    }

    public function getuserinfo() {
        $fields = array(
            "first_name", "last_name", "skype", "user_email", "tel"
        );
        $output = array();
        if ($this->request->is('post')) {
            $id = $this->request->data['id'];
            $data = $this->User->getUserFullInformationByUserID($id);
            foreach($data['fr_user'] as $key => $row) {
                if (in_array($key, $fields)) {
                    $output[$key] = (string)$row;
                }
            }
            echo json_encode($output);
            die();
        }
    }

    /*@method: support
    *for menu support help->service
    *@params:
    *@author: Svirsky Evgeny
    *@date: 12.05.2013
    */
    public function support() {

    }

    /*@method: advice
    *for menu help->advice
    *@params:
    *@author: Svirsky Evgeny
    *@date: 12.05.2013
    */
    public function advice() {

}
}
