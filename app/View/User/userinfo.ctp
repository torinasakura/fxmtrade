<div class="yui3-g">
	<?php echo $this->Form->create('EditMemberInfo'); ?>
	<div class="yui3-u-1-2">
		<?php
		    if (isset($label) && is_array($label)) {
                foreach ($label as $key => $value)
                {
                    echo '
                        <div class="fieldwrapper">
                            <label class="styled input_medium">'. $value[0] .':</label>
                            <div class="thefield">
                                <span>
                                    <input name="'.$key.'" class="input_medium" type="text" value="'.$ajaxUser[$key].'" '.(($value[1]) ? 'disabled="disabled"' : '').' />
                                </span>
                            </div>
                        </div>
                    ';
                }
            }
		?>
	</div>
	<div class="yui3-u-1-2">
		<input type="hidden" name="action" value="edit_personal_data" />
		<?php
		    if (isset($label_r) && is_array($label_r)) {
                foreach ($label_r as $key => $value)
                {
                    echo '
                        <div class="fieldwrapper">
                            <label class="styled input_medium">'. $value[0] .':</label>
                            <div class="thefield">
                                <span>
                                    <input name="'.$key.'" class="input_medium" type="text" value="'.$ajaxUser[$key].'" '.(($value[1]) ? 'disabled="disabled"' : '').' />
                                </span>
                            </div>
                        </div>
                    ';
                }
            }
			/*foreach ($aUserInfoTyps as $aUserInfoType)
			{
				echo'
				<div class="fieldwrapper">
					<label class="styled input_medium">'.$aUserInfoType["display_name"].' :</label>
					<div class="thefield">
						<span>
							<input name="'.$aUserInfoType["data_type"].'" class="input_medium" type="text" id="'.$aUserInfoType["id"].'" value="'.(array_key_exists("added_user_value", $aUserInfoType) ? $aUserInfoType["added_user_value"] : '' ).'"  />
							<img src="/img/css/wp-image/ajax-loader.gif" id="loading" alt="loading"  style="display:none; margin-left: 10px; margin-top: 5px; border: solid 1px #999;" />
						</span>
					</div>
				</div>
				';
			}*/
		?>
    <?php echo $this->Form->end('Сохранить'); ?>
	</div>
</div>