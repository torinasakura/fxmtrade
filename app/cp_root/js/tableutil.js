function check_one_row_selected(sSelector)
{
	var is_checked = $(sSelector).find('input:checked').length > 0 ? true : false;
	return is_checked;
}
function get_selected_rows(sSelector)
{
	var selected_rows = $(sSelector).find('input:checked');
	return selected_rows;
}

function toggleCheckBoxes(sSelector)
{
	var is_checked = $("#all_checkbox").is(':checked');
	$(sSelector).each(function(){
		this.checked=is_checked;
	});
}
/**
 *Error Output for not selecting a single row 
 */
function errorOutput()
{
	alert("Отметьте, пожалуйста номер счета.");
}