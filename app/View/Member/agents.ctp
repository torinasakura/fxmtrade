<?php echo $treeHTML; ?>

<div id="dialog-message" title="Подробная информация">
    <p>
        <b>Имя: </b><span key="first_name"></span>
    </p>
    <p>
        <b>Фамилия: </b><span key="last_name"></span>
    </p>
    <p>
        <b>Телефон: </b><span key="tel"></span>
    </p>
    <p>
        <b>Email: </b><span key="user_email"></span>
    </p>
    <p>
        <b>Skype: </b><span key="skype"></span>
    </p>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $( "#dialog-message" ).dialog({
            modal: true,
            buttons: {
                Закрыть: function() {
                    $( this ).dialog( "close" );
                }
            },
            height: 280,
            autoOpen: false
        });


        $(".agent_tree a").click(function() {
            var id = $(this).attr("id");
            if (id) {
                var data = {};
                data['id'] = id;
                $.ajax({
                    type : "post",
                    url : "/member/getuserinfo",
                    data : data
                })
                        .success(function(callback) {
                            var key;
                            var userdata;
                            userdata = JSON.parse(callback);
                            $("#dialog-message p span").each(function(e) {
                                key = $(this).attr("key");
                                $(this).text(userdata[key]);
                            });
                            $( "#dialog-message").dialog( "open" );
                            var closeDialog = setTimeout(function() {
                                $( "#dialog-message").dialog( "close" );
                            }, 5000);
                            $('.ui-button').click(function(){
                                clearTimeout(closeDialog);
                            })
                        });
            } else {
                console.log("id not found!");
            }
        });
    });
</script>
