<?php

  class UserInfoTypComponent extends Component
  {
    private $aUserInfoTyp = null;
    
    
    /**
    *Sets the UserInfoTyps loaded from the database
    */
    public function setUserInfoTyp($aUserInfoTyps)
    {
      if($aUserInfoTyps != null && count($aUserInfoTyps) > 0 )
      {
        $this->aUserInfoTyp = array();
        foreach($aUserInfoTyps as $aUserInfoType)
        {
          $this->aUserInfoTyp[$aUserInfoType["UserInfo"]["id"]] = $aUserInfoType["UserInfo"];
        }
        /** Information of the Global UserInfoType
         *
            1 => 
              array
                'id' => string '1' (length=1)
                'data_type' => string 'Phone_1' (length=7)
                'display_name' => string 'Тел. Основной' (length=24)
            2 => 
              array
                'id' => string '2' (length=1)
                'data_type' => string 'Phone_2' (length=7)
                'display_name' => string 'Тел. Мобильный' (length=26)
            3 => 
              array
                'id' => string '3' (length=1)
                'data_type' => string 'Phone_3' (length=7)
                'display_name' => string 'Тел. Рабочий' (length=22)
         *
         */
      }
    }
    
    public function getUserInfoTyp()
    {
      return $this->aUserInfoTyp;
    }
    
    public function getUserInfoTypByID($iID)
    {
      $aReturnUserInfoTyp = null;
      
      if($this->aUserInfoTyp != null && is_array($this->aUserInfoTyp))
        $aReturnUserInfoTyp = $this->aUserInfoTyp[$iID];
        
      return $aReturnUserInfoTyp;
    }
    
    public function getUserInfoTypByData_Type($sDataType)
    {
      $aReturnUserInfoTyp = null;
      if(!empty($sDataType))
      {
        foreach($this->aUserInfoTyp as $aUserInfoTyp)
        {
          if($aUserInfoTyp["data_type"] == $sDataType)
          {
            $aReturnUserInfoTyp = $aUserInfoTyp;
          }
        }
      }
      return $aReturnUserInfoTyp;
    }
    
    public function addUserInfoTypeInformation($aUserInfoTypes)
    {
      if($aUserInfoTypes != null && is_array($aUserInfoTypes))
      {
        foreach($aUserInfoTypes as $aUserInfoType)
        {
          $this->aUserInfoTyp[$aUserInfoType["fr_user_info"]["id"]]["added_user_value"] = $aUserInfoType["fr_user_info"]["value"];
        }
      }
    }
    
}
?>