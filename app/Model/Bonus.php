<?php
  
class Bonus extends AppModel
{
	public $name = 'Bonus';
	public $primaryKey = 'id';
	public $useTable = 'bonus_tables';
	
	function isUserAccess()
	{
		$query = "Select access From fr_user Where fr_user.id = " .USER_ID;
		$sql = $this->query($query);
		return $sql[0]['fr_user']['access'];
	}
		
	public function getAllBonusMember($iUserID, $iSelUserId = USER_ID)
	{
		
		if($this->isUserAccess())
			$iUserID = 131;
		$data = false;
		$query = 'SELECT o.user_id as user_id, fr_user.last_name, fr_user.first_name, COUNT(p.id)-1 AS level
					FROM fr_user_tree AS n, fr_user_tree AS p, fr_user_tree AS o, fr_user
					WHERE o.lft BETWEEN p.lft AND p.rgt
					AND o.lft BETWEEN n.lft AND n.rgt
					AND n.user_id = '.$iUserID.'
					AND o.user_id = fr_user.id
					GROUP BY o.lft
					ORDER BY o.lft';
		$sql = $this->query($query);
		usort($sql, array("Bonus", "compareUser"));
		//var_dump($sql);
		// { [0]=> array(3) { ["o"]=> array(1) { ["user_id"]=> string(3) "164" } ["fr_user"]=> array(2) { ["last_name"]=> string(11) "Negorozhina" ["first_name"]=> string(5) "Oxana" } 
		foreach($sql as $myrow)
		{
			if($myrow['o']['user_id'] == $iSelUserId)
				$selected = 'selected="selected"';
			else
				$selected = null;
			$data .= '<option value="' .$myrow['o']['user_id']. '" ' .$selected. '>' .$myrow['fr_user']['last_name']. ' ' .$myrow['fr_user']['first_name']. '</option>';
			
		}
		return $data;
	}
	
	function compareUser($oUserObject1, $oUserObject2)
	{
		if($oUserObject1['fr_user']['last_name'] == $oUserObject2['fr_user']['last_name']) 
			return 0;
		elseif($oUserObject1['fr_user']['last_name'] < $oUserObject2['fr_user']['last_name']) 
			return -1;
		else 
			return 1;
	}
	
	public function getFullTableInfo($iUserID)
	{
		$lavel_1 = $lavel_2 = $lavel_3 = $lavel_4 = array();
		$dumy_array = array('user_id' => null, 'last_name' => null, 'first_name' => null, 'dispaly_name' =>  null, 'memberCont' => null);
		if(!$this->userInStructurValidCheck(USER_ID, $iUserID))
		{
			$lavel_1 = array_pad ($lavel_1, 8, $dumy_array);
			$lavel_2 = array_pad ($lavel_2, 4, $dumy_array);
			$lavel_3 = array_pad ($lavel_3, 2, $dumy_array);
			$lavel_4 = array_pad ($lavel_4, 1, $dumy_array);
			return $data = array('lavel_1' => $lavel_1, 'lavel_2' => $lavel_2, 'lavel_3' => $lavel_3, 'lavel_4' => $lavel_4);;
		}
		if(!$iTableId = $this->getUserPosition($iUserID))
		{
			$lavel_1 = array_pad ($lavel_1, 8, $dumy_array);
			$lavel_2 = array_pad ($lavel_2, 4, $dumy_array);
			$lavel_3 = array_pad ($lavel_3, 2, $dumy_array);
			$lavel_4 = array_pad ($lavel_4, 1, $dumy_array);
			return $data = array('lavel_1' => $lavel_1, 'lavel_2' => $lavel_2, 'lavel_3' => $lavel_3, 'lavel_4' => $lavel_4);;
		}
		$lavel_1 = $lavel_2 = $lavel_3 = $lavel_4 = array();
		$query = "	Select fr_bonus_tables.user_id, fr_bonus_tables.lavel, fr_bonus_tables.series, fr_bonus_tables.typ,
			  			fr_user.last_name, fr_user.first_name
					From fr_bonus_tables Inner Join
			  			fr_user On fr_bonus_tables.user_id = fr_user.id
					Where fr_bonus_tables.table_id = ".$iTableId;
		$sql = $this->query($query);
		//var_dump($sql);
		
		foreach ($sql as $myrow)
		{
			$memberCont = $this->getMembersCount($myrow['fr_bonus_tables']['user_id']);
			$dispaly_name =  $myrow['fr_user']['last_name'][0].'...'.substr($myrow['fr_user']['last_name'], -1);
			if ($myrow['fr_bonus_tables']['lavel'] == 1)
			{
				$lavel_1[] = array(
								'user_id' => $myrow['fr_bonus_tables']['user_id'],
								'last_name' => $myrow['fr_user']['last_name'],
								'first_name' => $myrow['fr_user']['first_name'],
								'dispaly_name' =>  $dispaly_name,
								'memberCont' => $memberCont);
			}
			elseif ($myrow['fr_bonus_tables']['lavel'] == 2)
			{
				$lavel_2[] = array(
								'user_id' => $myrow['fr_bonus_tables']['user_id'],
								'last_name' => $myrow['fr_user']['last_name'],
								'first_name' => $myrow['fr_user']['first_name'],
								'dispaly_name' =>  $dispaly_name,
								'memberCont' => $memberCont);
			}
			elseif ($myrow['fr_bonus_tables']['lavel'] == 3)
			{
				$lavel_3[] = array(
								'user_id' => $myrow['fr_bonus_tables']['user_id'],
								'last_name' => $myrow['fr_user']['last_name'],
								'first_name' => $myrow['fr_user']['first_name'],
								'dispaly_name' =>  $dispaly_name,
								'memberCont' => $memberCont);
			}
			elseif ($myrow['fr_bonus_tables']['lavel'] == 4)
			{
				$lavel_4[] = array(
								'user_id' => $myrow['fr_bonus_tables']['user_id'],
								'last_name' => $myrow['fr_user']['last_name'],
								'first_name' => $myrow['fr_user']['first_name'],
								'dispaly_name' =>  $dispaly_name,
								'memberCont' => $memberCont);
			}
		}
		
		$lavel_1 = array_pad ($lavel_1, 8, $dumy_array);
		$lavel_2 = array_pad ($lavel_2, 4, $dumy_array);
		$lavel_3 = array_pad ($lavel_3, 2, $dumy_array);
		$lavel_4 = array_pad ($lavel_4, 1, $dumy_array);
		return $data = array('lavel_1' => $lavel_1, 'lavel_2' => $lavel_2, 'lavel_3' => $lavel_3, 'lavel_4' => $lavel_4);
	}
	
	public function addSeriesUser($aUser)
	{
		$sReturnMessage = null;
		if ($this->getUserPosition( $aUser["fr_user"]["id"]))
		{
			$sReturnMessage = 'участник уже существует в бонусном цикле'; 
		}
		else
		{
		 $iBonusTableID = $this->getUserPosition($aUser["fr_user"]["userParent_id"]);		 
		 if($this->getQuantityOneLavel( $iBonusTableID ) < 7){
			$sReturnMessage = $this->addFirstLevel($aUser["fr_user"]["id"], $iBonusTableID);
		 }
		 else{
		  $sReturnMessage = $this->addFission($aUser["fr_user"]["id"], $iBonusTableID);
		 }
		}
		return $sReturnMessage;
	}
	
	function getMembersCount($UserId)
	{
		if (!$UserId) return;
		$countrow = null;
		$query = "	Select Count(*) as countrow
					From fr_user Inner Join
						fr_invoice_registered On fr_user.id = fr_invoice_registered.user_id Inner Join
						fr_invoice_history On fr_invoice_registered.id = fr_invoice_history.invoice_id
						Inner Join
						fr_bonus_tables On fr_user.userParent_id = fr_bonus_tables.user_id
					Where fr_user.userParent_id = " .$UserId. " And fr_invoice_registered.status_id = 2 And
						fr_invoice_registered.typ = 1 And Date(fr_invoice_history.add_date) Between
						Date(fr_bonus_tables.add_date) And Now() And fr_invoice_history.status_id = 2";
		$sql = $this->query($query);
		if ($sql)
		{
		  	if($sql[0][0]['countrow'] == 0)
				return $countrow;
			else
				return $countrow = $sql[0][0]['countrow'];
			  
		}
		else return $countrow;
	}
	
	function userInStructurValidCheck($myUserID, $myStructurUserID)
	{
		if($this->isUserAccess())
			return true;
		$query = 'SELECT o.user_id
					FROM fr_user_tree AS n, fr_user_tree AS p, fr_user_tree AS o
					WHERE o.lft BETWEEN p.lft AND p.rgt
					AND o.lft BETWEEN n.lft AND n.rgt AND n.user_id = '.$myUserID.' AND o.user_id = '.$myStructurUserID.'
					GROUP BY o.lft
					ORDER BY o.lft';
		$row = $this->query($query);
		return $row != null;
	}
	
	private function addFission($iUserID, $iBonusTableID)
	{
		$this->query("INSERT INTO fr_bonus_tables(user_id, table_id, add_date) VALUES($iUserID, $iBonusTableID, NOW())"); //Добовляем участника на превый уровень в таблицу вышестоящего
		$this->query("UPDATE fr_bonus_tables SET lavel=lavel+1 WHERE table_id = ".$iBonusTableID ); //Поднимаем весех участников данной таблицы на один уровень выше
		  
			
		$query = "Select MAX(table_id) As max_table_id From fr_bonus_tables"; // Выборка послдней таблицы
		$aResult = $this->query($query);
		
		$iCountQuantityTable = $this->getQuantityTable( $iBonusTableID );
		
		if ($iCountQuantityTable == 8 ){
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 2 AND table_id = ' .$iBonusTableID. ' LIMIT 4' );
		}
		else if ( $iCountQuantityTable == 12 ){
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 3 AND table_id = ' .$iBonusTableID. ' LIMIT 2' );
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 2 AND table_id = ' .$iBonusTableID. ' LIMIT 4' );
		}
		else if ( $iCountQuantityTable == 14 ){
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 4 AND table_id = ' .$iBonusTableID. ' LIMIT 1' );
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 3 AND table_id = ' .$iBonusTableID. ' LIMIT 2' );
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 2 AND table_id = ' .$iBonusTableID. ' LIMIT 4' );
		}
		else if ( $iCountQuantityTable == 15 ){
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 4 AND table_id = ' .$iBonusTableID. ' LIMIT 1' );
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 3 AND table_id = ' .$iBonusTableID. ' LIMIT 2' );
			$this->query( 'UPDATE fr_bonus_tables SET table_id = ' .$aResult[0][0]["max_table_id"]. '+1 WHERE lavel = 2 AND table_id = ' .$iBonusTableID. ' LIMIT 4' );
			
			$this->query( 'UPDATE fr_bonus_tables SET table_id = 0,series = series+1,lavel=1,add_date = NOW() WHERE lavel = 5 AND table_id = ' .$iBonusTableID. ' LIMIT 1' );
			
			$query = 'SELECT fr_bonus_tables.user_id FROM fr_bonus_tables WHERE fr_bonus_tables.table_id = 0 AND fr_bonus_tables.lavel = 1';
		  	$aResult = $this->query($query);
			
			if($aResult != null && is_array($aResult))
			{
				$iNewUserId = $aResult[0]["fr_bonus_tables"]["user_id"];
				$this->addSeriesUser($iNewUserId);
				$this->query("INSERT INTO fr_invoice_registered(payments_id,user_id,sum,status_id,typ) VALUES(12,131,1550,1,4)");
			}
		}
		return 'таблица поделена пользователь добавлен';
	}
	  
	  
	  
	  	private function getUserPosition($iUserID)
		{
			$iTableID = null;
		  	$query = 'SELECT fr_bonus_tables.table_id FROM fr_bonus_tables WHERE fr_bonus_tables.table_id != 0 AND fr_bonus_tables.user_id = '.$iUserID;
		  	$aResult = $this->query($query);
			if($aResult != null && is_array($aResult))
			{
				$iTableID = $aResult[0]["fr_bonus_tables"]["table_id"];
			}
		  	return $iTableID;
	  	}
		
	function getQuantityOneLavel($iTableID)
	{
		$query = 'SELECT COUNT(*) As quantity FROM fr_bonus_tables WHERE fr_bonus_tables.lavel = 1 AND fr_bonus_tables.table_id = '.$iTableID;
		$aResult = $this->query($query);
		return $aResult[0][0]["quantity"];
	}
	  
	  private function getQuantityTable($iTableID){
		  $query = 'SELECT COUNT(*) As quantity FROM fr_bonus_tables WHERE fr_bonus_tables.table_id = '.$iTableID;
		  $aResult = $this->query($query);
		  return $aResult[0][0]["quantity"];
	  }
	  
	  
	  
	  private function addFirstLevel($iUserID, $iBonusTableID)
		{
			$this->query("INSERT INTO fr_bonus_tables(user_id, table_id, add_date) VALUES($iUserID, $iBonusTableID, NOW())"); //Добовляем участника на превый уровень в таблицу вышестоящего
		  return 'пользователь добавлен';
	  }
	function getAllMembersByLavel($ilevel)
	{
		$aReturn = null;
		$query = "	Select fr_user.id as user_id, fr_user.last_name, fr_user.first_name, fr_bonus_tables.lavel, fr_bonus_tables.add_date
					From fr_bonus_tables Inner Join
					  fr_user On fr_bonus_tables.user_id = fr_user.id
					Where fr_bonus_tables.lavel = ".$ilevel;
		$aResult = $this->query($query);
		if($aResult != null && is_array($aResult))
		{
			$aReturn = $aResult;
		}
		//var_dump($aReturn);
		return $aReturn;
		
	}
}
?>