<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<?php echo $this->Html->meta("description", "Investoria Plus Controlcenter");?>
	<?php echo $this->Html->meta("keywords", "Forex");?>
	<title>
        Investoria Plus
	</title>
	
	<script type="text/javascript" src="https://www.google.com/jsapi?key=IJ-TE2aYjkIGbHdhHpOjAnT6"></script>
	<script type="text/javascript">
		google.load("jquery", "1");
	  google.load("jqueryui", "1");
	</script>
	
	<script type="text/javascript" src="/js/wp/tbscript.js"></script>
	<script src="/js/wp/jquery.cookie.js" type="text/javascript"></script>
	<script src="/js/wp/jquery.treeview.js" type="text/javascript"></script>
	
	
	<?php
		if(isset($additiona_header_information)){
			echo $additiona_header_information;
		}
	
		echo $this->Html->meta('icon');
		echo $this->Html->css('normalize');
		echo $this->Html->css('style');
		echo $this->Html->css('grid');
		echo $this->Html->css('green');
		echo $this->Html->css('custom-theme/jquery-ui-1.8.19.custom');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.4.1/build/cssgrids/grids-min.css">
		
</head>
<body class="home page page-id-203 page-template page-template-page-home-php">
	<div id="main">
		
		<!-- HEADER -->
		<header id="header">
			<div class="container_12 clearfix">
				<div class="grid_12">
					<div class="logo">
						<h1><a title="Investoria Plus" href="#">Investoria Plus</a></h1>
					</div>
					<div id="widget-header">
						<div class="widget-header" id="my_sloganwidget-2">
							<div class="slogan-box">
								<h2>CONTROL CENTER</h2>
								<div class="slogan-desc">for your business</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- end HEADER -->
		
		<!-- Navigation -->
		<nav class="primary">
			<?php echo $this->element('navigation'); ?>
		</nav>
		<!-- end Navigation -->
		
		<div class="primary_content_wrap">
			<div class="clearfix" id="second_content_area">
				<div class="container_12">
						
					<!--Start sideboard r-->
					<div id="text-2" class="grid_6" style="width: 200px;">
						<div class="widget" id="archives-3">
							<h2>Menu</h2>
						</div>
					</div>
					<!--End sideboard r-->
						
					<!-- start scontent -->
					<div id="text-3" class="grid_6" style="width: 800px;">
						<div class="indent">
							<?php echo $this->fetch('content'); ?>
						</div>
					</div>
					<!-- end content -->
						
					<!--Start sideboard l-->
					<div id="text-4" class="grid_6" style="width: 190px;">
						<div class="indent">
							<div class="textwidget">
								<div style="margin-bottom: 40px;">
									[<a id="hlLogout" style="font-weight:bold;" href="/loguot#">Выход</a>]<br/><br/>
									<b>Добро пожаловать<br /><?php echo "user_info['last_name'].' '.user_info['first_name'] <-- logedin user";?></b>
								</div>
							</div>
						</div>
					</div>
					<!--End sideboard l-->
				</div> 
			</div>
		</div>
		<footer id="footer">
			<div class="container_12 clearfix">
				<div class="indent">
					<div class="wrapper">
						<div class="grid_6">© 2003-<?php echo date("Y")?> Investoria Plus Controlcenter</div><!-- {%FOOTER_LINK} -->
						<div id="widget-footer">
							<div id="social_networks-3"></div>
						</div>
					</div>
				</div>
			</div><!--.container-->
		</footer>
	</div>
	<script type="text/javascript" src="/js/tableutil.js"></script>
	<script type="text/javascript" src="/js/tablescript.js"></script>
	<script type="text/javascript" src="/js/invoicescript.js"></script>
	<script type="text/javascript" src="/js/accountscript.js"></script>
	<script type="text/javascript" src="/js/userscript.js"></script>
</body>
</html>