<?php
  
App::uses('AppModel', 'Model');

class Email extends AppModel
{
  public $name = 'Email';
  
  public $primaryKey = 'id';
  public $useTable = 'mail';
  
  
  public function getMailList()
  {
    $aReturn = null;
    $query = "SELECT fr_mail.*, fr_user_author.last_name, fr_user_author.first_name, fr_user_modified.last_name, fr_user_modified.first_name
              FROM fr_mail
              INNER JOIN fr_user fr_user_author ON fr_mail.post_author = fr_user_author.id
              INNER JOIN fr_user fr_user_modified ON fr_mail.last_modified_by = fr_user_modified.id
              ORDER BY fr_mail.id";
    $aResults = $this->query($query);

    if($aResults != null && is_array($aResults) )
      $aReturn = $aResults;
    
    return $aReturn;
  }
  
  public function getMailInformation($iEmailID)
  {
    $aReturn = null;
    /*$query = "SELECT fr_mail.*, fr_user_author.last_name, fr_user_author.first_name, fr_user_modified.last_name, fr_user_modified.first_name
              FROM fr_mail
              INNER JOIN fr_user fr_user_author ON fr_mail.post_author = fr_user_author.id
              INNER JOIN fr_user fr_user_modified ON fr_mail.last_modified_by = fr_user_modified.id
              WHERE fr_mail.id = $iEmailID ";*/
    $query = "SELECT * FROM fr_mail WHERE id = $iEmailID";
    $aResults = $this->query($query);

    if($aResults != null && is_array($aResults) )
      $aReturn = $aResults[0];
    
    return $aReturn;
  }
  
  public function saveEditMail($iEmailID, $aEmailInformation, $iEditorID)
  {
    $sReturnMessage = null;
    $sReturnMessage = $this->checkEmailContent($aEmailInformation);
    
    $bSaveAllowed = empty($sReturnMessage);
    if($bSaveAllowed)
    {
      $query = "UPDATE fr_mail SET subject = '".$aEmailInformation["subject"]."', body = '".$aEmailInformation["body"]."',
              signature_id = 0, content_type = 'html', title = '".$aEmailInformation["title"]."', last_modified_by = $iEditorID,
              send_type = 'programm_mail' WHERE id = $iEmailID";
      $this->query($query);
    }
    return $sReturnMessage;
  }
  
  public function saveNewMail($aEmailInformation, $iAuthorID)
  {
    $sReturnMessage = null;
    $sReturnMessage = $this->checkEmailContent($aEmailInformation);
    
    $bSaveAllowed = empty($sReturnMessage);
    if($bSaveAllowed)
    {
      $query = "INSERT INTO fr_mail(subject, body, signature_id, content_type, title, post_author,
            create_date, last_modified_by, send_type) VALUES
            ('".$aEmailInformation["subject"]."', '".$aEmailInformation["body"]."', 0, 'html', '".$aEmailInformation["title"]."', $iAuthorID, 
             NOW(), $iAuthorID, 'programm_mail')";
      $this->query($query);
    }
             
    return $sReturnMessage;
  }
  
  private function checkEmailContent($aEmailInformation)
  {
    $sReturnMessage = null;
    
    if(!array_key_exists("subject", $aEmailInformation) || empty($aEmailInformation["subject"])){
      $sReturnMessage = "No subject set";
    }
    if(!array_key_exists("body", $aEmailInformation) || empty($aEmailInformation["body"])){
      $sReturnMessage = "No body set";
    }
    if(!array_key_exists("title", $aEmailInformation) || empty($aEmailInformation["title"])){
      $sReturnMessage = "No title set";
    }
    
    return $sReturnMessage;
  }
}
?>