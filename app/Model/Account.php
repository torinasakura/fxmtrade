<?php
  
  App::uses('AppModel', 'Model');
  
  class Account extends AppModel
  {
    public $name = 'Account';
  
    public $primaryKey = 'id';
    public $useTable = 'statement';
    /*********** EXAMPLE
    public $belongsTo = array(
        'Parent' => array(
            'className' => 'Post',
            'foreignKey' => 'parent_id'
        )
    );

    public $hasMany = array(
        'Children' => array(
            'className' => 'Post',
            'foreignKey' => 'parent_id'
        )
    );
    */
    
    public function GetAllStatementType($type)
    {
      $query = 'SELECT fr_statement.invoice_id, fr_statement.sum, fr_statement.statement_typ, fr_invoice_history.add_date, fr_user.last_name, fr_user.first_name,
              fr_user1.last_name AS operator_last, fr_user1.first_name As operator_first, fr_user_settings.partner_id, fr_user2.last_name AS to_last,
              fr_user2.first_name AS to_first, fr_user_settings1.partner_id AS to_partner_id, fr_invoice_registered.user_id
                FROM fr_statement
                INNER JOIN  fr_invoice_history ON fr_statement.invoice_id = fr_invoice_history.invoice_id
                INNER JOIN  fr_invoice_registered On fr_statement.invoice_id = fr_invoice_registered.id
                INNER JOIN  fr_user On fr_invoice_registered.user_id = fr_user.id
                INNER JOIN  fr_user fr_user1 On fr_invoice_history.user_id = fr_user1.id
                INNER JOIN  fr_user_settings On fr_invoice_registered.user_id = fr_user_settings.user_id
                INNER JOIN  fr_user fr_user2 On fr_statement.user_id = fr_user2.id
                INNER JOIN  fr_user_settings fr_user_settings1 On fr_user2.id = fr_user_settings1.user_id
                WHERE fr_invoice_history.status_id = 2 And fr_statement.statement_typ = '.$type.'
                AND (TO_DAYS(now()) - TO_DAYS(fr_invoice_history.add_date) < 15)
                ORDER BY fr_statement.invoice_id DESC, fr_statement.sum DESC';
      $aResults = $this->query($query);
  
      return $aResults;
    }
    
    public function GetParentInvoiceForPayStructur($iUserID)
    {
      $aReturn = null;
      
      $query = "SELECT p.user_id, fr_user.last_name, fr_user_settings.status_id as user_status_id,
              (SELECT ROUND((n.rgt - n.lft -1) /2) FROM fr_user_tree AS n, fr_user_tree AS p WHERE n.lft BETWEEN p.lft AND p.rgt AND n.user_id = fr_user.id GROUP BY n.user_id) as countpartner
                FROM fr_user_tree n, fr_user_tree p
                INNER JOIN fr_user_settings On p.user_id = fr_user_settings.user_id
                INNER JOIN fr_user ON p.user_id = fr_user.id
                WHERE n.lft Between p.lft AND p.rgt AND n.user_id = $iUserID AND p.user_id != $iUserID";
       $aResults = $this->query($query);
       
      if($aResults != null && is_array($aResults))
        $aReturn = $aResults;
      return $aReturn;
    }
    
    public function insertStatements($iInvoiceID, $fSum, $iUserID)
    {
      $this->query("INSERT INTO fr_statement(invoice_id, statement_typ, sum, user_id) VALUES($iInvoiceID, 1, -$fSum, $iUserID)");
      $this->query("INSERT INTO fr_user_statement(invoice_id, sum, user_id) VALUES($iInvoiceID, $fSum, $iUserID)");
    }
    
    public function insertStatementUser($iInvoiceID, $fSum, $iUserID, $iTyp, $iNote = NULL)
    {
      $this->query("INSERT INTO fr_user_statement(invoice_id, sum, user_id, typ, note) VALUES($iInvoiceID, $fSum, $iUserID, $iTyp, $iNote)");
    }
    
    public function getInfoTransactionUserStatement($iInvoiceID)
    {
      $aReturn = null;
      
      $query = "SELECT fr_user.id, fr_user.last_name, fr_user.first_name, fr_user.sex, fr_invoice_history.add_date, fr_user_statement_typ.name, fr_invoice_registered.sum,
              (SELECT SUM(sum) FROM fr_user_statement WHERE fr_user_statement.user_id = fr_user.id) as full_sum, fr_user_settings.partner_id
                FROM fr_user
                INNER JOIN fr_user_statement ON fr_user_statement.user_id = fr_user.id
                INNER JOIN fr_user_settings ON fr_user_settings.user_id = fr_user.id
                INNER JOIN fr_invoice_registered ON fr_invoice_registered.id = fr_user_statement.invoice_id
                INNER JOIN fr_invoice_history ON fr_invoice_registered.id = fr_invoice_history.invoice_id
                INNER JOIN fr_user_statement_typ ON fr_user_statement.typ = fr_user_statement_typ.id
                WHERE fr_invoice_registered.id = $iInvoiceID AND fr_invoice_history.status_id = 2";
                
      $aResults = $this->query($query);
      
      if($aResults != null && is_array($aResults))
        $aReturn = $aResults;
        
      return $aReturn;
    }
	
	public function getUserStatement()
	{
		$query = 'SELECT sum,typ FROM `fr_user_statement` WHERE user_id = '.USER_ID;
		$sql = $this->query($query);
		$deposit = $profit = $storno = $withdrawal = number_format(0, 2, '.', '');
		foreach ($sql as $myrow){
			$deposit = $this->displayPrice( $deposit + $myrow['fr_user_statement']['sum'] );
			if ($myrow['fr_user_statement']['typ'] == 1 || $myrow['fr_user_statement']['typ'] == 6)
			{
				$profit = $this->displayPrice( $profit + $myrow['fr_user_statement']['sum'] );
			}
			elseif ($myrow['fr_user_statement']['sum'] < 0 && $myrow['fr_user_statement']['typ'] != 2)
			{
				$withdrawal = $this->displayPrice( $withdrawal + $myrow['fr_user_statement']['sum'] );
			}
			elseif ($myrow['fr_user_statement']['typ'] == 2)
			{
				$storno = $this->displayPrice( $storno + $myrow['fr_user_statement']['sum'] );
			}
			$profit = $this->displayPrice( $profit + $storno );
		}
		return array('deposit' => $deposit, 'profit' => $profit, 'withdrawal' => $withdrawal);
	}
	
	public function getStatement()
	{
		$reserve = $this->getStatementByType(3);
		$bonus = $this->getStatementByType(4);
		$profit = $this->getStatementByType(5);
		
		return array('reserve' => $reserve, 'bonus' => $bonus, 'profit' => $profit);
	}
	
	function getStatementByType($iTypeId)
	{
		$deposit = $profit = $storno = $withdrawal = number_format(0, 2, '.', '');
		
		$query = "SELECT sum(SUM) as summe FROM `fr_statement` WHERE statement_typ = $iTypeId";
		$sql = $this->query($query);
			$balance = $this->displayPrice($sql[0][0]['summe']);
		
		$query = "SELECT sum(SUM) as summe FROM `fr_statement` WHERE statement_typ = $iTypeId AND sum < 0";
		$sql = $this->query($query);
			$withdrawal = $this->displayPrice($sql[0][0]['summe']);
			$deposit = $this->displayPrice(($sql[0][0]['summe']/-1)+$balance);
			$profit = $this->displayPrice($balance + ($sql[0][0]['summe']/-1));
		
		$result = array('balance' => $balance, 'profit' => $profit, 'deposit' => $deposit, 'withdrawal' => $withdrawal);
		return $result;
	}
	
	public function displayPrice($value)
	{
		$value = number_format($value, 2, '.', '');
		if ($value < 0)
		{
			$value = '<span style="color:#C00">'. $value .'</span>';
		}
		return $value;
	}


    public function getDepositBalance() {
        $query = "SELECT sum FROM `fr_user_statement` WHERE typ='3' AND user_id='" . USER_ID . "' AND note IN ('6', '7')";
        $aResult = $this->query($query);
        $sum = 0;
        foreach ($aResult as $value) {
            $sum += $value['fr_user_statement']['sum'];
        }
        if ($sum) {
            return $sum;
        } else {
            return 0;
        }
    }

    /*@method: getDepositWithdrawal
    *Getting sum of deposit for withdrawal
    *@params:
    *@author: Svirsky Evgeny
    *@date: 03.04.2013
    */
    public function getDepositWithdrawal() {
        $query = "SELECT sum FROM `fr_user_statement` WHERE typ='3' AND user_id='" . USER_ID . "' AND note='8'";
        $aResult = $this->query($query);
        $sum = 0;
        foreach ($aResult as $value) {
            $sum += abs($value['fr_user_statement']['sum']);
        }
        if ($sum) {
            return $sum;
        } else {
            return 0;
        }
    }
}
  
	
  
  



















  
  
  
  
  
?>