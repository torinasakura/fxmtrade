<?php
  
  App::uses('AppModel', 'Model');
  
  class UserAccess extends AppModel
  {
    public $name = 'UserAccess';
    
    public $primaryKey = 'id';
    public $useTable = 'user_access';
    
  }
?>