<div class="table-header"> История торгового счёта </div>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Номер счета</th>
        <th>Дата открытия</th>
        <th>Тип</th>
        <th>Прибыль</th>
    </tr>
    </thead>

    <tbody>
    <?php
            if (count($historyInfo['demoAccount']['details'])) {
            foreach($historyInfo['demoAccount']['details'] as $row) {
            $html = "<tr>";
            $html .= "<td>{$row['order']}</td><td>{$row['start_time']}</td><td><span class='label label-success'>{$row['type']}</span></td><td>{$row['profit']}</td>";
            $html .= "</tr>";
            echo $html;
            }
            }
            ?>
    </tbody>
</table>

<div class="table-header"> История партнерского счёта</div>
<table class="table table-striped table-bordered">
<thead>
<tr>
    <th>Номер счета</th>
    <th>Дата открытия</th>
    <th>Тип</th>
    <th>Прибыль</th>
</tr>
</thead>

<tbody>
<?php
        if (count($historyInfo['agentAccount']['details'])) {
        foreach($historyInfo['agentAccount']['details'] as $row) {
        $html = "<tr>";
        $html .= "<td>{$row['order']}</td><td>{$row['start_time']}</td><td><span class='label label-success'>{$row['type']}</span></td><td>{$row['profit']}</td>";
        $html .= "</tr>";
        echo $html;
        }
        }
        ?>
</tbody>
</table>