<html>
	<head>
    <style type="text/css">
	ol{margin:0;padding:0}
	.c0{vertical-align:top;width:482.2pt;border-style:solid;background-color:#9aa9a1;border-color:#ffffff;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c4{vertical-align:top;width:67.5pt;border-style:solid;background-color:#e4e4e6;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c34{vertical-align:top;width:74.2pt;border-style:solid;background-color:#e4e4e6;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c23{vertical-align:top;width:90pt;border-style:solid;background-color:#e4e4e6;border-color:#ffffff;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c17{vertical-align:top;width:84.8pt;border-style:solid;background-color:#f1f1f2;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c13{vertical-align:top;width:288pt;border-style:solid;background-color:#f1f1f2;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c9{vertical-align:top;width:74.2pt;border-style:solid;background-color:#f1f1f2;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c19{vertical-align:top;width:67.5pt;border-style:solid;background-color:#f1f1f2;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c28{vertical-align:top;width:84.8pt;border-style:solid;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c32{vertical-align:top;width:285pt;border-style:solid;border-color:#f1f1f2;border-width:0.5pt;padding:5pt 5pt 5pt 5pt}
	.c18{vertical-align:top;width:288pt;border-style:solid;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c3{vertical-align:top;width:285.8pt;border-style:solid;border-color:#f1f1f2;border-width:0.5pt;padding:5pt 5pt 5pt 5pt}
	.c27{vertical-align:top;width:67.5pt;border-style:solid;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c1{vertical-align:top;width:56.2pt;border-style:solid;border-color:#efefef;border-width:1pt;padding:5pt 5pt 5pt 5pt}
	.c26{max-width:568.8pt;background-color:#ffffff;padding:50.4pt 21.6pt 50.4pt 21.6pt}
	.c8{color:#ffffff;font-size:8pt}
	.c2{height:10pt;direction:ltr}
	.c21{font-weight:bold}
	.c10{height:223pt}
	.c33{background-color:#f1f1f2}
	.c31{line-height:1.5}
	.c11{font-size:8pt}
	.c24{line-height:1.15}
	.c5{direction:ltr}
	.c29{color:#c2c2c4}
	.c22{margin-left:1pt}
	.c25{margin-left:0.8pt}
	.c15{border-collapse:collapse}
	.c14{color:#e4e4e6}
	.c16{height:0pt}
	.c7{text-align:right}
	.c20{height:139pt}
	.c12{color:#ffffff}
	.c6{text-align:center}
	.c30{height:22pt}
	.title{padding-top:0pt;line-height:1.15;text-align:left;color:#c2c2c4;font-size:48pt;font-family:"Arial";padding-bottom:0pt}
	.subtitle{padding-top:0pt;line-height:1.0;text-align:right;color:#ffffff;font-size:24pt;background-color:#9aa9a1;font-family:"Arial";font-weight:bold;padding-bottom:0pt}
	li{color:#7e8076;font-size:10pt;font-family:"Arial"}
	p{color:#7e8076;font-size:10pt;margin:0;font-family:"Arial"}
	h1{padding-top:0pt;line-height:1.0;text-align:left;color:#7e8076;font-size:8pt;font-family:"Arial";font-weight:bold;padding-bottom:0pt}
	h2{padding-top:0pt;line-height:1.0;text-align:right;color:#7e8076;font-size:8pt;font-family:"Arial";padding-bottom:0pt}
	h3{padding-top:0pt;line-height:1.5;text-align:left;color:#7e8076;font-size:9pt;font-family:"Arial";padding-bottom:0pt}
	h4{padding-top:0pt;line-height:1.5;text-align:left;color:#434343;font-size:8pt;font-family:"Arial";padding-bottom:0pt}
	h5{padding-top:0pt;line-height:1.0;text-align:right;color:#ffffff;font-size:10pt;background-color:#9aa9a1;font-family:"Arial";padding-bottom:0pt}
	h6{
		padding-top:0px;
		line-height:1.0;
		text-align:left;
		color:#666666;
		font-size:8pt;
		background-color:#e4e4e6;
		font-family:"Arial";
		padding: 0 0 0 5 px;
	}
    </style>
</head>
<body class="c26">
<table cellspacing="0" cellpadding="0" class="c15">
  <tbody>
   	<tr class="c16">
        		<td class="c23">
                	<h6 class="c5">
                    	<span>Aug 15, 2012<br></span>
                        <span>Invoice No. XXXX</span>
                    </h6>
                </td>
   	  <td class="c0">
            		<p class="c2"><span class="c12"></span></p>
        <p class="c5 subtitle"><span>Подтвирждения получения заказа</span></p>
        <h5 class="c5"><span>Prepared for Company Name • Project: Project X</span></h5>
        <h5 class="c5"><span>John Cooper • 000.000.000</span></h5>
      </td>
    </tr>
  </tbody>
</table>
<p class="c2"><span></span></p>
<table cellspacing="0" cellpadding="0" class="c15">
  <tbody>
        	<tr class="c30">
                <td class="c13">
                    <h1 class="c5"><span>Наиминование</span></h1>
                </td>
                <td class="c1 c33">
                    <h2 class="c5 c6 c25"><span>Цена</span></h2>
                </td>
                <td class="c17">
                    <h2 class="c5 c6"><span>НДС</span></h2>
                </td>
                <td class="c19">
                    <h2 class="c5 c6"><span>Кол-во</span></h2>
                </td>
                <td class="c9">
                    <h2 class="c5 c6"><span>Сумма</span></h2>
                </td>
            </tr>
    <tr class="c10">
            	<td class="c18">
                	<p class="c5"><span>Item number one: description of the work that was completed</span></p>
                    <p class="c2"><span></span></p>
                    <p class="c2"><span></span></p>
                    <p class="c2"><span></span></p>
                    <p class="c2"><span></span></p>
                    <p class="c2"><span></span></p>
                    <p class="c2"><span></span></p>
                    <p class="c2"><span></span></p>
                    <p class="c2"><span></span></p>
                </td>
                <td class="c1">
                	<p class="c5 c6"><span>$125</span></p>
                </td>
      <td class="c28">
       	<p class="c5 c6"><span>0%</span></p>
      </td>
                <td class="c27">
                	<p class="c5 c6"><span>1</span></p>
                </td>
                <td class="c9">
                	<p class="c5 c6"><span>$125</span></p>
                </td>
   	</tr>
    <tr class="c16">
            	<td class="c18">
                	<p class="c2"><span></span></p>
                </td>
                <td class="c1">
                	<p class="c2"><span></span></p>
                </td>
      <td class="c28">
                	<p class="c2"><span></span></p>
      </td><td class="c4">
       	<h1 class="c5 c7"><span>К оплате</span></h1>
                </td>
                <td class="c34">
                	<p class="c2"><span></span></p>
                </td>
    </tr>
  </tbody>
</table>
    <p class="c2"><span></span></p>
<p class="c2"><span></span></p>
    <table cellspacing="0" cellpadding="0" class="c15">
    	<tbody>
        	<tr class="c20">
            	<td class="c3">
                	<h3 class="c5"><span>PAYMENT TERMS</span></h3>
                    <h4 class="c5 c22"><span>To be made payable to First name, Last name</span></h4>
                    <p class="c2 c31"><span></span>
                    </p><h3 class="c5"><span>Адрес отправки счета и товара</span></h3>
                    <h4 class="c5 c22"><span>123 Main Street • Boulder, CO • 80026</span></h4>
                </td>
            	<td class="c32">
            		<h3 class="c5"><span>Банковские реквизиты</span></h3>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="c2"><span></span></p>
<div><p class="c2 c24"><span></span></p></div>
</body>
</html>