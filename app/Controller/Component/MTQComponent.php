<?php

  class MTQComponent extends Component
  {
    
    public function getDollarRate()
    {
      //This is a PHP(4/5) script example on how eurofxref-daily.xml can be parsed
      //Read eurofxref-daily.xml file in memory 
      //For this command you will need the config option allow_url_fopen=On (default)
      $XMLContent=file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
      //the file is updated daily between 2.15 p.m. and 3.00 p.m. CET
      
      foreach($XMLContent as $line){
        if(preg_match("/currency='([[:alpha:]]+)'/",$line,$currencyCode)){
          if(preg_match("/rate='([[:graph:]]+)'/",$line,$rate)){
            //Output the value of 1EUR for a currency code
            if ($currencyCode[1] == 'USD') $currency = $rate[1]; break;
            //--------------------------------------------------
            //Here you can add your code for inserting
            //$rate[1] and $currencyCode[1] into your database
            //--------------------------------------------------
          }
        }
      }
      
      return $currency;
    }
    
    public function formatMTQAccountFromCakePHP($aUser, $aUserInfos)
    {
      $aMTQAccount = null;

      if($aUser != null && is_array($aUser) && $aUserInfos != null && is_array($aUserInfos))
      {

          $sMTQpassword  = strtoupper($aUser["fr_user"]["last_name"][0]);
				$sMTQpassword .= strtoupper($aUser["fr_user"]["first_name"][0]);
				$sMTQpassword .= strftime("%d%m%Y", strtotime($aUser["fr_user"]["birthday"]));
				$sMTQpassword .= strtolower($aUser["fr_user"]["last_name"][0]);
				$sMTQpassword .= strtolower($aUser["fr_user"]["first_name"][0]);

                $agentParentAccount = isset($aUser["fr_user_parent_settings"]["agent_account"]) ? $aUser["fr_user_parent_settings"]["agent_account"] : "";

				$trading_account = ($aUser["fr_user_settings"]["trading_account"] > 0) ? $aUser["fr_user_settings"]["trading_account"] : 'Нет';
				$agent_account = ($aUser["fr_user_settings"]["agent_account"] > 0) ? $aUser["fr_user_settings"]["agent_account"] : 'Нет';

          $aMTQAccount = array (
                        "user_id" => $aUser["fr_user"]["id"],
                        "last_name" => $aUser["fr_user"]["last_name"],
                        "first_name" => $aUser["fr_user"]["first_name"],
                        "email" => $aUser["fr_user"]["user_email"],
                        "birthday" => $aUser["fr_user"]["birthday"],
                        "user_registered" => $aUser["fr_user"]["user_registered"],
                        "sex" => $aUser["fr_user"]["sex"],
                        "address" => $aUser["fr_user"]["address"],
                        "zip_code" => $aUser["fr_user"]["zip_code"],
                        "city" => $aUser["fr_user"]["city"],
                        "country" => $aUser["cc"]["cn"],
                        "partner_id" => $aUser["fr_user_settings"]["partner_id"],
                        "Phone_1" => (key_exists("added_user_value", $aUserInfos[1])) ? $aUserInfos[1]["added_user_value"] : "",
                        "Phone_2" => (key_exists("added_user_value", $aUserInfos[2])) ? $aUserInfos[2]["added_user_value"] : "",
                        "Phone_3" => (key_exists("added_user_value", $aUserInfos[3])) ? $aUserInfos[3]["added_user_value"] : "",
                        "Fax" => (key_exists("added_user_value", $aUserInfos[4])) ? $aUserInfos[4]["added_user_value"] : "",
                        "sype_name" => (key_exists("added_user_value", $aUserInfos[5])) ? $aUserInfos[5]["added_user_value"] : "",
                        "nickname" => (key_exists("added_user_value", $aUserInfos[6])) ? $aUserInfos[6]["added_user_value"] : "",
                        "programm_typ" => $aUser["fr_user_settings"]["programm_typ"],
                        "trading_account" => $trading_account,
                        "agent_account" => $agent_account,
                        "MTQpassword" => $sMTQpassword,
                        "userParent_id" => $aUser["fr_user"]["userParent_id"],
                        "agentParentAccount" => $agentParentAccount
                      );
      }
      return $aMTQAccount;
    }
  }
?>