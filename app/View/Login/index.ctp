<div class="container">
  <div class="login-wrapper" style="margin-top: 120px">
    <div style="text-align: center">
      <i class="icon-magic logo-icon"></i>
    </div>

    <div id="login-manager">
      <div id="login" class="login-wrapper animated">
        <form action="/login/loginuser" method="post">
          <div class="input-group">
            <input type="text" placeholder="Логин (E-Mail)" class="input-transparent" id="email" name="username"/>
            <input type="password" placeholder="Пароль" class="input-transparent" name="password"/>
          </div>
          <button id="login-submit" type="submit" class="login-button">Войти</button>
        </form>
      </div>

      <div id="forgot" class="login-wrapper animated" style="display: none;">
        <form action="?mode=restore" method="post">
          <div class="input-group">
            <input type="text" placeholder="Логин (E-Mail)" class="input-transparent" name="username" id="username"/>
          </div>
          <!--<button id="forgot-submit" type="submit" class="login-button">Восстановить</button>-->
		  <button href="?mode=restore" class="login-button" id="resend_password_link">Напомнить пароль</button>
        </form>
      </div>

      <div class="inner-well" style="text-align: center; margin: 20px 0;">
        <a href="#" id="login-link" class="button mini rounded gray"><i class="icon-signin"></i> Войти</a>
        <a href="#" id="forgot-link" class="button mini rounded gray"><i class="icon-question-sign"></i> Напомнить пароль</a>
      </div>
    </div>
  </div>
</div>
<script type="text/html" id="template-notification">
  <div class="notification animated fadeInLeftMiddle fast{{ item.itemClass }}">
    <div class="left">
      <div style="background-image: url({{ item.imagePath }})" class="{{ item.imageClass }}"></div>
    </div>
    <div class="right">
      <div class="inner">{{ item.text }}</div>
      <div class="time">{{ item.time }}</div>
    </div>

    <i class="icon-remove-sign hide"></i>
  </div>
</script>
<script type="text/html" id="template-notifications">
  <div class="container">
    <div class="row" id="notifications-wrapper">
      <div id="notifications" class="{{ bootstrapPositionClass }} notifications animated">
        <div id="dismiss-all" class="dismiss-all button blue">Закрыть</div>
        <div id="content">
          <div id="notes"></div>
        </div>
      </div>
    </div>
  </div>
</script><script src="../../javascripts/application.js" type="text/javascript"></script><script src="../../javascripts/docs.js" type="text/javascript"></script><script src="../../javascripts/docs_charts.js" type="text/javascript"></script>