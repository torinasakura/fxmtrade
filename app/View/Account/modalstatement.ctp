<form method="post" name="bonus_view" action="/account/payconfirm">
    <?php
			$fPayBonus = 0;
			if(isset($aUserParents))
			{
				foreach ( $aUserParents  as $aParent )
				{
					$fPayBonus = $aParent["fr_user_status"]["bonus_2"] - $fPayBonus;
					
					echo '
						<div style="float: left; width: 100px;">'.$aParent["fr_user"]["last_name"].'</div>
						<div style="float: left; width: 70px;">'.$aParent["fr_user_status"]["bonus_2"].' =></div>
						<input style="float: left; width: 50px;" name="confirm['.$aParent["p"]['user_id'].'][bonus]" type="text" value="'.$fPayBonus.'" />
						<input name="confirm['.$aParent["p"]['user_id'].'][user_id]" type="hidden" value="'.$aParent["p"]['user_id'].'" /><br /><br />
					';
					
					$fPayBonus = $aParent["fr_user_status"]["bonus_2"];
				}
			}
			else
				echo "error";
		?>
		<div style="float:left; width:70px; margin-left: 160px">
			<hr>
		</div>
		
		<div style="float:left; width:50px; margin-left: 183px">
			<div id="summ_structure">-</div>
		</div>
		<div style="float:right; margin-top:30px">
			<button class="pay_structur_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" >
				<span class="ui-button-text">Распределить</span>
			</button>
		</div>
		
		<input name="invoice_id" type="hidden" value=" <?php echo $iInvoiceID ?>" />
</form>

<script >
	jQuery(document).ready(function($) {
		calculateSum();
		
		$('.pay_structur_button').hover(
			function(){
				$(this).addClass("ui-state-hover");
			},
			function(){
				$(this).removeClass("ui-state-hover")
			}
		);
		
		function calculateSum()
		{
			var summ_structure = 0;
			$("input[type='text']").each(function(){
				summ_structure += parseInt($(this).val());
			});
			$("#summ_structure").text(summ_structure);
			
			if(summ_structure === 80){
				$("#summ_structure").css("color", "green");
			}
			else{
				$("#summ_structure").css("color", "red");
			}
		}
		
		$("input[type='text']").bind('keypress', function(){
					setTimeout(calculateSum, 1);
				}
		);
		
		
		$(".pay_structur_button").click(function(){
			//TODO: prüfung auf richtigkeit der eingetragenen werte mitaufnehmen
			document.bonus_view.submit();
		});
	});
</script>